To Start MongoDB Goto- Mongo db installation directory and fire following command for example with database location("E:\bin\mongodb\data" )

cd C:\Program Files\MongoDB\Server\3.0\bin
mongod.exe --dbpath "E:\bin\mongodb\data"

To Check if mongodb is running fine
cd C:\Program Files\MongoDB\Server\3.0\bin
run exe file:  mongo.exe
fire command: show dbs
Start Redis server

cd C:\Program Files\Redis
redis-server redis.windows.conf

Goto to project root directory and fire command
mvn clean spring-boot:run
It should setup download and start tomcat 
Goto to URL : http://localhost:8080
Put Login details if prompted: Username: user, Password: password
Click on login &  put Username: user, Password: password
Mysql Username:root, Password:root
Create database using create-dn.sql under database-scripts folder under root directory
Setting up javaagent
Pull down Windows
Select Preferences
Select Java
Select Installed JREs
Edit 
-javaagent:C:\Users\sheetal\.m2\repository\org\aspectj\aspectjweaver\1.8.7\aspectjweaver-1.8.7.jar -javaagent:C:\Users\sheetal\.m2\repository\org\springframework\spring-instrument\4.1.5.RELEASE\spring-instrument-4.1.5.RELEASE.jar
Give Goal as clean spring-boot:run in eclipse
Ready to go

Connect to mysql db on cloud
mysql -u root -p  --port 6033
root/root@321#
CREATE USER 'folitics'@'10.128.0.3' IDENTIFIED BY 'folitcs@321#';
GRANT ALL PRIVILEGES ON *.* TO 'folitics'@'10.128.0.3' IDENTIFIED BY 'folitcs@321#';


spring.data.mongodb.uri=mongodb://104.154.246.195:7107/folitics
security.user.password=password
#added by Mayank sharma - start
spring.jackson.date-format=yyyy-MM-dd'T'HH:mm:ss
multipart.maxFileSize=10Mb
#added by Mayank sharma - end
spring.data.mongodb.host=104.154.246.195
spring.data.mongodb.port=7107
#spring.data.mongodb.username=user
spring.data.mongodb.database=folitics
#spring.data.mongodb.password=password
#### Hot deploy
spring.thymeleaf.cache=false
spring.template.cache=false
##### Hibernate properties #######
db.driver: com.mysql.jdbc.Driver
db.url: jdbc:mysql://10.128.0.2:6033/folitics?autoReconnect=true&useSSL=false
db.username: folitics
db.password: folitcs@321#

start mongo db on cloud
mongod --port 7107 --dbpath=/home/jahidiitr/folitics/mongodb &