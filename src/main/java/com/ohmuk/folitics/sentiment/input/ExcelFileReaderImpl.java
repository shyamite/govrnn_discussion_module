package com.ohmuk.folitics.sentiment.input;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.ohmuk.folitics.excel.reader.input.ExcelFileReader;

@Service
public class ExcelFileReaderImpl implements ExcelFileReader {

	@Override
	public Workbook getReader(String filePath) throws IOException {
	    FileInputStream excelFile = new FileInputStream(new File(filePath));
		Workbook reader = new XSSFWorkbook(excelFile);
		return reader;
	}
}
