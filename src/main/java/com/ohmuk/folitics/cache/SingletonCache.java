package com.ohmuk.folitics.cache;

import java.util.HashMap;
import java.util.Map;

//Singleton Cache
public class SingletonCache implements FoliticsCache<Object, Object> {
	// Passed Object as type parameter to fix the warning issue
	private Map<Object, Object> map;

	// Constructor
	private static SingletonCache sc = new SingletonCache();

	// Only once instance is kept during the lifecycle of the application
	public static SingletonCache getInstance() {
		return sc;
	}

	// Private constructor to prevent instantiation
	private SingletonCache() {
		map = new HashMap<Object, Object>();
	}

	// The warning is gone by using object
	// variable parameters in map
	public void put(Object key, Object value) {
		map.put(key, value);
	}

	public Object get(Object key) {
		return map.get(key);
	}
}
