package com.ohmuk.folitics.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;

import com.ohmuk.folitics.component.newsfeed.ImageTypeEnum;
import com.ohmuk.folitics.hibernate.entity.UserImage;

public class ImageUtil {

	public static final int MAX_SENTIMENT_IMAGES= 10;
	
    public static final String getFileExention(String filename){
//        int index = filename.indexOf(".");
//        String extension=  filename.substring(index, filename.length()-1);
        String ext = FilenameUtils.getExtension(filename);
        return ext;
    }
    
    public static List<Long> getListOfIds(String ids){
        List<String> idsList =  Arrays.asList(ids.split(","));
        List<Long> list = new ArrayList<Long>();
        for(String s:idsList){
            list.add(Long.parseLong(s));
        }
        return list;
    }
    
    public static UserImage getUserImage(String imageType,
			List<UserImage> uimages) {
		if (uimages != null && !uimages.isEmpty()) {
			for (UserImage u : uimages) {
				if (u.getImageType().equals(
						ImageTypeEnum.USERIMAGE.getImageType())) {
					return u;
				} else if (u.getImageType().equals(
						ImageTypeEnum.ALBUMIMAGE.getImageType())) {
					return u;
				}
			}
		}
		return null;
	}

    public static  List<UserImage> addUserImage(String imageType, List<UserImage> uimages,
			UserImage image) {
		UserImage removedUserImage = removeUserImage(imageType, uimages);
		//image.setId(removedUserImage.getId());
		if(removedUserImage != null){
			image.setUser(removedUserImage.getUser());			
		}
		uimages.add(image);		
		return uimages;
	}

    public static UserImage removeUserImage(String imageType,
			List<UserImage> uimages) {
    	UserImage userImage = null;
		if (uimages != null && !uimages.isEmpty()) {
			int index = -1;
			int count = 0;
			for (UserImage u : uimages) {
				if (imageType.equals(u.getFileType())) {
					userImage = u;
					System.out.println("user image "+ u);
					index = count;
				} else if (imageType.equals(u.getFileType())) {
					userImage = u;
					System.out.println("album image "+ u);
					index = count; 
				}
				count++;
			}
			if (index != -1) {
				uimages.remove(index);
			}
		}
		return userImage;
	}

    public static final byte[] cropImage(byte[] imageInByte, int width , int height){
    	InputStream in = new ByteArrayInputStream(imageInByte);
    	BufferedImage bImageFromConvert;
    	ByteArrayOutputStream baos =null;
		try {
			bImageFromConvert = ImageIO.read(in);
			System.out.println("Original Image Dimension: "+bImageFromConvert.getWidth()+"x"+bImageFromConvert.getHeight());
	    	BufferedImage subImgage = bImageFromConvert.getSubimage(100, 50, width, height);
	    	baos = new ByteArrayOutputStream();
	    	ImageIO.write( subImgage, "jpg", baos );
	    	baos.flush();
	    	byte[] imageOutByte = baos.toByteArray();
	    	return imageOutByte;
		} catch (IOException e) {
			e.printStackTrace();
			return imageInByte;
		}
		finally {
				try {
					if(baos !=null)
						baos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
    }

}
