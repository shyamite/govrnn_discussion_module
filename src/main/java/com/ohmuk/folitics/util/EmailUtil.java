package com.ohmuk.folitics.util;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailUtil {


	private static Logger logger = LoggerFactory
			.getLogger(EmailUtil.class);

	protected static final Logger LOGGER = LoggerFactory.getLogger(EmailUtil.class);

	public static final String  body_for_forgot_username = "<!DOCTYPE html>"
			+ "<html lang='en'><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>"
			+ "<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->"
			+ "<title>Govrnn::Forget Username</title></head><body style='font-family:sans-serif; margin:0 auto'><div style='width:600px; background-color:#edf0f3;'>"
			+ "<div style='width:90%;margin:0 auto;background-color:#fff;'><div style='background-color:#ef2d27;padding:5px;font-size:16px;'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452858/logo_email_fjs1sc.jpg'  ></div>"
			+ "<div style='float:right'><div style='margin-top:10px;'><div style='float:left; padding-right:5px;padding-top:10px'>"
			+ "<a href='#' style='color:#fff; text-decoration:none'>{0} &nbsp;&nbsp;</a></div><div style='float:left;padding-right:10px;padding-top:9px'><img src='cid:image' width='36px' height='36px'></div>"
			+ "<div style='clear:both'></div></div></div><div style='clear:both'></div></div><div style='clear:both; height:15px;'></div><div style='padding:20px'><div style='font-size:18px;font-weight:bold;'>Dear User,</div>"
			+ "<div style='clear:both; height:30px;'></div><div style='font-size:17px;font-weight:400;color:#797979;'><br/> Following are the login details as you have requested."
			+ "<div style='margin-top:20px; color:#000; font-size:16px;'>"
			+ "<div style='font-weight:bold' >Login Name : {1}</div><div style='clear:both; height:40px;'></div></div><p>Please note if you have forgotten the password also, please use forgot password functionality to retrieve the password also.</p></div>"
			+ "<div style='clear:both; height:100px;'></div> <div style='font-size:16px;color:#000;'> <div style='float:left;'> <div style='color:#ef2d27;'>Thank you for using Govrnn !</div>"
			+ "<div style='color:#000'>Govrnn Team</div> <div> <div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452860/twit_umdr8z.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452849/fb_fwzllz.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453294/G_PLUS_uqpfyk.jpg'   /></a></div>  "
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452854/likn_uu6scq.jpg'   /></a></div>"
			+ "<div style='clear:both;'></div>     </div> <div style='clear:both; height:5px;'></div> <div> <a href='#'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453115/email_icon_qztver.png'  ></div>"
			+ " <div style='float:left; font-size: 12px; color: #000; padding: 2px;  padding-top: 4px;'>contact@govrnn.com&nbsp;&nbsp;</div></a> <a href='#'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453112/phn_icon_opodes.png'  ></div>"
			+ " <div style='float:left; font-size:12px;color:#000;padding:2px;padding-top:4px;' >01147852477 </div></a> <div class='clearfix'></div> </div>  <div style='clear:both;'></div>"
			+ " <div style='font-size:12px;color:#00;	padding:2px;padding-top:4px; padding-top:0px;' >Delhi: 807, Hemkunt House, Rajender Place,<br> New Delhi - 110008, INDIA</div> </div>"
			+ " <div style='float:right;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452842/email_foot_dj8ncv.jpg'  ></div>"
			+ " <div style='clear:both; '></div> </div></div></div><div style='clear:both; height:15px'></div></div></body></html>";

	public static final String body_for_forgot_passowrd = "<!DOCTYPE html>"
			+ "<html lang='en'><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>"
			+ "<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags --><title>Govrnn::Forget Password</title>"
			+ "<!-- Bootstrap -->"
			+ "</head><body  style='font-family:sans-serif; margin:0 auto'><div style='width:600px; background-color:#edf0f3;'><div style='width:90%;margin:0 auto;background-color:#fff;'>"
			+ "<div style='background-color:#ef2d27;padding:5px;font-size:16px;'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452858/logo_email_fjs1sc.jpg'  ></div>"
			+ "<div style='float:right'><div style='margin-top:10px;'><div style='float:left;padding-right:5px;padding-top:10px'>"
			+ "<a href='#' style='color:#fff; text-decoration:none'>{0}  </a></div><div style='float:left ; padding-right:10px ; padding-top:9px'><img src='cid:image'  width='36px' height='36px'></div>"
			+ "<div style='clear:both'></div></div></div><div style='clear:both'></div></div><div style='clear:both; height:15px;'></div><div style='padding:20px'><div style='font-size:18px;font-weight:bold;'>Dear User,</div>"
			+ "<div style='clear:both; height:30px;'></div><div style='font-size:17px;font-weight:400;color:#797979;'><br/> Following are the login details as you have requested.<div style='margin-top:20px; color:#000; font-size:16px;'>"
			+ "<div style='font-weight:bold' >Login Name : {1}</div><div  style='font-weight:bold; padding-top:5px;'>Password : {2}</div><div style='clear:both; height:40px;'></div>"
			+ "</div><p>Please note the password is system generated password. Please login to <a target='_blank' href='http://www.govrnn.com' style='color:red; text-decoration:underline; font-weight:bold'>www.govrnn.com</a> and change your password.</p></div>"
			+ "<div style='clear:both; height:100px;'></div> <div style='font-size:16px;color:#000;'> <div style='float:left;'> <div style='color:#ef2d27;'>Thank you for using Govrnn !</div>"
			+ "<div style='color:#000'>Govrnn Team</div> <div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452860/twit_umdr8z.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452849/fb_fwzllz.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453294/G_PLUS_uqpfyk.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452854/likn_uu6scq.jpg'   /></a></div>"
			+ "<div style='clear:both;'></div>     </div> <div style='clear:both; height:5px;'></div> <div> <a href='#'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453115/email_icon_qztver.png'  ></div>"
			+ " <div style='float:left; font-size: 12px; color: #000; padding: 2px;  padding-top: 4px;'>contact@govrnn.com&nbsp;&nbsp;</div></a> <a href='#'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453112/phn_icon_opodes.png'  ></div>"
			+ " <div style='float:left; font-size:12px;color:#000;padding:2px;padding-top:4px;' >01147852477 </div></a> <div class='clearfix'></div> </div>  <div style='clear:both;'></div>"
			+ " <div style='font-size:12px;color:#00;	padding:2px;padding-top:4px; padding-top:0px;' >Delhi: 807, Hemkunt House, Rajender Place,<br> New Delhi - 110008, INDIA</div>"
			+ " </div> <div style='float:right;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452842/email_foot_dj8ncv.jpg'  ></div>"
			+ " <div style='clear:both; '></div> </div></div></div><div style='clear:both; height:15px'></div></div></body></html>";

	public static final String body_for_reset_passowrd = "<!DOCTYPE html>"
			+ "<html lang='en'><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>"
			+ "<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags --><title>Govrnn::Forget Password</title>"
			+ "<!-- Bootstrap -->"
			+ "</head><body  style='font-family:sans-serif; margin:0 auto'><div style='width:600px; background-color:#edf0f3;'><div style='width:90%;margin:0 auto;background-color:#fff;'>"
			+ "<div style='background-color:#ef2d27;padding:5px;font-size:16px;'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452858/logo_email_fjs1sc.jpg'  ></div>"
			+ "<div style='float:right'><div style='margin-top:10px;'><div style='float:left;padding-right:5px;padding-top:10px'>"
			+ "<a href='#' style='color:#fff; text-decoration:none'>{0}  </a></div><div style='float:left ; padding-right:10px ; padding-top:9px'><img src='cid:image'  width='36px' height='36px'></div>"
			+ "<div style='clear:both'></div></div></div><div style='clear:both'></div></div><div style='clear:both; height:15px;'></div><div style='padding:20px'><div style='font-size:18px;font-weight:bold;'>Dear User,</div>"
			+ "<div style='clear:both; height:30px;'></div><div style='font-size:17px;font-weight:400;color:#797979;'><br/> Following are the login details as you have requested.<div style='margin-top:20px; color:#000; font-size:16px;'>"
			+ "<div style='font-weight:bold' >Login Name : {1}</div><div  style='font-weight:bold; padding-top:5px;'>New Password : {2}</div><div style='clear:both; height:40px;'></div>"
			+ "</div><p>Please note the password is system generated password. Please login to <a target='_blank' href='http://www.govrnn.com' style='color:red; text-decoration:underline; font-weight:bold'>www.govrnn.com</a> and change your password.</p></div>"
			+ "<div style='clear:both; height:100px;'></div> <div style='font-size:16px;color:#000;'> <div style='float:left;'> <div style='color:#ef2d27;'>Thank you for using Govrnn !</div>"
			+ "<div style='color:#000'>Govrnn Team</div> <div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452860/twit_umdr8z.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452849/fb_fwzllz.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453294/G_PLUS_uqpfyk.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452854/likn_uu6scq.jpg'   /></a></div>"
			+ "<div style='clear:both;'></div>     </div> <div style='clear:both; height:5px;'></div> <div> <a href='#'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453115/email_icon_qztver.png'  ></div>"
			+ " <div style='float:left; font-size: 12px; color: #000; padding: 2px;  padding-top: 4px;'>contact@govrnn.com&nbsp;&nbsp;</div></a> <a href='#'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453112/phn_icon_opodes.png'  ></div>"
			+ " <div style='float:left; font-size:12px;color:#000;padding:2px;padding-top:4px;' >01147852477 </div></a> <div class='clearfix'></div> </div>  <div style='clear:both;'></div>"
			+ " <div style='font-size:12px;color:#00;	padding:2px;padding-top:4px; padding-top:0px;' >Delhi: 807, Hemkunt House, Rajender Place,<br> New Delhi - 110008, INDIA</div>"
			+ " </div> <div style='float:right;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452842/email_foot_dj8ncv.jpg'  ></div>"
			+ " <div style='clear:both; '></div> </div></div></div><div style='clear:both; height:15px'></div></div></body></html>";

	public static final String body_welcome_email = "<!DOCTYPE html>"
			+ "<html lang='en'><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>"
			+ "<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags --><title>Govrnn::Forget Password</title>"
			+ "<!-- Bootstrap -->"
			+ "</head><body  style='font-family:sans-serif; margin:0 auto'><div style='width:600px; background-color:#edf0f3;'><div style='width:90%;margin:0 auto;background-color:#fff;'>"
			+ "<div style='background-color:#ef2d27;padding:5px;font-size:16px;'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452858/logo_email_fjs1sc.jpg'  ></div>"
			+ "<div style='float:right'><div style='margin-top:10px;'><div style='float:left;padding-right:5px;padding-top:10px'>"
			+ "<a href='#' style='color:#fff; text-decoration:none'>{0}  </a></div><div style='float:left ; padding-right:10px ; padding-top:9px'><img src='cid:image'  width='36px' height='36px'></div>"
			+ "<div style='clear:both'></div></div></div><div style='clear:both'></div></div><div style='clear:both; height:15px;'></div><div style='padding:20px'><div style='font-size:18px;font-weight:bold;'>Dear User,</div>"
			+ "<div style='clear:both; height:30px;'></div><div style='font-size:17px;font-weight:400;color:#797979;'><br/> Thank you for registering with Govrrn. You can login to <a target='_blank' href='http://www.govrnn.com' style='color:red; text-decoration:underline; font-weight:bold'>www.govrnn.com</a>  using following login details.<div style='margin-top:20px; color:#000; font-size:16px;'>"
			+ "<div style='font-weight:bold' >Login Name : {1}</div><div  style='font-weight:bold; padding-top:5px;'>Password : {2}</div><div style='clear:both; height:40px;'></div>"
			+ "</div><p>Please click on the following url to verify your email - </p>"
			+ "<p>{3}</p></div>"
			+ "<div style='clear:both; height:100px;'></div> <div style='font-size:16px;color:#000;'> <div style='float:left;'> <div style='color:#ef2d27;'>Thank you for using Govrnn !</div>"
			+ "<div style='color:#000'>Govrnn Team</div> <div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452860/twit_umdr8z.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452849/fb_fwzllz.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453294/G_PLUS_uqpfyk.jpg'   /></a></div>"
			+ "<div style='float:left;'><a href='#'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452854/likn_uu6scq.jpg'   /></a></div>"
			+ "<div style='clear:both;'></div>     </div> <div style='clear:both; height:5px;'></div> <div> <a href='#'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453115/email_icon_qztver.png'  ></div>"
			+ " <div style='float:left; font-size: 12px; color: #000; padding: 2px;  padding-top: 4px;'>contact@govrnn.com&nbsp;&nbsp;</div></a> <a href='#'><div style='float:left;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486453112/phn_icon_opodes.png'  ></div>"
			+ " <div style='float:left; font-size:12px;color:#000;padding:2px;padding-top:4px;' >01147852477 </div></a> <div class='clearfix'></div> </div>  <div style='clear:both;'></div>"
			+ " <div style='font-size:12px;color:#00;	padding:2px;padding-top:4px; padding-top:0px;' >Delhi: 807, Hemkunt House, Rajender Place,<br> New Delhi - 110008, INDIA</div>"
			+ " </div> <div style='float:right;'><img src='http://res.cloudinary.com/govrnn-com/image/upload/v1486452842/email_foot_dj8ncv.jpg'  ></div>"
			+ " <div style='clear:both; '></div> </div></div></div><div style='clear:both; height:15px'></div></div></body></html>";


	public static final void sendEmail(String to, String cc, String from, String subject,
			String body, byte[] image, String imageType) throws Exception {

		String bcc = "jahidiitr04@gmail.com";
		logger.info("Inside sendEmail method in business delegate");
		/**
		 * Recovery phone	
		(864) 720-8722 Edit
		Recovery email	
		calmsoumya@gmail.com Edit
		 */
//		final String username = "info@govrnn.com";
//		final String password = "zahid@2017";
		final String username = "sampltest2017@gmail.com";
		final String password = "ohmukgroup2016";
		
		
		cc = username;
		from = "info@govrnn.com";
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		
//		Properties props = new Properties();
//		props.put("mail.smtp.auth", "true");
////		props.put("mail.smtp.starttls.enable", "true");
//		props.put("mail.smtp.host", "mail.govrnn.com");
//		props.put("mail.smtp.port", "25");

		try {

			javax.mail.Session emailSession = javax.mail.Session.getInstance(
					props, new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username,
									password);
						}
					});

			Message emailMessage = new MimeMessage(emailSession);
			
			emailMessage.addRecipient(Message.RecipientType.TO,
					new InternetAddress(to));
			emailMessage.addRecipient(Message.RecipientType.CC,
					new InternetAddress(cc));
			emailMessage.addRecipient(Message.RecipientType.BCC,
					new InternetAddress(bcc));
			emailMessage.setFrom(new InternetAddress(from));
			emailMessage.setSubject(subject);

			MimeMultipart multipart = new MimeMultipart("related");

			// first part (the html)
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(body, "text/html");
			// add it
			multipart.addBodyPart(messageBodyPart);
			if (image != null) {
				// second part (the image)
				messageBodyPart = new MimeBodyPart();
				if(imageType==null){
					imageType="image/jpg";
				}
				final ByteArrayDataSource dataSource = new ByteArrayDataSource(
						image, imageType);
				messageBodyPart.setDataHandler(new DataHandler(dataSource));
				messageBodyPart.setHeader("Content-ID", "<image>");
				// add image to the multipart
				multipart.addBodyPart(messageBodyPart);
			}
			// put everything together
			emailMessage.setContent(multipart);
			Transport.send(emailMessage);

			logger.info("Email sent successfully to " + to);
			logger.info("Exiting from SendEmail method");
		} catch (Exception addExe) {
			addExe.printStackTrace();
			logger.error("Error while sending email : "+addExe);
			throw new AddressException();
		}
	}

}
