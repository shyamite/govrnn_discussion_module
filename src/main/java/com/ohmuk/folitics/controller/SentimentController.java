package com.ohmuk.folitics.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ohmuk.folitics.businessDelegate.interfaces.IOpinionBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IPollBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.ISentimentBusinessDelegate;
import com.ohmuk.folitics.component.newsfeed.ImageSizeEnum;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.enums.FileType;
import com.ohmuk.folitics.enums.SentimentState;
import com.ohmuk.folitics.enums.SentimentType;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.ouput.model.SentimentOutputModel;
import com.ohmuk.folitics.ouput.model.SentimentOutputModelFull;
import com.ohmuk.folitics.sentiment.input.ISentimentInputReader;
import com.ohmuk.folitics.sentiment.input.SentimentInputModel;
import com.ohmuk.folitics.service.newsfeed.ISentimentNewsLoaderService;
import com.ohmuk.folitics.util.AttachmentUtil;
import com.ohmuk.folitics.util.ThumbnailUtil;

/**
 * @author Abhishek
 * 
 */
@Controller
@RequestMapping("/sentiment")
public class SentimentController {

	private static Logger LOGGER = LoggerFactory
			.getLogger(SentimentController.class);

	@Autowired
	private volatile ISentimentBusinessDelegate sentimentBusinessDelegate;
	
	@Autowired
	private volatile IPollBusinessDelegate pollBusinessDelegate;
	
	@Autowired
	private IOpinionBusinessDelegate opinionBusinessDelegate;
	
	@Autowired
	private ISentimentNewsLoaderService sentimentNewsLoaderService;
	
	@Autowired
	ISentimentInputReader sentimentInputReader;

	@RequestMapping
	public String getSentimentPage() {

		return "sentiment-page";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Sentiment> getAdd() {

		List<Sentiment> sentiments = new ArrayList<>();
		sentiments.add(getTestSentiment());
		return new ResponseDto<Sentiment>(true, sentiments);
	}

	/**
	 * This method is used to add sentiment. This is a multipart spring web
	 * service that requires image and json for sentiment.
	 * 
	 * @author gautam.yadav
	 * @param image
	 * @param sentiment
	 * @return ResponseDto<Sentiment>
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Sentiment> add(
			@RequestPart(value = "file") MultipartFile image,
			@RequestPart(value = "sentiment") Sentiment sentiment) {

		LOGGER.info("Inside SentimentController add method");
		if (image == null) {
			LOGGER.error("Image is null", new Exception(
					"Image is required to add sentiment"));
			return new ResponseDto<Sentiment>(false);
		}
		if (sentiment == null) {
			LOGGER.error("Sentiment is null",
					new Exception("Sentiment is null"));
			return new ResponseDto<Sentiment>(false);
		}
		try {
			LOGGER.debug("Image " + image.getOriginalFilename() + "Received "
					+ "Image size " + image.getSize());
			LOGGER.debug("Sentiment to be saved " + sentiment.getSubject());

			sentiment.setId(null);
			sentiment.setState(SentimentState.ALIVE.getValue());

			sentiment.setImage(image.getBytes());
			sentiment.setImageType(FileType.getFileType(
					image.getOriginalFilename().split("\\.")[1]).getValue());
			/*
			 * List<Poll> polls = new ArrayList<Poll>(); if
			 * (sentiment.getPolls() != null) { for (Poll poll :
			 * sentiment.getPolls()) { // poll.setSentiment(sentiment);
			 * 
			 * Poll sessionPoll = pollBusinessDelegate.getPollById(poll
			 * .getId()); //sessionPoll.setSentiment(sentiment);
			 * polls.add(sessionPoll);
			 * 
			 * // pollBusinessDelegate.saveAndFlush(sessionPoll);
			 * 
			 * logger.debug("Poll :" + sessionPoll.getQuestion() + " saved"); }
			 * sentiment.setPolls(polls); }
			 */
			sentiment = sentimentBusinessDelegate.save(sentiment);
			LOGGER.debug("Sentiment and polls saved/updated");

			return new ResponseDto<Sentiment>(true, sentiment);

		} catch (IOException ioException) {
			LOGGER.error("Error saving sentiment/poll", ioException);
			LOGGER.error("Exiting add sentiment");

			return new ResponseDto<Sentiment>(false);
		} catch (Exception exception) {
			LOGGER.error("Error saving sentiment/poll", exception);
			LOGGER.info("Exiting from SentimentController add method");
			return new ResponseDto<Sentiment>(false);
		}
	}

	/**
	 * This method is used to add sentiment. This is a multipart spring web
	 * service that requires image and json for sentiment.
	 * 
	 * @author gautam.yadav
	 * @param image
	 * @param sentiment
	 * @return ResponseDto<Sentiment>
	 */
	@RequestMapping(value = "/addSentiment", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Sentiment> addSentiment(
			@RequestPart(value = "file") MultipartFile image,
			@RequestPart(value = "attachments") List<MultipartFile> files,
			@RequestPart(value = "sentiment") Sentiment sentiment) {

		LOGGER.info("Inside SentimentController add method");
		if (image == null) {
			LOGGER.error("Image is null", new Exception(
					"Image is required to add sentiment"));
			return new ResponseDto<Sentiment>(false);
		}
		if (sentiment == null) {
			LOGGER.error("Sentiment is null",
					new Exception("Sentiment is null"));
			return new ResponseDto<Sentiment>(false);
		}
		try {
			LOGGER.debug("Image " + image.getOriginalFilename() + "Received "
					+ "Image size " + image.getSize());
			LOGGER.debug("Sentiment to be saved " + sentiment.getSubject());

			sentiment.setId(null);
			sentiment.setState(SentimentState.ALIVE.getValue());

			sentiment.setImage(image.getBytes());
			sentiment.setImageType(FileType.getFileType(
					image.getOriginalFilename().split("\\.")[1]).getValue());

			List<Attachment> attachments = new ArrayList<Attachment>();
			for (MultipartFile file : files) {
				Attachment attachment = new Attachment();
				attachments.add(AttachmentUtil.prepareAttachment(file,
						attachment));
			}
			sentiment.setAttachments(attachments);
			sentiment = sentimentBusinessDelegate.save(sentiment);
			LOGGER.debug("Sentiment and polls saved/updated");

			return new ResponseDto<Sentiment>(true, sentiment);

		} catch (IOException ioException) {
			LOGGER.error("Error saving sentiment/poll", ioException);
			LOGGER.error("Exiting add sentiment");

			return new ResponseDto<Sentiment>(false);
		} catch (Exception exception) {
			LOGGER.error("Error saving sentiment/poll", exception);
			LOGGER.info("Exiting from SentimentController add method");
			return new ResponseDto<Sentiment>(false);
		}
	}

	/**
	 * Web service is to edit {@link Sentiment}
	 * 
	 * @param sentiment
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Sentiment> edit(
			@RequestBody Sentiment sentiment) {
		LOGGER.info("Inside SentimentController edit method");
		try {
			sentiment = sentimentBusinessDelegate.update(sentiment);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in updating Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController edit method");

			return new ResponseDto<Sentiment>(false);
		}
		if (sentiment != null) {
			LOGGER.debug("Sentiment with id: " + sentiment.getId()
					+ " is update");
			LOGGER.info("Exiting from SentimentController edit method");
			return new ResponseDto<Sentiment>(true, sentiment);
		}
		LOGGER.debug("Sentiment is not update");
		LOGGER.info("Exiting from SentimentController edit method");
		return new ResponseDto<Sentiment>(false);
	}

	/**
	 * Web service to clone {@link Sentiment}
	 * 
	 * @param sentiment
	 * @return
	 */
	@RequestMapping(value = "/clone", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Sentiment> clone(
			@RequestBody Sentiment sentiment) {
		LOGGER.info("Inside SentimentController clone method");
		if (null != sentiment) {
			try {
				sentiment = sentimentBusinessDelegate.clone(sentiment);
			} catch (Exception exception) {
				LOGGER.error("Exception in cloning Sentiment");
				LOGGER.error("Exception: " + exception);

				return new ResponseDto<Sentiment>(false);
			}
			LOGGER.debug("Sentiment clone with id :" + sentiment.getId()
					+ " is saved");
			/*
			 * try { for (Poll poll : sentiment.getPolls()) {
			 * poll.setSentiment(sentiment);
			 * 
			 * Poll sessionPoll = pollBusinessDelegate.getPollById(poll
			 * .getId()); sessionPoll.setSentiment(sentiment);
			 * 
			 * pollBusinessDelegate.saveAndFlush(sessionPoll);
			 * 
			 * logger.debug("Poll :" + sessionPoll.getQuestion() + " saved"); }
			 * } catch (Exception exception) {
			 * logger.error("Error saving sentiment/poll", exception);
			 * logger.error("Exiting add sentiment");
			 * logger.info("Exiting from SentimentController clone method");
			 * 
			 * return new ResponseDto<Sentiment>(false); }
			 */
			LOGGER.info("Exiting from SentimentController clone method");
			return new ResponseDto<Sentiment>(true, sentiment);
		}
		LOGGER.info("Exiting from SentimentController clone method");
		return new ResponseDto<Sentiment>(false);

	}

	/**
	 * Web service is to soft delete {@link Sentiment} by id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteById", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Sentiment> delete(Long id) {
		LOGGER.info("Inside SentimentController deleteById method");
		try {
			if (sentimentBusinessDelegate.delete(id)) {
				LOGGER.debug("Sentiment with id: " + id + " is soft delete");
				LOGGER.info("Exiting from SentimentController deleteById method");
				return new ResponseDto<Sentiment>(true);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in deleting Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController deleteById method");

			return new ResponseDto<Sentiment>(false);
		}
		LOGGER.debug("Sentiment with id: " + id + " is not delete");
		LOGGER.info("Exiting from SentimentController deleteById method");
		return new ResponseDto<Sentiment>(false);
	}

	/**
	 * Web service is to soft delete {@link Sentiment}
	 * 
	 * @param sentiment
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Sentiment> delete(
			@RequestBody Sentiment sentiment) {
		LOGGER.info("Inside SentimentController delete method");
		try {
			if (sentimentBusinessDelegate.delete(sentiment)) {
				LOGGER.debug("Sentiment with id: " + sentiment.getId()
						+ " is soft delete");
				LOGGER.info("Exiting from SentimentController delete method");
				return new ResponseDto<Sentiment>(true);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in updating Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController delete method");

			return new ResponseDto<Sentiment>(false);
		}
		LOGGER.debug("Sentiment is not delete");
		LOGGER.info("Exiting from SentimentController delete method");
		return new ResponseDto<Sentiment>(false);
	}

	/**
	 * Web service is to hard delete {@link Sentiment} by id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteFromDbById", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Sentiment> deleteFromDB(
			@RequestParam Long id) {
		LOGGER.info("Inside SentimentController deleteFromDbById method");
		try {
			if (sentimentBusinessDelegate.deleteFromDB(id)) {
				LOGGER.debug("Sentiment with id: " + id + " is deleted from DB");
				LOGGER.info("Exiting from SentimentController deleteFromDbById method");
				return new ResponseDto<Sentiment>(true);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in deleteFromDBbyId Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController deleteFromDbById method");

			return new ResponseDto<Sentiment>(false);
		}
		LOGGER.debug("Sentiment with id: " + id + " is not deleted");
		LOGGER.info("Exiting from SentimentController deleteFromDbById method");
		return new ResponseDto<Sentiment>(false);
	}

	/**
	 * Web service is to delete {@link Sentiment}
	 * 
	 * @param sentiment
	 * @return
	 */
	@RequestMapping(value = "/deleteFromDb", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Sentiment> deleteFromDB(
			@RequestBody Sentiment sentiment) {
		LOGGER.info("Inside SentimentController deleteFromDb method");
		try {
			if (sentimentBusinessDelegate.deleteFromDB(sentiment)) {
				LOGGER.debug("Sentiment of id: " + sentiment.getId()
						+ " is deleted form DB");
				return new ResponseDto<Sentiment>(true);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in deleteFromdb Sentiment : "
					+ sentiment.getId());
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController deleteFromDb method");

			return new ResponseDto<Sentiment>(false);
		}
		LOGGER.debug("Sentiment is not deleted");
		LOGGER.info("Exiting from SentimentController deleteFromDb method");
		return new ResponseDto<Sentiment>(false);
	}

	/**
	 * Web service is to get all source ( {@link Link} ) by {@link Sentiment} id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getAllSources", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Link> getAllSources(Long id) {
		LOGGER.info("Inside SentimentController getAllSources method");
		Sentiment sentiment = null;
		try {
			sentiment = sentimentBusinessDelegate.read(id);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getAllSources Sentiment");
			LOGGER.error("Exception: " + exception);

			return new ResponseDto<Link>(false);
		}
		if (sentiment != null) {
			List<Link> linkSet = null;
			try {
				linkSet = sentimentBusinessDelegate
						.getAllSourcesForSentiment(sentiment);
			} catch (Exception exception) {
				LOGGER.error("Exception in getAllSources 2 Sentiment");
				LOGGER.error("Exception: " + exception);
				LOGGER.info("Exiting from SentimentController getAllSources method");

				return new ResponseDto<Link>(false);
			}
			if (linkSet != null) {
				LOGGER.debug(linkSet.size()
						+ " source is found for sentiment with id: " + id);
				LOGGER.info("Exiting from SentimentController getAllSources method");
				return new ResponseDto<Link>(true, linkSet);
			}
		}
		LOGGER.debug("No source is found for sentiment with id: " + id);
		LOGGER.info("Exiting from SentimentController getAllSources method");
		return new ResponseDto<Link>(false);
	}

	/**
	 * Web service is to get {@link Sentiment} by id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Sentiment> find(Long id) {
		LOGGER.info("Inside SentimentController find method");
		Sentiment sentiment = null;
		try {
			sentiment = sentimentBusinessDelegate.read(id);
			if (sentiment != null) {
//				try {
//					sentiment.setImage( ThumbnailUtil.getSentimentImageThumbnail(sentiment.getImage(),sentiment.getImageType()));
//				} catch (IOException e) {
//				}
				LOGGER.debug("Sentiment is found for id: " + id);
				LOGGER.info("Exiting from SentimentController find method");
				return new ResponseDto<Sentiment>(true, sentiment);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in finding Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController find method");

			return new ResponseDto<Sentiment>(false);
		}
		LOGGER.debug("Sentiment is not found for id: " + id);
		LOGGER.info("Exiting from SentimentController find method");
		return new ResponseDto<Sentiment>(false);
	}

	@RequestMapping(value = "/findSentiment", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<SentimentOutputModelFull> findSentiment(Long id) {
		LOGGER.info("Inside SentimentController findSentiment method");
		Sentiment sentiment = null;
		try {
			sentiment = sentimentBusinessDelegate.read(id);
			if (sentiment != null) {
//				try {
//					sentiment.setImage( ThumbnailUtil.getSentimentImageThumbnail(sentiment.getImage(),sentiment.getImageType()));
//				} catch (IOException e) {
//				}
				LOGGER.debug("Sentiment is found for id: " + id);
				LOGGER.info("Exiting from SentimentController find method");
				SentimentOutputModelFull model =SentimentOutputModelFull.getModel(sentiment);
				model.prepareReferences(sentiment.getAttachments());
				sentiment.setAttachments(null);
				List<Opinion> opinions = opinionBusinessDelegate
						.getOpinionForSentiment(id);
				if (opinions != null) {
					Collections.sort(opinions);
					for (Opinion entity : opinions) {
						if (entity.getLink() != null) {
							model.addReferences(entity);
						}
					}
				}
				return new ResponseDto<SentimentOutputModelFull>(true, model);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in finding Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController find method");

			return new ResponseDto<SentimentOutputModelFull>(false);
		}
		LOGGER.debug("Sentiment is not found for id: " + id);
		LOGGER.info("Exiting from SentimentController find method");
		return new ResponseDto<SentimentOutputModelFull>(false);
	}

	/**
	 * Web service is to get all {@link Sentiment}
	 * 
	 * @return
	 */
	//@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Sentiment> getall() {
		LOGGER.info("Inside SentimentController getAll method");
		List<Sentiment> sentiments = null;
		try {
			sentiments = sentimentBusinessDelegate.readAll();
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getAll Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController getAll method");

			return new ResponseDto<Sentiment>(false);
		}
		if (sentiments != null) {
			LOGGER.debug(sentiments.size() + " sentiment is found");
			LOGGER.info("Exiting from SentimentController getAll method");
			return new ResponseDto<Sentiment>(true, sentiments);
		}
		LOGGER.debug("No sentiment is found");
		LOGGER.info("Exiting from SentimentController getAll method");
		return new ResponseDto<Sentiment>(false);
	}

	@RequestMapping(value = "/findByType", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Sentiment> findByType(String type) {
		LOGGER.info("Inside SentimentController findByType method");
		List<Sentiment> sentiments = null;
		try {
			sentiments = sentimentBusinessDelegate.findByType(type);
		} catch (Exception exception) {
			LOGGER.error("Exception in findByType Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController findbyType method");

			return new ResponseDto<Sentiment>(false);
		}
		if (sentiments != null) {
			LOGGER.debug(sentiments.size() + " sentiment is found");
			LOGGER.info("Exiting from SentimentController find by type method");
			return new ResponseDto<Sentiment>(true, sentiments);
		}
		LOGGER.debug("No sentiment is found");
		LOGGER.info("Exiting from SentimentController findByType method");
		return new ResponseDto<Sentiment>(false);
	}

	/**
	 * Web service is to get all {@link Sentiment} not having sentiment in ids
	 * 
	 * @author Mayank Sharma
	 * @return new ResponseDto<Sentiment>
	 */
	@RequestMapping(value = "/getAllSentimentNotIn", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Sentiment> getAllSentimentNotIn(
			@RequestBody Set<Long> ids) {
		LOGGER.info("Inside SentimentController getAllSentimentNotIn method");
		List<Sentiment> sentiments = null;
		try {
			sentiments = sentimentBusinessDelegate.getAllSentimentNotIn(ids);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getAllSentimentNotIn Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController getAllSentimentNotIn method");

			return new ResponseDto<Sentiment>(false);
		}
		if (sentiments != null) {
			LOGGER.debug(sentiments.size() + " Sentiment found");
			LOGGER.info("Exiting from SentimentController getAllSentimentNotIn method");
			return new ResponseDto<Sentiment>(true, sentiments);
		}
		LOGGER.debug("No sentiment found");
		LOGGER.info("Exiting from SentimentController getAllSentimentNotIn method");
		return new ResponseDto<Sentiment>(false);
	}

	/**
	 * Web service is to update status of sentiment by id
	 * 
	 * @author Mayank Sharma
	 * @param allRequestParams
	 * @return
	 */
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Sentiment> deleteFromDB(
			@RequestParam Map<String, Object> allRequestParams) {
		LOGGER.info("Inside SentimentController updateStatus method");
		Long id = Long.parseLong((String) allRequestParams.get("id"));
		String status = (String) allRequestParams.get("status");
		if (id != 0 && status != null) {
			try {
				sentimentBusinessDelegate.updateSentimentStatus(id, status);
			} catch (Exception exception) {
				exception.printStackTrace();
				LOGGER.error("Exception in updateStatus Sentiment");
				LOGGER.error("Exception: " + exception);
				LOGGER.info("Exiting from SentimentController updateStatus method");

				return new ResponseDto<Sentiment>(false);
			}
			LOGGER.debug("sentiment with id: " + id + " status is changed to "
					+ status);
			LOGGER.info("Exiting from SentimentController updateStatus method");
			return new ResponseDto<Sentiment>(true);
		}
		LOGGER.debug("sentiment with id: " + id + " status is not changed to "
				+ status);
		LOGGER.info("Exiting from SentimentController updateStatus method");
		return new ResponseDto<Sentiment>(false);

	}

	/**
	 * Web service is to get list of indicator attached with sentiment
	 * 
	 * @param id
	 * @return ResponseDto<Category>
	 * @author Mayank Sharma
	 */
	@RequestMapping(value = "/getAllSentimentIndicator", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Set<Category>> getAllSentimentIndicator(
			Long id) {
		LOGGER.info("Inside SentimentController getAllSentimentIndicator method");
		Set<Category> indicadtors = null;
		try {
			indicadtors = sentimentBusinessDelegate.getAllIndicator(id);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getAllSentimentIndicator Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController getAllSentimentIndicator method");

			return new ResponseDto<Set<Category>>(false);
		}
		if (null != indicadtors) {
			LOGGER.debug(indicadtors.size() + " indicators is found");
			LOGGER.info("Exiting from SentimentController getAllSentimentIndicator method");
			return new ResponseDto<Set<Category>>(true, indicadtors);
		} else {
			LOGGER.debug("No indicator is found");
			LOGGER.info("Exiting from SentimentController getAllSentimentIndicator method");
			return new ResponseDto<Set<Category>>(false);
		}
	}

	/**
	 * Web service is to get all {@link Sentiment}
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getRelatedSentiment", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Sentiment> getRelatedSentiment(Long id) {
		LOGGER.info("Inside SentimentController getAll method");
		Set<Sentiment> sentiments = null;
		try {
			sentiments = sentimentBusinessDelegate.getRelatedSentiment(id);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getRelatedSentiment Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController getAll method");

			return new ResponseDto<Sentiment>(false);
		}
		if (sentiments != null) {
			LOGGER.debug(sentiments.size() + " sentiment is found");
			LOGGER.info("Exiting from SentimentController getAll method");
			return new ResponseDto<Sentiment>(true, sentiments);
		}
		LOGGER.debug("No sentiment is found");
		LOGGER.info("Exiting from SentimentController getAll method");
		return new ResponseDto<Sentiment>(false);
	}

	/**
	 * Web service is to get all {@link Sentiment}
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getSentimentSupport", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<String> getSentimentSupport(
			Long sentimentId) {
		LOGGER.info("Inside SentimentController getSentimentSupport method");
		try {
			return new ResponseDto<String>(false,
					sentimentBusinessDelegate.getSentimentSupport(sentimentId));
		} catch (Exception exception) {
			LOGGER.error("Exception in getting Sentiment support");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController getSentimentSupport method");
			return new ResponseDto<String>(false);
		}
	}

	@RequestMapping(value = "/getSentimentList", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<SentimentOutputModel> getSentimentList(
			String type) {
		LOGGER.info("Inside SentimentController getSentimentList method");
		List<SentimentOutputModel> sentimentModels = null;
		try {
			sentimentModels = sentimentBusinessDelegate
					.getAllSentimentByType(type);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in updating Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController getSentimentList method");

			return new ResponseDto<SentimentOutputModel>(false);
		}
		if (sentimentModels != null) {
			LOGGER.debug(sentimentModels.size() + " sentiment is found");
			LOGGER.info("Exiting from SentimentController getSentimentList method");
			return new ResponseDto<SentimentOutputModel>(true, sentimentModels);
		}
		LOGGER.debug("No sentiment is found");
		LOGGER.info("Exiting from SentimentController getSentimentList method");
		return new ResponseDto<SentimentOutputModel>(false);
	}

	@RequestMapping(value = "/getTestSentiment", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Sentiment getTestSentiment() {

		return getDummySentiment();
	}

	@RequestMapping(value = "/saveNewsFeed", method = RequestMethod.POST)
	public @ResponseBody void saveNewsFeed() {

		sentimentBusinessDelegate.saveNewsFeed();

	}

	
	
	@RequestMapping(value = "/downloadSentimentImage", method = RequestMethod.GET)
	public @ResponseBody byte[] downloadUserImage(
			@RequestParam("sentimentId") Long sentimentId,
			@RequestParam("imageType") String imageType,
			@RequestParam("imageSize") String imageSize) {
		LOGGER.info("Inside UserController downloadUserImage method");
		Sentiment sentiment = null;
		try {
			// Need to add delegate method
			sentiment = sentimentBusinessDelegate.read(sentimentId);
			if (sentiment == null) {
				LOGGER.debug("Sentiment not found sentiment id: " + sentimentId);
				LOGGER.info("Exiting from SentimentController downloadSentimentImage method");
				return null;
			}
			byte[] bytes = null;
			String fileType = null;
			fileType = "jpg";
			bytes = sentiment.getImage();

			if (imageSize.equals(ImageSizeEnum.THUMBNAIL.getImageSize())) {
				return ThumbnailUtil.getImageThumbnail(bytes, fileType);
			} else if (imageSize.equals(ImageSizeEnum.STANDARD.getImageSize())) {
				return ThumbnailUtil.getImageOriginal(bytes, fileType);
			} else if (imageSize.equals(ImageSizeEnum.SNAPSHOT.getImageSize())) {
				return ThumbnailUtil.getImageSnapshot(bytes, fileType);
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in downloadSentimentImage with user id: "
					+ sentimentId);
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController downloadSentimentImage method");
			return null;
		}
		LOGGER.info("Exiting from SentimentController downloadSentimentImage method");
		return null;
	}

	private Sentiment getDummySentiment() {

		Sentiment sentiment = new Sentiment();
		sentiment.setSubject("Subject for test Sentiment");
		sentiment.setDescription("Description for test Sentiment");
		sentiment.setType(SentimentType.TEMPORARY.toString());
		return sentiment;

	}

	@RequestMapping(value = "/loadSentimentNewsRSS", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Boolean> loadSentiment() {
		sentimentNewsLoaderService.loadNewsForAllSentiments();
		LOGGER.info("loadSentimentNewsRSS ----");
		return new ResponseDto<Boolean>(true);
		
	}
	/**
	 * The file should be under /testdata/sentiment
	 * 
	 * @param fileName
	 * @return
	 */
	@RequestMapping(value = "/loadSentiment", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Sentiment> loadSentiment(
			@RequestParam String sentimentName, @RequestParam String sentimentType) {
		String filePath = "." + File.separator + "testdata" + File.separator
				+ "sentiment" + File.separator +sentimentType+File.separator + sentimentName+File.separator + sentimentName+"- SentimentData.xlsx";
		LOGGER.info("Sentiment File path : "+filePath);
		SentimentInputModel sentimentsData = sentimentInputReader
				.loadSentiment(filePath);
		String imagesPath = "." + File.separator + "testdata" + File.separator
				+ "sentiment" + File.separator +sentimentType+File.separator+sentimentName + File.separator +sentimentName;	
		// prepareSentimet object here from SentimentData
		LOGGER.info("Sentiment Image path : "+imagesPath);
		Sentiment sentiment = sentimentInputReader
				.getSentimentFromInputModel(sentimentsData,imagesPath);
		LOGGER.info("Inside SentimentController loadSentiment method");
		if (sentiment == null) {
			LOGGER.error("Sentiment is null",
					new Exception("Sentiment is null"));
			return new ResponseDto<Sentiment>(false);
		}
		if (sentiment.getImage() == null) {
			LOGGER.error("Image is null", new Exception(
					"Image is required to add sentiment"));
			return new ResponseDto<Sentiment>(false);
		}
		try {
			LOGGER.debug("Received " + "Image size "
					+ sentiment.getImage().length);
			LOGGER.debug("Sentiment to be saved " + sentiment.getSubject());
			sentiment.setId(null);
			sentiment.setState(SentimentState.ALIVE.getValue());
			sentiment.setImage(sentiment.getImage());
			sentiment.setImageType(sentiment.getImageType());
			sentiment = sentimentBusinessDelegate.save(sentiment);
			LOGGER.debug("Sentiment and polls saved/updated");
			return new ResponseDto<Sentiment>(true, sentiment);

		} 
		catch (IOException ioException) {
			LOGGER.error("Error saving sentiment/poll", ioException);
			LOGGER.error("Exiting add sentiment");

			return new ResponseDto<Sentiment>(false);
		} 
		catch (Exception exception) {
			LOGGER.error("Error saving sentiment/poll", exception);
			LOGGER.info("Exiting from SentimentController loadSentiment method");
			return new ResponseDto<Sentiment>(false);
		}
	}

	@RequestMapping(value = "/loadAllSentiment", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<List> loadAllSentiment(
			@RequestParam String type) {
		String directoryPath = "." + File.separator + "testdata"
				+ File.separator + "sentiment" + File.separator + type;
		File folder = new File(directoryPath);
		int filecount = 0;
		List<String> failedFiles = new ArrayList<String>();
		List<String> successFiles =new ArrayList<String>();
    	// get all the sub directories from a directory
	    File[] dirList = folder.listFiles();
	    List<File> directories = new ArrayList<File>();
	    for (File file : dirList) {
	        if (file.isDirectory()) {
	        	directories.add(file);
	        }
	    }	    
	    if(!directories.isEmpty()){
	    	for(File dir : directories){
		    	List<File> files = (List<File>) FileUtils.listFiles(dir,
						new String[] { "xlsx" }, true);
				for (File file : files) {
					System.out.println("Total No.Of files reading " + files.size());
					try {
						 
						LOGGER.info("Cuurently reading file from: " + file.getCanonicalPath());
						LOGGER.info("File Name "+ file.getName());
						SentimentInputModel sentimentsData = sentimentInputReader
								.loadSentiment(file.getCanonicalPath());
						String formattedFileName = file.getName().split("-")[0];
						String imagesPath = "." + File.separator + 
								"testdata" + File.separator + "sentiment" + File.separator + type +  File.separator + formattedFileName.trim() +  File.separator +
								formattedFileName.trim();
						// prepareSentimet object here from SentimentData
						Sentiment sentiment = sentimentInputReader
								.getSentimentFromInputModel(sentimentsData,imagesPath);
						LOGGER.info("Inside SentimentController loadSentiment method");
						if (sentiment == null) {
							LOGGER.error("Sentiment object is  null for the file"
									+ file.getName());
							// return new ResponseDto<Sentiment>(false);
						}
						if (sentiment.getImage() == null) {
							LOGGER.error("Image is null", new Exception(
									"Image is required to add sentiment"));
							// return new ResponseDto<Sentiment>(false);
						} else {
							filecount++;
							LOGGER.debug("Received " + "Image size "
									+ sentiment.getImage().length);
							LOGGER.debug("Sentiment to be saved "
									+ sentiment.getSubject());
							sentiment.setId(null);
							sentiment.setState(SentimentState.ALIVE.getValue());
							sentiment.setImage(sentiment.getImage());
							sentiment.setImageType(sentiment.getImageType());
							sentiment = sentimentBusinessDelegate.save(sentiment);
							successFiles.add(file.getName());
							LOGGER.debug("Sentiment and polls are lodaed");
						}
					} catch (IOException ioException) {
						LOGGER.error("Error saving sentiment/poll", ioException);
						LOGGER.error("Exiting add sentiment");
						failedFiles.add(file.getName());
					} catch (Exception exception) {
						LOGGER.error("Error loading sentiment/poll", exception);
						LOGGER.info("Exiting from SentimentController loadSentiment method");
						failedFiles.add(file.getName());
					}
				}
			
		    
	    	}
	    	
	    }
			return new ResponseDto<List>(true, failedFiles,"Failed Files");
	}

	/**
	 * The file should be under /testdata/sentiment
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = "/loadSentimentNews", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Boolean> loadSentimentNews(
@RequestParam("file") MultipartFile file)
			throws IOException {

		SentimentInputModel sentimentsData = sentimentInputReader.loadFile(file.getInputStream());

		// prepareSentimet object here from SentimentData
		List<Sentiment> sentiments = sentimentInputReader.getSentimentFromInputModelForNews(sentimentsData);
		LOGGER.info("Inside SentimentController loadSentiment method");

		if (sentiments != null && !sentiments.isEmpty()) {
			for (Sentiment sentiment : sentiments) {
				try {
					sentiment = sentimentBusinessDelegate.update(sentiment);
					LOGGER.debug("Sentiment and polls saved/updated");
				} catch (IOException ioException) {
					LOGGER.error("Error saving sentiment/poll", ioException);
					LOGGER.error("Exiting add sentiment");
					return new ResponseDto<Boolean>(false);
				} catch (Exception exception) {
					LOGGER.error("Error saving sentiment/poll", exception);
					LOGGER.info("Exiting from SentimentController loadSentiment method");
					return new ResponseDto<Boolean>(false);
				}
			}
		}
		return new ResponseDto<Boolean>(true);
	}
	
	@RequestMapping(value = "/getPagedSentimentList", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<SentimentOutputModel> getPagedSentimentList(
			String type,int from,int to) {
		LOGGER.info("Inside SentimentController getPagedSentimentList method");
		List<SentimentOutputModel> sentimentModels = null;
		try {
			sentimentModels = sentimentBusinessDelegate
					.getPaginatedSentimentByType(type, from, to);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getPagedSentimentList Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController getPagedSentimentList method");

			return new ResponseDto<SentimentOutputModel>(false);
		}
		if (sentimentModels != null) {
			LOGGER.debug(sentimentModels.size() + " sentiment is found");
			LOGGER.info("Exiting from SentimentController getPagedSentimentList method");
			return new ResponseDto<SentimentOutputModel>(true, sentimentModels);
		}
		LOGGER.debug("No sentiment is found");
		LOGGER.info("Exiting from SentimentController getPagedSentimentList method");
		return new ResponseDto<SentimentOutputModel>(false);
	}
	@RequestMapping(value = "/getSentimentListSizeByType", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Integer> getSentimentListSizeByType(
			String type) {
		LOGGER.info("Inside SentimentController getSentimentListSizeByType method");
		int sentimentListSize;
		try {
			sentimentListSize =  (sentimentBusinessDelegate.findByType(type) != null ? sentimentBusinessDelegate.findByType(type).size():0 );
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getPagedSentimentList Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController getSentimentListSizeByType method");

			return new ResponseDto<Integer>(false);
		}
		if (sentimentListSize > 0 ) {
			LOGGER.debug(sentimentListSize + " sentiment is found");
			LOGGER.info("Exiting from SentimentController getSentimentListSizeByType method");
			return new ResponseDto<Integer>(true, sentimentListSize);
		}
		LOGGER.debug("No sentiment is found");
		LOGGER.info("Exiting from SentimentController getSentimentListSizeByType method");
		return new ResponseDto<Integer>(false);
	}
	
	@RequestMapping(value = "/getSentimentNameList", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<SentimentOutputModel> getSentimentNameList(
			String type) {
		LOGGER.info("Inside SentimentController getSentimentNameList method");
		List<SentimentOutputModel> sentimentModels = null;
		try {
			sentimentModels = sentimentBusinessDelegate
					.getAllSentimentByType(type);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in updating Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController getSentimentNameList method");

			return new ResponseDto<SentimentOutputModel>(false);
		}
		if (sentimentModels != null) {
			LOGGER.debug(sentimentModels.size() + " sentiment is found");
			LOGGER.info("Exiting from SentimentController getSentimentNameList method");
			return new ResponseDto<SentimentOutputModel>(true, sentimentModels);
		}
		LOGGER.debug("No sentiment is found");
		LOGGER.info("Exiting from SentimentController getSentimentNameList method");
		return new ResponseDto<SentimentOutputModel>(false);
	}
	
	
	
	
	/**
	 * Web service is to hide the sentiment
	 * 
	 * @author Jahid Ali
	 * @param allRequestParams
	 * @return
	 */
	@RequestMapping(value = "/hideSentiment", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> hideSentiment(String sentimentName, String sentimentType) {
		LOGGER.info("Inside SentimentController hideSentiment method");
		boolean result  = false;
		try {
			result  = sentimentBusinessDelegate.changeSentimentState(sentimentName, sentimentType, SentimentState.HIDDEN);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in hideSentiment Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController updateStatus method");
				return new ResponseDto<Object>(false);
		}
		LOGGER.info("Exiting from SentimentController hideSentiment method");
		return new ResponseDto<Object>(result);
	}

	/**
	 * Web service is to hide the sentiment
	 * 
	 * @author Jahid Ali
	 * @param allRequestParams
	 * @return
	 */
	@RequestMapping(value = "/deleteSentiment", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> deleteSentiment(String sentimentName, String sentimentType) {
		LOGGER.info("Inside SentimentController hideSentiment method");
		boolean result  = false;
		try {
			result  = sentimentBusinessDelegate.changeSentimentState(sentimentName, sentimentType, SentimentState.HIDDEN);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in hideSentiment Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController updateStatus method");
				return new ResponseDto<Object>(false);
		}
		LOGGER.info("Exiting from SentimentController hideSentiment method");
		return new ResponseDto<Object>(result);
	}

}
