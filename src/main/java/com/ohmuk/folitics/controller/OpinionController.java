package com.ohmuk.folitics.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ohmuk.folitics.businessDelegate.interfaces.IOpinionBusinessDelegate;
import com.ohmuk.folitics.charting.beans.OpinionAggregation;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.enums.OpinionState;
import com.ohmuk.folitics.enums.SentimentState;
import com.ohmuk.folitics.enums.SentimentType;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.ouput.model.GetAllOpinionOutputModel;
import com.ohmuk.folitics.ouput.model.GetOpinionOutputModel;
import com.ohmuk.folitics.ouput.model.OpinionOutputModel;
import com.ohmuk.folitics.ouput.model.PastOpinionOutputModel;
import com.ohmuk.folitics.service.IOpinionService;
import com.ohmuk.folitics.util.AttachmentUtil;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.FileLoadingUtils;
import com.ohmuk.folitics.util.FoliticsUtils;
import com.ohmuk.folitics.util.PollUtil;

/**
 * @author Abhishek
 *
 */
@Controller
@RequestMapping("/opinion")
public class OpinionController {
    final static Logger logger = LoggerFactory.getLogger(OpinionController.class);
    @Autowired
    private IOpinionBusinessDelegate businessDelegate;

    @RequestMapping
    public String getOpinionPage() {
        return "opinion-page";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Opinion> getAdd() {
        List<Opinion> opinions = new ArrayList<>();
        opinions.add(getTestOpinion());
        return new ResponseDto<Opinion>(true, opinions);
    }

    /**
     * Spring web service(POST) to add a opinion
     * 
     * @author Abhishek
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion opinion
     * @return com.ohmuk.folitics.dto.ResponseDto.ResponseDto<Opinion>(Boolean ,
     *         List<Opinion>)
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<OpinionOutputModel> add(@RequestBody Opinion opinion) {
        try {

            opinion.setAgreeDisagrePoll(PollUtil.createOpinionPoll(opinion.getUser()));
            List<Opinion> opinions = businessDelegate.getOpinionForSentimentAndUser(opinion.getSentiment().getId(),
                    opinion.getUser().getId());
           
            List<Opinion> allOpinions = businessDelegate.readAll();
            
            if(allOpinions != null && allOpinions.size() > 0 ){
            	for(Opinion op : allOpinions){
            		
            		if(FoliticsUtils.compareStringsJaroWinkler(opinion.getSubject(), op.getSubject()) > 0.85){
            			 return new ResponseDto<OpinionOutputModel>(
                                 "You cannot create opinion with the same subject", false);
            			
            		}
            	}
            }
          
            if (opinions != null && opinions.size() < IOpinionService.OpinionLimit) {
                opinion = businessDelegate.create(opinion);
            } else {
                return new ResponseDto<OpinionOutputModel>(
                        "You have reached the maximum limit of submitting opinion on this sentiment", false);
            }

        } catch (Exception exception) {
            exception.printStackTrace();
            logger.debug("Error while saving opinion with opinion type = " + opinion.getType() + ", sentiment id = "
                    + opinion.getSentiment().getId() + " and user id = " + opinion.getUser().getId());
            logger.error("Exception in adding Opinion");
            logger.error("Exception: " + exception);
            return new ResponseDto<OpinionOutputModel>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
        }
        if (opinion != null) {
        	OpinionOutputModel oom = OpinionOutputModel.getModel(opinion);
            return new ResponseDto<OpinionOutputModel>(true, oom);
        }
        return new ResponseDto<OpinionOutputModel>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    /**
     * Spring web service(POST) to add a opinion with multipart request
     * 
     * @author Abhishek
     * @param Multipart
     *            file
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion opinion
     * @return com.ohmuk.folitics.dto.ResponseDto.ResponseDto<Opinion>(Boolean ,
     *         List<Opinion>)
     * @throws Exception
     */
    @RequestMapping(value = "/addOpinion", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<OpinionOutputModel> add(@RequestPart(value = "file") MultipartFile file,
            @RequestPart(value = "opinion") Opinion opinion) {

        try {
            opinion.setAttachment(AttachmentUtil.prepareAttachment(file, opinion.getAttachment()));
            opinion.setAgreeDisagrePoll(PollUtil.createOpinionPoll(opinion.getUser()));
            opinion = businessDelegate.create(opinion);

            if (opinion != null) {
            	OpinionOutputModel oom = OpinionOutputModel.getModel(opinion);
                return new ResponseDto<OpinionOutputModel>(true, oom);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseDto<OpinionOutputModel>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    /**
     * Spring web service(POST) to edit a opinion
     * 
     * @author Abhishek
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion opinion
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<Opinion> edit(@RequestBody Opinion opinion) {
        try {
            opinion = businessDelegate.update(opinion);
        } catch (Exception exception) {
            logger.error("Exception in editing Opinion");
            logger.error("Exception: " + exception);
            return new ResponseDto<Opinion>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
        }
        if (opinion != null) {
            return new ResponseDto<Opinion>(true, opinion);
        }
        return new ResponseDto<Opinion>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    @RequestMapping(value = "/deleteById", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Opinion> delete(Long id) {
        Opinion opinion = null;
        try {
            opinion = businessDelegate.delete(id);
        } catch (Exception exception) {
            logger.error("Exception in delete by id Opinion");
            logger.error("Exception: " + exception);
            return new ResponseDto<Opinion>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
        }
        if (opinion != null) {
            if (opinion.getState().equals(ComponentState.DELETED.getValue())) {
                return new ResponseDto<Opinion>(true);
            }
        }
        return new ResponseDto<Opinion>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<Opinion> delete(@Validated @RequestBody Opinion opinion) {
        try {
            opinion = businessDelegate.delete(opinion);
        } catch (Exception exception) {
            logger.error("Exception in delete Opinion");
            logger.error("Exception: " + exception);
            return new ResponseDto<Opinion>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
        }
        if (opinion != null) {
            if (opinion.getState().equals(ComponentState.DELETED.getValue())) {
                return new ResponseDto<Opinion>(true);
            }
        }
        return new ResponseDto<Opinion>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    @RequestMapping(value = "/deleteFromDbById", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Opinion> deleteFromDB(Long id) {
        try {
            if (businessDelegate.deleteFromDB(id)) {
                return new ResponseDto<Opinion>(true);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ResponseDto<Opinion>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    @RequestMapping(value = "/deleteFromDb", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<Opinion> deleteFromDB(@Validated @RequestBody Opinion opinion) {
        try {
            if (businessDelegate.deleteFromDB(opinion)) {
                return new ResponseDto<Opinion>(true);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ResponseDto<Opinion>(false);
    }

    //@RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Opinion> getall() {
        List<Opinion> opinions = null;
        try {
            opinions = businessDelegate.readAll();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (opinions != null) {
            return new ResponseDto<Opinion>(true, opinions);
        }
        return new ResponseDto<Opinion>(false);
    }

    @RequestMapping(value = "/getTopMostOpinion", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Opinion> getTopMostOpinion(Long sentimentId) {
        return null;
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<GetOpinionOutputModel> find(Long id) {
        Opinion opinion = null;
        GetOpinionOutputModel opinionBeanOutputModel = null;
        try {
            opinion = businessDelegate.read(id);
            if (opinion != null){
                opinionBeanOutputModel = GetOpinionOutputModel.getModel(opinion);
                opinionBeanOutputModel.setImageUrl(FileLoadingUtils.returnStandardImagePath(opinionBeanOutputModel.getImageUrl()));
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (opinionBeanOutputModel != null) {
            return new ResponseDto<GetOpinionOutputModel>(true, opinionBeanOutputModel);
        }
        return new ResponseDto<GetOpinionOutputModel>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    @RequestMapping(value = "/getTestOpinion", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Opinion getTestOpinion() {
        return getDummyOpinion();
    }

    private Opinion getDummyOpinion() {
        Opinion opinion = new Opinion();
        Sentiment sentiment = new Sentiment();
        sentiment.setId(1l);
        sentiment.setCreateTime(DateUtils.getSqlTimeStamp());
        sentiment.setSubject("Subject for test Sentiment");
        sentiment.setDescription("Description for test Sentiment");
        sentiment.setCreated_By(new Long("100"));
        sentiment.setState(SentimentState.ALIVE.getValue());
        sentiment.setStartTime(DateUtils.getSqlTimeStamp());
        sentiment.setEndTime(DateUtils.getSqlTimeStamp());
        sentiment.setType(SentimentType.TEMPORARY.toString());
        Set<Sentiment> sentiments = new HashSet<Sentiment>();
        sentiments.add(sentiment);

        OpinionState os = OpinionState.NEW;
        opinion.setSubject("Subject of Opinion");
        opinion.setText("Text of the opinion");
        opinion.setState(os.getValue());
        return opinion;
    }

    @RequestMapping(value = "/myopinion", method = RequestMethod.GET)
    @ResponseBody
    ResponseDto<Object> getMyTasks(Long userId) {

        List<Opinion> list;

        try {
            if (userId == null) {
                throw (new MessageException("No or invalid arguments provided"));
            }

            list = businessDelegate.getOpinionByUser(userId);

        } catch (MessageException e) {
            logger.error("CustomException while fetching the tasks " + e);
            return new ResponseDto<Object>(false, null, FoliticsUtils.SOMETHING_WENT_WRONG_ERROR);
        }

        catch (Exception e) {
            logger.error("Exception while fetching the tasks " + e);
            return new ResponseDto<Object>(false, null, FoliticsUtils.SOMETHING_WENT_WRONG_ERROR);
        }

        return new ResponseDto<Object>(true, list, "List Fetched Successfully");
    }

    @RequestMapping(value = "/getOpinionsByType", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<GetAllOpinionOutputModel> getOpinions(Long sentimentId, String opinionType,
            int count) {
        logger.info("Inside OpinionController getOpinions method");
        List<Opinion> opinions = null;
        List<GetAllOpinionOutputModel> getAllOpinionOutputModels = new ArrayList<GetAllOpinionOutputModel>();
        try {
            opinions = businessDelegate.getOpinionsByType(sentimentId, opinionType, count);
        } catch (Exception exception) {
            logger.error("Exception in getOpinions for Sentiment");
            logger.error("Exception: " + exception);
            logger.info("Exiting from OpinionController getOpinions method");

            return new ResponseDto<GetAllOpinionOutputModel>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
        }
        if (opinions != null) {
            for (Opinion opinion : opinions) {
                getAllOpinionOutputModels.add(GetAllOpinionOutputModel.getModel(opinion));
            }
            logger.debug(opinions.size() + " opinions exsits");
            logger.info("Exiting from OpinionController getOpinions method");
            return new ResponseDto<GetAllOpinionOutputModel>(true, getAllOpinionOutputModels);
        }
        logger.debug("No sentiment is found");
        logger.info("Exiting from OpinionController getOpinions method");
        return new ResponseDto<GetAllOpinionOutputModel>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    @RequestMapping(value = "/getOpinions", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Opinion> getOpinions(Long id) {
        logger.info("Inside OpinionController getOpinions method");
        List<Opinion> opinions = null;
        try {
            opinions = businessDelegate.getOpinionForSentiment(id);
        } catch (Exception exception) {
            logger.error("Exception in getOpinions for Sentiment");
            logger.error("Exception: " + exception);
            logger.info("Exiting from OpinionController getOpinions method");

            return new ResponseDto<Opinion>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
        }
        if (opinions != null) {
            logger.debug(opinions.size() + " opinions exsits");
            logger.info("Exiting from OpinionController getOpinions method");
            return new ResponseDto<Opinion>(true, opinions);
        }
        logger.debug("No sentiment is found");
        logger.info("Exiting from OpinionController getOpinions method");
        return new ResponseDto<Opinion>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    @RequestMapping(value = "/getOpinionsModel", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<OpinionOutputModel> getOpinionsModel(Long id) {
        logger.info("Inside OpinionController getOpinions method");
        List<Opinion> opinions = null;
        List<OpinionOutputModel> opinionsModel = new ArrayList<OpinionOutputModel>();
        try {
            opinions = businessDelegate.getOpinionForSentiment(id);
            if (opinions != null) {
                for (Opinion op : opinions) {
                    opinionsModel.add(OpinionOutputModel.getModel(op));
                }
            }
            logger.debug(opinionsModel.size() + " opinions exsits");
            logger.info("Exiting from OpinionController getOpinions method");
            return new ResponseDto<OpinionOutputModel>(true, opinionsModel);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception in getOpinions for Sentiment");
            logger.error("Exception: " + e);
            logger.info("Exiting from OpinionController getOpinions method");
            return new ResponseDto<OpinionOutputModel>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
        }
    }

    @RequestMapping(value = "/getOpinionAggregation", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<OpinionAggregation> getOpinionAggregation(Long sentimentId) {
        logger.info("Inside OpinionController getOpinionAggregation method");
        OpinionAggregation opinionAggregation = null;
        try {
            opinionAggregation = businessDelegate.getOpinionsAggregate(sentimentId);
        } catch (Exception exception) {
            logger.error("Exception in getOpinionAggregation for Sentiment");
            logger.error("Exception: " + exception);
            logger.info("Exiting from OpinionController getOpinionAggregation method");
            return new ResponseDto<OpinionAggregation>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
        }
        if (opinionAggregation != null) {
            logger.info("Exiting from OpinionController getOpinionAggregation method");
            return new ResponseDto<OpinionAggregation>(true, opinionAggregation);
        }
        logger.debug("No opinion aggregation caculated for sentiemnt is found");
        logger.info("Exiting from OpinionController getOpinionAggregation method");
        return new ResponseDto<OpinionAggregation>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    @RequestMapping(value = "/getOpinionComponent", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Opinion> getOpinionComponent(Long sentimentId, Long componentId,
            String componentType) {
        logger.info("Inside OpinionController getOpinionComponent method");
        List<Opinion> opinions = null;
        try {
            opinions = businessDelegate.getOpinionComponent(sentimentId, componentId, componentType);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("Exception in getOpinionComponent for Sentiment");
            logger.error("Exception: " + e);
            logger.info("Exiting from OpinionController getOpinionComponent method");
        }
        if (opinions != null) {
            return new ResponseDto<Opinion>(true, opinions);
        }
        return new ResponseDto<Opinion>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    @RequestMapping(value = "/getLatestOpinions", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<List<OpinionOutputModel>> getLatestOpinions(int count) {
        logger.info("Inside from OpinionController getLatestOpinion method");
        List<OpinionOutputModel> opinions = null;
        try {
            opinions = businessDelegate.getLatestOpinions(count);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("Exception in getLatestOpinion");
            logger.error("Exception: " + e);
            logger.info("Exiting from OpinionController getLatestOpinion method");
        }
        if (opinions != null) {
            return new ResponseDto<List<OpinionOutputModel>>(true, opinions);
        }
        return new ResponseDto<List<OpinionOutputModel>>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    @RequestMapping(value = "/getPastOpinionOutputModel", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<PastOpinionOutputModel> getPastOpinionOutputModel(Long id) {

        logger.info("Inside OpinionController getPastOpinionOutputModel method");
        PastOpinionOutputModel model;
        try {
            model = businessDelegate.getPastOpinionOutputModel(id);

        } catch (Exception exception) {
            logger.error("Exception in OpinionController getPastOpinionOutputModel with user id: " + id);
            logger.error("Exception: " + exception);
            logger.info("Exiting from OpinionController getPastOpinionOutputModel method");
            return new ResponseDto<PastOpinionOutputModel>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
        }
        if (model != null) {
            logger.info("Exiting from OpinionController getPastOpinionOutputModel method");
            return new ResponseDto<PastOpinionOutputModel>(true, model);
        }
        logger.info("Exiting from OpinionController getPastOpinionOutputModel method");
        return new ResponseDto<PastOpinionOutputModel>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }

    /**
     * Web service is for forgotPassword {@link User} via email
     * 
     * @param id
     * @return ResponseDto<User>
     */
    @RequestMapping(value = "/getOpinionCountByUserId", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Integer> getByUserId(Long id) {

        logger.info("Inside OpinionController getOpinionCountByUserId method");
        int opinionCount = 0;
        try {
            List<Opinion> opinions = businessDelegate.readAll();

            if (opinions != null && !opinions.isEmpty()) {

                for (Opinion opinion : opinions) {

                    User user = opinion.getUser();
                    if (user.getId().longValue() == id.longValue()) {
                        opinionCount++;
                    }
                }

            }

        } catch (Exception exception) {
            logger.error("Exception in OpinionController getOpinionCountByUserId with user id: " + id);
            logger.error("Exception: " + exception);
            logger.info("Exiting from OpinionController getOpinionCountByUserId method");
            return new ResponseDto<Integer>(false);
        }
        if (opinionCount > 0) {
            logger.info("Exiting from OpinionController getOpinionCountByUserIdmethod");
            return new ResponseDto<Integer>(true, opinionCount);
        }
        logger.info("Exiting from OpinionController getOpinionCountByUserId method");
        return new ResponseDto<Integer>(false);
    }

    @RequestMapping(value = "getOpinionsByUserId", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<List<GetOpinionOutputModel>> getOpinionsByUserId(Long id) {
        List<Opinion> opinions = null;
        List<GetOpinionOutputModel> opinionBeanOutputModels = new ArrayList<>();
        try {
            opinions = businessDelegate.getOpinionByUser(id);
            if (opinions != null && !opinions.isEmpty())

                for (Opinion opinion : opinions) {
                    GetOpinionOutputModel opinionBeanOutputModel = null;
                    opinionBeanOutputModel = GetOpinionOutputModel.getModel(opinion);
                    opinionBeanOutputModels.add(opinionBeanOutputModel);
                }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (!opinionBeanOutputModels.isEmpty()) {
            return new ResponseDto<List<GetOpinionOutputModel>>(true, opinionBeanOutputModels);
        } else
            return new ResponseDto<List<GetOpinionOutputModel>>(false);
    }

    @RequestMapping(value = "/searchOpinionbykeyword", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<OpinionOutputModel> searchOpinionBykeyword(String search,
            @RequestParam(value = "sentimentId", required = false) Long sentimentId) {
        logger.info("Inside OpinionController searchOpinionbykeyword method");
        LinkedHashSet<Opinion> opinions = null;
        List<OpinionOutputModel> opinionsModel = new ArrayList<OpinionOutputModel>();
        try {
            opinions = businessDelegate.searchOpinion(search, sentimentId);
            if (opinions != null) {
                for (Opinion op : opinions) {
                    opinionsModel.add(OpinionOutputModel.getModel(op));
                }
            }
            
            logger.info("search opinion sentiment Id :::"+opinionsModel.get(0).getSentimentId());
            logger.info("search opinion sentiment Name :::"+opinionsModel.get(0).getSentimentName());
            
            logger.debug(opinionsModel.size() + " opinions exsits");
            logger.info("Exiting from OpinionController searchOpinionbykeyword method");
            return new ResponseDto<OpinionOutputModel>(true, opinionsModel);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception in searchOpinionbykeyword for Sentiment");
            logger.error("Exception: " + e);
            logger.info("Exiting from OpinionController searchOpinionbykeyword method");
            return new ResponseDto<OpinionOutputModel>(false);
        }
    }

    @RequestMapping(value = "/updateOpinionText", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<OpinionOutputModel> updateOpinionText(@RequestParam Long opinionId,
            @RequestParam String opinionText) {
        logger.info("Inside OpinionController updateOpinionText method");
        Opinion opinion = null;
        OpinionOutputModel opinionsModel = null;
        try {
            opinion = businessDelegate.read(opinionId);
            if (opinion != null) {
                opinion.setText(opinionText.trim());
                opinion = businessDelegate.update(opinion);
                opinionsModel = OpinionOutputModel.getModel(opinion);
                logger.debug("Opinion text updated to " + opinionsModel.getText());
            }
            logger.info("Exiting from OpinionController updateOpinionText method");
            return new ResponseDto<OpinionOutputModel>(true, opinionsModel);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception in updateOpinionText for Opinion");
            logger.error("Exception: " + e);
            logger.info("Exiting from OpinionController updateOpinionText method");
            return new ResponseDto<OpinionOutputModel>(false);
        }
    }

    @RequestMapping(value = "/updateOpinionSubject", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<OpinionOutputModel> updateOpinionSubject(@RequestParam Long opinionId,
            @RequestParam String subject) {
        logger.info("Inside OpinionController updateOpinionSubject method");
        Opinion opinion = null;
        OpinionOutputModel opinionsModel = null;
        try {
            opinion = businessDelegate.read(opinionId);
            if (opinion != null) {
                opinion.setSubject(subject.trim());
                opinion = businessDelegate.update(opinion);
                opinionsModel = OpinionOutputModel.getModel(opinion);
                logger.debug("Opinion subject updated to " + opinionsModel.getText());
            }
            logger.info("Exiting from OpinionController updateOpinionSubject method");
            return new ResponseDto<OpinionOutputModel>(true, opinionsModel);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception in updateOpinionSubject for Opinion");
            logger.error("Exception: " + e);
            logger.info("Exiting from OpinionController updateOpinionSubject method");
            return new ResponseDto<OpinionOutputModel>(false);
        }
    }

    @RequestMapping(value = "/uploadOpinionImage", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<String> uploadImage(@RequestParam("image") MultipartFile file,
            @RequestParam("sentimentId") Long sentimentId, @RequestParam(value = "userId") Long userId) {
        logger.info("Inside OpinionController uploadImage method");
        String imagePath = "";
        try {
            imagePath = businessDelegate.uploadOpinionImage(file, sentimentId, userId);
        } catch (Exception exception) {
            exception.printStackTrace();
            logger.error("Exception in upload opinion image with sentiment id: " + userId);
            logger.error("Exception: " + exception);
            logger.info("Exiting from UserController uploadImage method");
            return new ResponseDto<String>(false, imagePath);
        }
        if (imagePath != null) {
            logger.debug("User image is uploaded with user id: " + userId);
            logger.info("Exiting from UserController uploadImage method");
            return new ResponseDto<String>(true, imagePath);
        }
        logger.info("Exiting from UserController uploadImage method");
        return new ResponseDto<String>(true, imagePath);
    }

    @RequestMapping(value = "/getRelatedOpinions", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<List<OpinionOutputModel>> getRelatedOpinions(Long id) {
        logger.info("Inside from OpinionController getRelatedOpinions method");
        List<OpinionOutputModel> opinions = null;
        try {
            opinions = businessDelegate.getRelatedOpinions(id);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("Exception in getRelatedOpinions");
            logger.error("Exception: " + e);
            logger.info("Exiting from OpinionController getRelatedOpinions method");
        }
        if (opinions != null) {
            return new ResponseDto<List<OpinionOutputModel>>(true, opinions);
        }
        return new ResponseDto<List<OpinionOutputModel>>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR, false);
    }
}
