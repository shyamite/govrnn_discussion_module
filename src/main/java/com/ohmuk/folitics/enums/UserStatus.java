package com.ohmuk.folitics.enums;

public enum UserStatus {
    ACTIVE("Active"), EMAIL_VERIFIED("VerifiedByEmail"),PENDING_VERIFICATION("Pending_Verification"), DELETED("Deleted"),SUSSPENDED("Suspended");
    
    private String value;

    private UserStatus(String value) {
        this.setValue(value);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static final UserStatus getUserStatus(String value) {
        if (ACTIVE.getValue().equals(value)) {
            return ACTIVE;
        }
        if (PENDING_VERIFICATION.getValue().equals(value)) {
            return PENDING_VERIFICATION;
        }
        if (DELETED.getValue().equals(value)) {
            return DELETED;
        }
        if (SUSSPENDED.getValue().equals(value)) {
            return SUSSPENDED;
        }
        return null;
    }
}
