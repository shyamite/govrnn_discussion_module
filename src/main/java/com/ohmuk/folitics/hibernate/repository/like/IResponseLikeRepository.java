package com.ohmuk.folitics.hibernate.repository.like;

import com.ohmuk.folitics.hibernate.entity.like.ResponseLike;

/**
 * Interface for entity: {@link TaskLike} repository
 * 
 * @author Harish
 *
 */
public interface IResponseLikeRepository extends
		ILikeHibernateRepository<ResponseLike> {

}
