package com.ohmuk.folitics.hibernate.service.rating;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.beans.RatingDataBean;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.rating.RatingId;
import com.ohmuk.folitics.hibernate.entity.rating.SentimentNewsRating;
import com.ohmuk.folitics.hibernate.repository.rating.ISentimentNewsRatingRepository;

/**
 * @author Soumya
 *
 */

@Service
@Transactional
public class SentimentNewsRatingService implements
		IRatingService<RatingDataBean> {

	private static Logger logger = LoggerFactory
			.getLogger(SentimentNewsRatingService.class);

	@Autowired
	private ISentimentNewsRatingRepository repository;

	@Override
	public RatingDataBean create(RatingDataBean ratingDataBean)
			throws MessageException, Exception {
		logger.info("Entered SentimentNewsRating service create method");
		if (ratingDataBean.getComponentId() == null) {
			logger.error("componentId found to be null");
			throw (new MessageException("componentId can't be null"));
		}
		if (ratingDataBean.getUserId() == null) {
			logger.error("userId found to be null");
			throw (new MessageException("userId can't be null"));
		}

		// check whether the entry for same component id and user id already
		// exists in database

		RatingDataBean ratingDataBeanExisting = getByComponentIdAndUserId(ratingDataBean);
		logger.info("ratingDataBeanExisting : "+ratingDataBeanExisting);
		// if no entry exist for rating for SentimentNews of sentiment in
		// database
		if (ratingDataBeanExisting == null) {
			RatingId ratingId = new RatingId();
			ratingId.setComponentId(ratingDataBean.getComponentId());
			ratingId.setUserId(ratingDataBean.getUserId());
			ratingId.setParentId(ratingDataBean.getParentId());
			SentimentNewsRating ratingEntity = new SentimentNewsRating();
			ratingId.setFeelType(ratingDataBean.getFeelType());
			ratingEntity.setRatingId(ratingId);
			ratingEntity.setRating(ratingDataBean.getRating());
			// save the new entry for like event
			ratingEntity = repository.save(ratingEntity);
			return RatingDataBean.getRatingDataBean(ratingEntity,
					ratingDataBean.getComponentType());
		} else {
			logger.info("updating  ratingDataBean: "+ratingDataBean);
			return update(ratingDataBean);

		}
	}

	@Override
	public List<RatingDataBean> read(RatingDataBean ratingDataBean)
			throws MessageException {
		logger.info("Entered SentimentNewsRating service update method");
		List<RatingDataBean> ratingDataBeans = new ArrayList<>();
		if (ratingDataBean.getParentId() == null) {
			logger.error("parentId found to be null");
			throw (new MessageException("getParentId can't be null"));
		}
		if (ratingDataBean.getUserId() == null) {
			logger.error("UserId found to be null");
			throw (new MessageException("UserId can't be null"));
		}

		List<SentimentNewsRating> ratings = repository.findByParentIdAndUserId(
				ratingDataBean.getParentId(), ratingDataBean.getUserId());
		for (SentimentNewsRating rating : ratings) {
			ratingDataBeans.add(RatingDataBean.getRatingDataBean(rating,
					ratingDataBean.getComponentType()));
		}
		return ratingDataBeans;

	}

	@Override
	public List<RatingDataBean> readAll() {
		logger.info("Entered SentimentNewsRating service readAll method");
		List<RatingDataBean> ratingDataBeans = new ArrayList<>();
		List<SentimentNewsRating> ratings = repository.findAll();
		for (SentimentNewsRating rating : ratings) {
			ratingDataBeans.add(RatingDataBean.getRatingDataBean(rating,
					"SentimentNews"));
		}
		return ratingDataBeans;
	}

	@Override
	public RatingDataBean update(RatingDataBean ratingDataBean)
			throws MessageException {
		logger.info("Entered SentimentNewsRating service update method");
		if (ratingDataBean.getComponentId() == null) {
			logger.error("componentId found to be null");
			throw (new MessageException("componentId can't be null"));
		}
		if (ratingDataBean.getUserId() == null) {
			logger.error("userId found to be null");
			throw (new MessageException("userId can't be null"));
		}
		SentimentNewsRating originalRating = repository
				.findByComponentIdAndUserId(ratingDataBean.getComponentId(),
						ratingDataBean.getUserId(),ratingDataBean.getFeelType());
		if (originalRating != null) {
			logger.info("Updating SentimentLike with sentiment id = "
					+ originalRating.getRatingId().getComponentId()
					+ " and user id = "
					+ originalRating.getRatingId().getUserId());
			originalRating.setRating(ratingDataBean.getRating());
			SentimentNewsRating rating = repository.update(originalRating);
			return RatingDataBean.getRatingDataBean(rating,
					ratingDataBean.getComponentType());
		}
		else
			logger.info("Rating not updated: "+ratingDataBean);
		return null;
	}

	@Override
	public RatingDataBean delete(RatingDataBean ratingBean)
			throws MessageException {
		// TODO Auto-generated method stub
		SentimentNewsRating rating = repository.findByComponentIdAndUserId(
				ratingBean.getComponentId(), ratingBean.getUserId(),ratingBean.getFeelType());
		RatingDataBean rdb = null;
		if (rating != null) {
			rdb = RatingDataBean.getRatingDataBean(rating,
					ratingBean.getComponentType());
			repository.delete(rating.getRatingId());
		}
		return rdb;
	}

	@Override
	public RatingDataBean getByComponentIdAndUserId(
			RatingDataBean ratingDataBean) throws MessageException {
		logger.info("Entered SentimentNewsRating service getByComponentIdAndUserId method");
		if (ratingDataBean.getComponentId() == null) {
			logger.error("componentId found to be null");
			throw (new MessageException("componentId can't be null"));
		}
		if (ratingDataBean.getUserId() == null) {
			logger.error("userId found to be null");
			throw (new MessageException("userId can't be null"));
		}
		logger.debug("Getting SentimentNewsRating with sentiment id = "
				+ ratingDataBean.getComponentId() + " and user id = "
				+ ratingDataBean.getUserId());
		return RatingDataBean.getRatingDataBean(
				repository.findByComponentIdAndUserId(
						ratingDataBean.getComponentId(),
						ratingDataBean.getUserId(),ratingDataBean.getFeelType()),
				ratingDataBean.getComponentType());
	}

	@Override
	public List<RatingDataBean> getByParentIdAndUserId(
			RatingDataBean ratingDataBean) throws MessageException {
		logger.info("Entered getByParentIdAndUserId service readAll method");
		logger.info("Entered SentimentNewsRating service getByParentIdAndUserId method");
		if (ratingDataBean.getParentId() == null) {
			logger.error("parentId found to be null");
			throw (new MessageException("parentId can't be null"));
		}
		if (ratingDataBean.getUserId() == null) {
			logger.error("userId found to be null");
			throw (new MessageException("userId can't be null"));
		}

		List<RatingDataBean> ratingDataBeans = new ArrayList<>();
		List<SentimentNewsRating> ratings = repository.findByParentIdAndUserId(
				ratingDataBean.getParentId(), ratingDataBean.getUserId());
		for (SentimentNewsRating rating : ratings) {
			ratingDataBeans.add(RatingDataBean.getRatingDataBean(rating,
					ratingDataBean.getComponentType()));
		}
		return ratingDataBeans;
	}

	@Override
	public List<RatingDataBean> getByComponentId(
			RatingDataBean ratingDataBean) throws MessageException {
		logger.info("Entered SentimentNewsRating service getByComponentId method");
		if (ratingDataBean.getComponentId() == null) {
			logger.error("componentId found to be null");
			throw (new MessageException("componentId can't be null"));
		}
		logger.debug("Getting SentimentNewsRating with sentiment id = "
				+ ratingDataBean.getComponentId());
		
		List<RatingDataBean> ratingDataBeans = new ArrayList<>();
		List<SentimentNewsRating> ratings = repository.findByComponentId(ratingDataBean.getComponentId());
		for (SentimentNewsRating rating : ratings) {
			ratingDataBeans.add(RatingDataBean.getRatingDataBean(rating,
					ratingDataBean.getComponentType()));
		}
		return ratingDataBeans;
	
	}

	@Override
	public List<RatingDataBean> getByComponentIdByDate(
			RatingDataBean ratingDataBean) throws MessageException {
		logger.info("Entered SentimentNewsRating service getByComponentIdByDate method");
		if (ratingDataBean.getComponentId() == null) {
			logger.error("componentId found to be null");
			throw (new MessageException("componentId can't be null"));
		}
		logger.debug("Getting SentimentNewsRating with sentiment id = "
				+ ratingDataBean.getComponentId());
		
		List<RatingDataBean> ratingDataBeans = new ArrayList<>();
		List<SentimentNewsRating> ratings = repository.findByComponentIdByDate(ratingDataBean.getComponentId());
		for (SentimentNewsRating rating : ratings) {
			ratingDataBeans.add(RatingDataBean.getRatingDataBean(rating,
					ratingDataBean.getComponentType()));
		}
		return ratingDataBeans;
	
	}

}
