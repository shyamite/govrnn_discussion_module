package com.ohmuk.folitics.hibernate.entity.comment;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.util.DateUtils;

/**
 * Entity for comment on trend: {@link Response}
 * 
 * @author soumya
 *
 */

@Entity
@Table(name = "trendComment")
public class TrendComment implements Serializable,Comparable<TrendComment> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private Long trendMappingId;

	@Column(nullable = false)
	private Long userId;

	@Column(nullable = false, length = 512)
	private String comment;
	
	@Column(nullable = false)
	@NotNull(message = "error.trend.createTime.notNull")
	private Timestamp createTime;


	public TrendComment() {
		setCreateTime(DateUtils.getSqlTimeStamp());
		
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the trendMappingId
	 */
	public Long getTrendMappingId() {
		return trendMappingId;
	}

	/**
	 * @param trendMappingId the trendMappingId to set
	 */
	public void setTrendMappingId(Long trendMappingId) {
		this.trendMappingId = trendMappingId;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the createTime
	 */
	public Timestamp getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Override
	public int compareTo(TrendComment o) {
		if (getCreateTime() == null || o.getCreateTime() == null)
			return 0;
		return getCreateTime().compareTo(o.getCreateTime());	}


}
