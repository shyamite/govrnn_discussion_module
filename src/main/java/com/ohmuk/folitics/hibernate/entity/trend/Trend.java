package com.ohmuk.folitics.hibernate.entity.trend;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ohmuk.folitics.util.DateUtils;

/**
 * 
 * @author Deewan
 *
 */
@Entity
@Table(name = "trend")
// @JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
// @PrimaryKeyJoinColumn(name = "id")
public class Trend implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Column(unique = true, nullable = false, length = 200)
	@NotNull(message = "error.trend.name.notNull")
	@Size(min = 1, max = 200, message = "error.trend.name.size")
	private String name;

	@Column(nullable = false)
	@NotNull(message = "error.trend.createTime.notNull")
	private Timestamp createTime;

	@Column(nullable = false)
	@NotNull(message = "error.trend.editTime.notNull")
	private Timestamp editTime;

	@OneToMany(mappedBy = "trend")
    @Cascade(value = CascadeType.ALL) 
	@JsonManagedReference
	private List<TrendMapping> trendMappings;
	
	@Column(nullable = false)
	@NotNull(message = "error.component.createdBy.notNull")
	private String createdBy;
	
	@Column(nullable = false)
	private Boolean trending = true;
	
	@Column(nullable = false)
	@NotNull(message = "error.trend.startTrending.notNull")
	private Timestamp startTrending;
	
	@Column(nullable = false)
	@NotNull(message = "error.trend.endTrending.notNull")
	private Timestamp endTrending;
	
	@Column(nullable = false)
	private Boolean adminTrend = false;

	@Column(nullable = false)
	private Boolean enabled = true;
		
	public Trend() {
		// setComponentType(ComponentType.SENTIMENT.getValue());
		setCreateTime(DateUtils.getSqlTimeStamp());
		setEditTime(DateUtils.getSqlTimeStamp());
		setStartTrending(DateUtils.getSqlTimeStamp());
		setEndTrending(DateUtils.addDays(1, DateUtils.getSqlTimeStamp()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public List<TrendMapping> getTrendMappings() {
		return trendMappings;
	}

	public void setTrendMappings(List<TrendMapping> trendMappings) {
		this.trendMappings = trendMappings;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	public Boolean getTrending() {
		return trending;
	}

	public void setTrending(Boolean trending) {
		this.trending = trending;
	}

	/**
	 * @return the startTrending
	 */
	public Timestamp getStartTrending() {
		return startTrending;
	}

	/**
	 * @param startTrending the startTrending to set
	 */
	public void setStartTrending(Timestamp startTrending) {
		this.startTrending = startTrending;
	}

	/**
	 * @return the endTrending
	 */
	public Timestamp getEndTrending() {
		return endTrending;
	}

	/**
	 * @param endTrending the endTrending to set
	 */
	public void setEndTrending(Timestamp endTrending) {
		this.endTrending = endTrending;
	}

	public Boolean getAdminTrend() {
		return adminTrend;
	}

	public void setAdminTrend(Boolean adminTrend) {
		this.adminTrend = adminTrend;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Timestamp getEditTime() {
		return editTime;
	}

	public void setEditTime(Timestamp editTime) {
		this.editTime = editTime;
	}

}
