package com.ohmuk.folitics.daily.news.input;

import java.io.InputStream;
import java.util.List;

import com.ohmuk.folitics.ouput.model.DailyNewsOutPutModal;

public interface DailyNewsInputReader {
	List<DailyNewsInputPutModal> loadDailyNews(String filePath);

	List<DailyNewsOutPutModal> getDailyNewsOutpuModal(List<DailyNewsInputPutModal> inputPutModals);

	List<DailyNewsInputPutModal> loadDailyNews(InputStream inputStream);
	
	List<DailyNewsInputPutModal> addDailyNewsToCache();
}
