package com.ohmuk.folitics.daily.news.input;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.bytecode.opencsv.CSVReader;

import com.ohmuk.folitics.cache.FoliticsCache;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.cvs.reader.input.CSVFileReader;
import com.ohmuk.folitics.ouput.model.DailyNewsOutPutModal;
import com.ohmuk.folitics.util.FileLoadingUtils;

@Service
public class DailyNewsInputReaderImp implements DailyNewsInputReader {

	final static Logger logger = LoggerFactory
			.getLogger(DailyNewsInputReaderImp.class);

	@Autowired
	CSVFileReader fileReader;

	@Override
	public List<DailyNewsInputPutModal> loadDailyNews(String filePath) {
		logger.info("Enter DailyNewsInputReaderImp loadDailyNews");
		try {
			CSVReader csvReader = fileReader.getReader(filePath);
			List<DailyNewsInputPutModal> dailyNewsInputPutModals = prepareDailyNewsInputModel(csvReader);
			logger.info("Exit DailyNewsInputReaderImp loadDailyNews");
			return dailyNewsInputPutModals;
		} catch (IOException e) {
			logger.debug("Error in loadDailyNews" + e.getMessage());
			e.printStackTrace();
		}
		logger.info("Exit DailyNewsInputReaderImp loadDailyNews");

		return null;
	}

	@Override
	public List<DailyNewsInputPutModal> loadDailyNews(InputStream inputStream) {
		logger.info("Enter DailyNewsInputReaderImp loadDailyNews");
		try {
			CSVReader csvReader = new CSVReader(new InputStreamReader(
					inputStream), '\t');
			List<DailyNewsInputPutModal> dailyNewsInputPutModals = prepareDailyNewsInputModel(csvReader);
			logger.info("Exit DailyNewsInputReaderImp loadDailyNews");
			return dailyNewsInputPutModals;
		} catch (IOException e) {
			logger.debug("Error in loadDailyNews" + e.getMessage());
			e.printStackTrace();
		}
		logger.info("Exit DailyNewsInputReaderImp loadDailyNews");

		return null;
	}

	private List<DailyNewsInputPutModal> prepareDailyNewsInputModel(
			CSVReader reader) throws IOException {

		List<DailyNewsInputPutModal> dailyNewsInputPutModals = new ArrayList<DailyNewsInputPutModal>();

		List<String[]> rows = reader.readAll();

		for (int rowCount = 0; rowCount < rows.size();) {
			String[] record = rows.get(rowCount);
			// String[] record = rowRecord[0].split("\t");
			if (rowCount == 0 || rowCount == 1) {
				rowCount++;
			} else {
				DailyNewsInputPutModal dailyNewsInputPutModal = new DailyNewsInputPutModal();
				dailyNewsInputPutModal.setTitle(record[0].trim());
				dailyNewsInputPutModal.setLink(record[1].trim());
				dailyNewsInputPutModal.setSource(record[2].trim());
				dailyNewsInputPutModal.setStatus(record[3].trim());
				dailyNewsInputPutModal.setCreated(record[4].trim());
				dailyNewsInputPutModals.add(dailyNewsInputPutModal);
				rowCount++;
			}
		}
		return dailyNewsInputPutModals;

	}

	@Override
	public List<DailyNewsOutPutModal> getDailyNewsOutpuModal(
			List<DailyNewsInputPutModal> inputPutModals) {

		List<DailyNewsOutPutModal> outPutModals = new ArrayList<DailyNewsOutPutModal>();

		if (inputPutModals != null && !inputPutModals.isEmpty()) {

			for (DailyNewsInputPutModal inputPutModal : inputPutModals) {
				DailyNewsOutPutModal modal = new DailyNewsOutPutModal();
				modal.setLink(inputPutModal.getLink());
				modal.setTitle(inputPutModal.getTitle());
				outPutModals.add(modal);
			}
		}
		return outPutModals;
	}

	@Override
	public List<DailyNewsInputPutModal> addDailyNewsToCache() {
		SingletonCache singletonCache = SingletonCache.getInstance();
		List<DailyNewsInputPutModal> dailyNewsInputPutModals = new ArrayList<>();
		List<File> files = FileLoadingUtils.loadDailyNews();
		if (files != null && !files.isEmpty()) {
			for (File file : files) {
				dailyNewsInputPutModals.addAll(loadDailyNews(file.getPath()));
			}
		}
		singletonCache.put(FoliticsCache.KEYS.DAILY_NEWS,
				dailyNewsInputPutModals);
		return dailyNewsInputPutModals;
	}
}
