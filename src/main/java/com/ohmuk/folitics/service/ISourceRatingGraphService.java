/**
 * 
 */
package com.ohmuk.folitics.service;

import java.util.List;
import com.ohmuk.folitics.ouput.model.SourceRatingGraphOutputModel;

/**
 * @author Deewan
 *
 */
public interface ISourceRatingGraphService {

	List<SourceRatingGraphOutputModel> top50PercentRatingType(String feedName);

	List<SourceRatingGraphOutputModel> ratingTypePercentage(String feedName);

	List<SourceRatingGraphOutputModel> ratingTypeBarGraph(String feedName); 

	
}
