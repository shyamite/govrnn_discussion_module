package com.ohmuk.folitics.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.hibernate.entity.poll.PollOption;
import com.ohmuk.folitics.ouput.model.ResponseOutputModel;

/**
 * @author Abhishek
 *
 */
@Service
@Transactional
public class ResponseService implements IResponseService {

	private static Logger logger = LoggerFactory.getLogger(ResponseService.class);

	@Autowired
	private SessionFactory _sessionFactory;

	private Session getSession() {
		return _sessionFactory.getCurrentSession();
	}

	@Override
	public Response create(Response response) throws Exception {
		logger.info("Inside ResponseService create method");
		Long id = (Long) getSession().save(response);
		logger.info("Exiting from ResponseService create method");
		return getResponseById(id);
	}

	@Override
	public Response getResponseById(Long id) throws Exception {
		logger.info("Inside ResponseService getResponseById method");
		Response response = (Response) getSession().get(Response.class, id);
		logger.info("Exiting from ResponseService getResponseById method");
		return response;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Response> readAll() throws Exception {
		logger.info("Inside ResponseService readAll method");
		List<Response> responses = getSession().createCriteria(Response.class).list();
		logger.info("Exiting from ResponseService readAll method");
		return responses;
	}

	@Override
	public Response update(Response response) throws Exception {
		logger.info("Inside ResponseService update method");
		getSession().update(response);
		logger.info("Exiting from ResponseService update method");
		return response;
	}

	@Override
	public List<Response> getByOpinionId(Long id) throws Exception {
		logger.info("Inside ResponseService getByOpinionId method");
		Opinion opinion = (Opinion) getSession().get(Opinion.class, id);
		Criteria criteria = getSession().createCriteria(Response.class);
		criteria.add(Restrictions.eqOrIsNull("opinion", opinion));
		criteria.addOrder(Order.desc("id"));
        criteria.setMaxResults(ResponseMaxResults);
		List<Response> response = criteria.list();
		logger.info("Exiting from ResponseService getByOpinionId method");
		return response;
	}

	@Override
	public List<Response> getByUserId(Long id) throws Exception {
		logger.info("Inside ResponseService getByUserId method");
		User user = (User) getSession().get(User.class, id);
		Criteria criteria = getSession().createCriteria(Response.class);
		criteria.add(Restrictions.eqOrIsNull("user", user));
		List<Response> response = criteria.list();
		logger.info("Exiting from ResponseService getByUserId method");
		return response;
	}

	@Override
	public boolean delete(Long id) throws Exception {
		logger.info("Inside ResponseService delete method");
		Response response = getResponseById(id);
		response.setState(ComponentState.DELETED.getValue());
		getSession().update(response);
		logger.info("Exiting from ResponseService delete method");
		return true;
	}

	@Override
	public boolean delete(Response response) throws Exception {
		logger.info("Inside ResponseService delete method");
		response = getResponseById(response.getId());
		response.setState(ComponentState.DELETED.getValue());
		getSession().update(response);
		logger.info("Exiting from ResponseService delete method");
		return true;
	}

	@Override
	public boolean deleteFromDBById(Long id) throws Exception {
		logger.info("Inside ResponseService deleteFromDBById method");
		Response response = getResponseById(id);
		if (null != response) { 
			response.setUser(null);
			response.setOpinion(null);
			response.setParentResponse(null);
			//response.setChildResponses(null);
			getSession().delete(response);
            logger.info("Exiting from ResponseService deleteFromDBById method");
            return true;
        }        
		logger.info("Exiting from ResponseService deleteFromDBById method");
		return true;
	}

	@Override
	public boolean deleteFromDB(Response response) throws Exception {
		logger.info("Inside ResponseService deleteFromDB method");
		response = getResponseById(response.getId());
		getSession().delete(response);
		logger.info("Exiting from ResponseService deleteFromDB method");
		return true;
	}

	@Override
	public List<Response> userPointsAggregations(Response response,Double userPoints) throws Exception {
		logger.info("Inside ResponseService userPointsAggregations method");
		Long userId =response.getUser().getId();
		getSession().update(getSession().get(User.class, userId));
		List<Response> responses = readAll();
		logger.info("Exiting from ResponseService deleteFromDB method");
		return responses;
	}
	
	@Override
	public List<Response> getResponseForOpinion(Long id) throws Exception {
		    logger.info("Inside ResponseService getResponseForOpinion method");
		    
	        Criteria criteria = getSession().createCriteria(Response.class);
	        criteria.add(Restrictions.eq("opinion.id", id));
	        		
	        List<Response> responses = criteria.list();		
	        logger.info("Exiting from ResponseService getResponseForOpinion method");
	        return responses;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ResponseOutputModel> getAllResponseById(long opinionId) {
		logger.info("Inside responseService getAllResponseById method");
		List<ResponseOutputModel> models = null;
		List<Response> responses = null;
		try {
			Criteria criteria = getSession().createCriteria(Response.class)
					.add( Restrictions.and(
							Restrictions.eq("opinion.id", opinionId),
							Restrictions.isNull("parentResponse")));
			criteria.addOrder(Order.desc("id"));
	        criteria.setMaxResults(ResponseMaxResults);
			responses = criteria.list();
			if (null != responses) {
				models = new ArrayList<ResponseOutputModel>();
				for (Response response : responses) {
					ResponseOutputModel responseOutputModel = ResponseOutputModel.getModel(response);
					
					if(response.getChildResponses() != null)
						responseOutputModel = recursiveChildResponses(response, responseOutputModel);
					
					models.add(responseOutputModel);
				}
			}

		} catch (Exception exception) {
			logger.error("Exception in getAllResponseById for opinionId : "
					+opinionId );
			logger.info("Exiting ResponseSerivce getAllResponseById method");
		}

		if (null != models) {
			logger.debug("Matching responses list with opinionid : " + opinionId);
			logger.info("Exiting response service getAllresponseByid method");
			return models;
		}
		logger.info("Exiting ResponseSerivce getAllResponseById method");
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.ohmuk.folitics.service.IResponseService#recursiveChildResponses(com.ohmuk.folitics.hibernate.entity.Response, com.ohmuk.folitics.ouput.model.ResponseOutputModel)
	 */
	@Override
	public ResponseOutputModel recursiveChildResponses(Response response, ResponseOutputModel responseOutputModel) {

		if (response.getChildResponses() == null)
			return null;

		for (Response childResponse : response.getChildResponses()) {
			ResponseOutputModel childResponseOutputModel = ResponseOutputModel.getModel(childResponse);
			responseOutputModel.getChildResponses().add(childResponseOutputModel);
			
			if(childResponse.getChildResponses() != null)
				recursiveChildResponses(childResponse, childResponseOutputModel);
		}

		return responseOutputModel;

	}

	/* (non-Javadoc)
	 * @see com.ohmuk.folitics.service.IResponseService#getByOpinionAndUser(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<Response> getByOpinionAndUser(Long opinionId, Long userId) {
		// TODO Auto-generated method stub
		logger.info("Inside ResponseService getByOpinionId method");
		Opinion opinion = (Opinion) getSession().get(Opinion.class, opinionId);
		User user = (User) getSession().get(User.class, userId);
		Criteria criteria = getSession().createCriteria(Response.class);
		Criterion criterion = Restrictions.and(Restrictions.eqOrIsNull("opinion", opinion),Restrictions.eq("user", user));
		criteria.add(criterion);
		List<Response> responses = criteria.list();
		logger.info("Exiting from ResponseService getByOpinionId method");
		return responses;
	}

    @Override
    public Response getResponseByPollId(Long id) {
        logger.info("Inside from ResponseService getResponseByPollId method");
        Criteria criteria = getSession().createCriteria(Response.class)
        .createAlias("upDownVote", "p")
        .add(Restrictions.eq("p.id", id));
       
        List<Response> responses = criteria.list();
        if(responses!=null){
            return responses.get(0);
        }else{
            return null;
        }
        
    }
    
    @Override
	public Integer getCountByUserId(Long id) throws Exception {
		logger.info("Inside ResponseService getCountByUserId method");
		User user = (User) getSession().get(User.class, id); 
		Criteria criteria = getSession().createCriteria(Response.class);
		criteria.add(Restrictions.eqOrIsNull("user", user));
		if (criteria.list() != null){
			logger.info("Exiting from ResponseService getCountByUserId method");
			return criteria.list().size();
		}
		logger.info("Exiting from ResponseService getCountByUserId method");
		return null;
	}
	
}
