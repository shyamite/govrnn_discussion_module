/**
 * 
 */
package com.ohmuk.folitics.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.hibernate.entity.rating.SentimentNewsRating;
import com.ohmuk.folitics.ouput.model.SourceRatingGraphOutputModel;

/**
 * @author Deewan
 *
 */
@Service
@Transactional
public class SourceRatingGraphService implements ISourceRatingGraphService {

	private static Logger logger = Logger
			.getLogger(SourceRatingGraphService.class);

	@Autowired
	private SessionFactory _sessionFactory;

	private Session getSession() {
		return _sessionFactory.getCurrentSession();
	}

	@Override
	public List<SourceRatingGraphOutputModel> top50PercentRatingType(String feedName) {
		// TODO Auto-generated method stub
		logger.info("Inside sourceRatingGraph  method");
		List<SourceRatingGraphOutputModel> models = new ArrayList<SourceRatingGraphOutputModel>();
		List<SentimentNewsRating> sentimentNewsRating = null;
		List<Long> componentId = null;
		try {

			// First Get the component Ids from Sentiment_News table
			componentId=getComponentId(feedName);

			// Based on above component ID need to get the graph data from sentiment news rating table or directly we can use below query and it will return the 
			/* Rating Type and its percentage
			 * SELECT ratingId Rating_Type, Count(ratingId) as Rating_Type_Count,
				(Count(ratingId)*100/(select Count(*)FROM folitics.sentimentnewsrating 
				where componentId in (select id from folitics.sentiment_news where sentiment_news.feedName='Aaj Tak')) ) as Percentage
				FROM folitics.sentimentnewsrating 
						where componentId in (select id from folitics.sentiment_news 
												where sentiment_news.feedName='Aaj Tak') group by ratingId;

			 */
			Criteria criteria = getSession().createCriteria(SentimentNewsRating.class).add(Restrictions.in("componentId", componentId));
			for (SentimentNewsRating entity : sentimentNewsRating) {
				//System.out.println("1.Component ID"+entity.getId()+" 2.Feed Name "+entity.getFeedName()+" ");
				models.add(SourceRatingGraphOutputModel.getModel(entity));
			}


		} catch (Exception exception) {
			logger.error("Exception in sourceRatingGraph for match Feed name : "
					+ feedName);
		}

		if (null != sentimentNewsRating) {
			logger.debug("sourceRatingGraph list found starting for match Feed Name: "
					+ feedName);
			return models;
		}
		return null;

	}

	private List<Long> getComponentId(String feedName) {
		// TODO Auto-generated method stub
		List<SentimentNews> sentimentNews = null;
		List<Long> componentId = null;
		Criteria criteria = getSession().createCriteria(SentimentNews.class).add(Restrictions.eq("feedName", feedName));
		sentimentNews = criteria.list();
		for (SentimentNews entity : sentimentNews) {
			System.out.println("1.Component ID" + entity.getId()+ " 2.Feed Name " + entity.getFeedName() + " ");
			componentId.add(entity.getId());
		}
		
		return componentId;
	}

	@Override
	public List<SourceRatingGraphOutputModel> ratingTypePercentage(
			String feedName) {
		// TODO Auto-generated method stub
		logger.info("Inside ratingTypePercentage  method");
		List<SourceRatingGraphOutputModel> models = new ArrayList<SourceRatingGraphOutputModel>();
		List<SentimentNewsRating> sentimentNewsRating = null;
		List<Long> componentId = null;
		try {

			// First Get the component Ids from Sentiment_News table
			componentId=getComponentId(feedName);

			// Based on above component ID need to get the graph data from sentiment news rating table or directly we can use below query and it will return the 
			/* Rating Type and its percentage
			 * 
				SELECT  ratingId Rating_Type, Count(ratingId) as Rating_Type_Count
				FROM folitics.sentimentnewsrating 
					where componentId in 
						(select id from folitics.sentiment_news where sentiment_news.feedName='Aaj Tak') 
					group by ratingId;
			 */
			Criteria criteria = getSession().createCriteria(SentimentNewsRating.class).add(Restrictions.in("componentId", componentId));
			for (SentimentNewsRating entity : sentimentNewsRating) {
				//System.out.println("1.Component ID"+entity.getId()+" 2.Feed Name "+entity.getFeedName()+" ");
				models.add(SourceRatingGraphOutputModel.getModel(entity));
			}


		} catch (Exception exception) {
			logger.error("Exception in ratingTypePercentage for match Feed name : "
					+ feedName);
		}

		if (null != sentimentNewsRating) {
			logger.debug("sourceRatingGraph list found starting for match Feed Name: "
					+ feedName);
			return models;
		}
		return null;
	}

	@Override
	public List<SourceRatingGraphOutputModel> ratingTypeBarGraph(String feedName) {
		// TODO Auto-generated method stub
		logger.info("Inside ratingTypeBarGraph  method");
		List<SourceRatingGraphOutputModel> models = new ArrayList<SourceRatingGraphOutputModel>();
		List<SentimentNewsRating> sentimentNewsRating = null;
		List<Long> componentId = null;
		try {

			// First Get the component Ids from Sentiment_News table
			componentId=getComponentId(feedName);

			// Based on above component ID need to get the graph data from sentiment news rating table or directly we can use below query and it will return the 
			/* Rating Type and its percentage
			 * 
				SELECT  year(createTime) as Year, month(createTime)as Month, Count(RatingId) as Grand_Total
				FROM folitics.sentimentnewsrating 
					where componentId in (select id from folitics.sentiment_news where sentiment_news.feedName='Aaj Tak') 
					group by year(createTime), month(createTime);
			 */
			Criteria criteria = getSession().createCriteria(SentimentNewsRating.class).add(Restrictions.in("componentId", componentId));
			for (SentimentNewsRating entity : sentimentNewsRating) {
				//System.out.println("1.Component ID"+entity.getId()+" 2.Feed Name "+entity.getFeedName()+" ");
				models.add(SourceRatingGraphOutputModel.getModel(entity));
			}


		} catch (Exception exception) {
			logger.error("Exception in ratingTypeBarGraph for match Feed name : "
					+ feedName);
		}

		if (null != sentimentNewsRating) {
			logger.debug("sourceRatingGraph list found starting for match Feed Name: "
					+ feedName);
			return models;
		}
		return null;		
	}

}
