package com.ohmuk.folitics.businessDelegate.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ohmuk.folitics.businessDelegate.interfaces.IResponseBusinessDelegate;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.notification.InterfaceNotificationService;
import com.ohmuk.folitics.notification.NotificationMapping;
import com.ohmuk.folitics.ouput.model.ResponseOutputModel;
import com.ohmuk.folitics.service.IOpinionService;
import com.ohmuk.folitics.service.IResponseService;
import com.ohmuk.folitics.service.ISentimentService;
import com.ohmuk.folitics.service.IUserService;
import com.ohmuk.folitics.util.NotificationUtil;

@Component
@Transactional
public class ResponseBusinessDelegate implements IResponseBusinessDelegate {
	private static Logger logger = LoggerFactory
			.getLogger(ResponseBusinessDelegate.class);

	final int PRO_AGREE = 1;
	final int PRO_DISAGREE = 1;
	final int ANTI_AGREE = -1;
	final int ANTI_DISAGREE = -1;

	@Autowired
	private volatile IResponseService responseService;

	@Autowired
	private volatile ISentimentService sentimentService;

	@Autowired
	private volatile IOpinionService opinionService;

	@Autowired
	private volatile IUserService userService;

	@Autowired
	private volatile InterfaceNotificationService notificationService;

	@Override
	public Response create(Response response, Long id) throws Exception {
		NotificationMapping notificationMapping = new NotificationMapping();
		/*
		 * if(response.getUpDownVote().getSentiment() == null){ Sentiment
		 * sentiment = new Sentiment(); sentiment =
		 * response.getOpinion().getSentiment();
		 * response.getUpDownVote().setSentiment(sentiment); } Sentiment
		 * sentiment = new Sentiment();
		 */
		Opinion opinion = null;
		User user = new User();
		Response parentResponse = new Response();
		if (response.getOpinion() != null) {
			opinion = opinionService.read(response.getOpinion().getId());
			response.setOpinion(opinion);
		}
		if (response.getUser() != null) {
			user = userService.findUserById(response.getUser().getId());
			response.setUser(user);
		}
		if (response.getParentResponse() != null) {
			parentResponse = responseService.getResponseById(response
					.getParentResponse().getId());
			response.setParentResponse(parentResponse);
		}
		/*
		 * sentiment = response.getOpinion().getSentiment();
		 * if(sentiment.getPolls() != null){
		 * if(!sentiment.existsPoll(response.getUpDownVote())){
		 * sentiment.addPoll(response.getUpDownVote()); } }else{
		 * sentiment.addPoll(response.getUpDownVote()); }
		 * 
		 * sentimentService.update(sentiment);
		 */
		Response responseData = responseService.create(response);
		String numberAsString = null;
		// call for notification
		if (responseData != null) {
			notificationMapping = NotificationUtil
					.createNotificationMappingForResponse(
							responseData.getUser(),
							ComponentType.RESPONSE.getValue(),
							responseData.getId());
			if (responseData != null) {
			        notificationService.generalNotification(notificationMapping,
                            response, null,id,null);
			    
				/*if (responseData.getUpDownVote()!= null) {
				id = responseData.getUpDownVote().getId();
				try{
				dbResponse = responseService.getResponseById(id);
				if (dbResponse != null) {
					notificationService.generalNotification(notificationMapping,
							response, null);
				}
				}catch(Exception e){
					e.printStackTrace();}
				}*/
			}
		}
		if (response.getUserIds() == null) {
			
		}else{
		    List<Long> users = new ArrayList<Long>();
            for (Long userId : response.getUserIds()) {
                if (!userId.equals(notificationMapping.getUserId())) {
                    users.add(userId);
                }
            }
            notificationService.sendTagNotification(notificationMapping, users);
		}
		logger.info("Exiting create method in business delegate");
		return responseData;
	}

	@Override
	public Response getResponseById(Long id) throws Exception {
		logger.info("Inside getResponseById method in business delegate");
		Response responseData = responseService.getResponseById(id);
		logger.info("Exiting getResponseById method in business delegate");
		return responseData;
	}

	@Override
	public List<Response> readAll() throws Exception {
		logger.info("Inside readAll method in business delegate");
		List<Response> responseData = responseService.readAll();
		logger.info("Exiting readAll method in business delegate");
		return responseData;
	}

	@Override
	public Response update(Response response) throws Exception {
		logger.info("Inside update method in business delegate");
		Response responseData = responseService.update(response);
		logger.info("Exiting update method in business delegate");
		return responseData;
	}

	@Override
	public List<Response> getByOpinionId(Long id) throws Exception {
		logger.info("Inside getByOpinionId method in business delegate");
		List<Response> responseData = responseService.getByOpinionId(id);
		/*
		 * for (Response response : responseData) {
		 * Hibernate.initialize(response.getOpinion());
		 * Hibernate.initialize(response.getUser()); for(Response response2 :
		 * response.getOpinion().getResponses() ){
		 * Hibernate.initialize(response2.getUser()); } }
		 */
		logger.info("Exiting getByOpinionId method in business delegate");
		return responseData;
	}

	@Override
	public List<Response> getByUserId(Long id) throws Exception {
		logger.info("Inside getByUserId method in business delegate");
		List<Response> responseData = responseService.getByUserId(id);
		logger.info("Exiting getByUserId method in business delegate");
		return responseData;
	}

	
	
	@Override
	public Integer getCountByUserId(Long id) throws Exception {
		logger.info("Inside getCountByUserId method in business delegate");
		Integer count = responseService.getCountByUserId(id);
		logger.info("Exiting getCountByUserId method in business delegate");
		return count;
	}
	@Override
	public List<Response> getByOpinionAndUser(Long opinionId, Long userId)
			throws Exception {
		logger.info("Inside getByOpinionAndUser method in business delegate");
		List<Response> responseData = responseService.getByOpinionAndUser(
				opinionId, userId);
		logger.info("Exiting getByOpinionAndUser method in business delegate");
		return responseData;
	}

	@Override
	public boolean delete(Long id) throws Exception {
		logger.info("Inside delete method in business delegate");
		boolean sucess = responseService.delete(id);
		logger.info("Exiting delete method in business delegate");
		return sucess;
	}

	@Override
	public boolean delete(Response response) throws Exception {
		logger.info("Inside delete method in business delegate");
		boolean sucess = responseService.delete(response);
		logger.info("Exiting delete method in business delegate");
		return sucess;
	}

	@Override
	public boolean deleteFromDBById(Long id) throws Exception {
		logger.info("Inside deleteFromDBById method in business delegate");
		boolean sucess = responseService.deleteFromDBById(id);
		notificationService.deleteNotificationByComponentIdAndType(id,
				ComponentType.RESPONSE.getValue());
		logger.info("Exiting deleteFromDBById method in business delegate");
		return sucess;
	}

	@Override
	public boolean deleteFromDB(Response response) throws Exception {
		logger.info("Inside deleteFromDB method in business delegate");
		boolean sucess = responseService.deleteFromDB(response);
		logger.info("Exiting deleteFromDB method in business delegate");
		return sucess;
	}

	/**
	 * This method is to aggregate user point
	 * 
	 * @param startTime
	 * @param endTime
	 * @throws Exception
	 */

	public List<Response> userPointsAggregations(Response response)
			throws Exception {
		logger.info("Inside deleteFromDB method in business delegate");
		String type = response.getOpinion().getType();
		Double userPoints = response.getUser().getPoints();
		String flag = response.getFlag();
		if (type == "pro") {
			if (flag == "Agree") {
				userPoints = userPoints + PRO_AGREE;
			} else {
				userPoints = userPoints + PRO_DISAGREE;
			}
		} else {
			if (flag == "Agree") {
				userPoints = userPoints + ANTI_AGREE;
			} else {
				userPoints = userPoints + ANTI_DISAGREE;
			}
		}

		List<Response> responses = responseService.userPointsAggregations(
				response, userPoints);
		logger.info("Exiting deleteFromDB method in business delegate");
		return responses;

	}

	public void userAggregation(String type, Double userPoint, String flag) {

	}

	@Override
	public List<Response> getResponseForOpinion(Long id) throws Exception {
		logger.info("Inside getResponseForOpinion method in business delegate");
		List<Response> response = responseService.getResponseForOpinion(id);
		logger.info("Exiting getResponseForOpinion method in business delegate");
		return response;
	}

	@Override
	public List<ResponseOutputModel> getAllResponseById(long opinionId)
			throws Exception {
		logger.info("Inside getAllResponseById method in business delegate");
		List<ResponseOutputModel> responseModels = responseService
				.getAllResponseById(opinionId);
		logger.info("Exiting getAllResponseById method in business delegate");
		return responseModels;
	}
}
