package com.ohmuk.folitics.businessDelegate.implementations;

import java.util.List;

import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserImage;
import com.ohmuk.folitics.ouput.model.BaseOutputModel;
import com.ohmuk.folitics.ouput.model.PollOptionOutputModel;

public class SearchOutputModel extends BaseOutputModel<String> {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    String componentType;
    byte[] image;
    String imageUrl;
    String url;

    User user;
    List<PollOptionOutputModel> list;

    String description;
    String ageString;
    String connectionStatus;
    String type;
    String userName;
    String name;
    private List<UserImage> userImages;;
    Long userId;
    String location;

    String sentimentCategory;
    long sentimentId;

    public String getComponentType() {
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String url) {
        this.imageUrl = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<PollOptionOutputModel> getList() {
        return list;
    }

    public void setList(List<PollOptionOutputModel> list) {
        this.list = list;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAgeString() {
        return ageString;
    }

    public void setAgeString(String ageString) {
        this.ageString = ageString;
    }

    public String getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(String connectionStatus) {
        this.connectionStatus = connectionStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserImage> getUserImages() {
        return userImages;
    }

    public void setUserImages(List<UserImage> userImages) {
        this.userImages = userImages;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSentimentCategory() {
        return sentimentCategory;
    }

    public void setSentimentCategory(String sentimentCategory) {
        this.sentimentCategory = sentimentCategory;
    }

    public long getSentimentId() {
        return sentimentId;
    }

    public void setSentimentId(long sentimentId) {
        this.sentimentId = sentimentId;
    }

}
