/**
 * 
 */
package com.ohmuk.folitics.businessDelegate.implementations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ohmuk.folitics.businessDelegate.interfaces.ICommonBusinessDelegate;
import com.ohmuk.folitics.service.IComponentService;


/**
 * @author Kalpana
 *
 */
@Component
public class CommonBusinessDelegate implements ICommonBusinessDelegate {
	 private static Logger logger = LoggerFactory.getLogger(CommonBusinessDelegate.class);

	 @Autowired
	 private volatile IComponentService componentService;
	/* (non-Javadoc)
	 * @see com.ohmuk.folitics.businessDelegate.interfaces.ICommonBusinessDelegate#incrementView(java.lang.Long)
	 */
	@Override
	public boolean incrementView(Long opinionId) throws Exception {
		// TODO Auto-generated method stub
		Long noOfViews = 0l;
		com.ohmuk.folitics.hibernate.entity.Component component = componentService.read(opinionId);
		if(component.getViews() != null){
			component.setViews(component.getViews()+1);
		}else{
			component.setViews(noOfViews);
		}
		componentService.update(component);
		return true;	
	}
	 
}
