package com.ohmuk.folitics.businessDelegate.implementations;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.catalina.connector.Connector;
import org.elasticsearch.action.search.SearchResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.stereotype.Component;

import com.ohmuk.folitics.businessDelegate.interfaces.IOpinionBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.ISearchBusinessDeligate;
import com.ohmuk.folitics.businessDelegate.interfaces.ISentimentBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IUserBusinessDelegate;
import com.ohmuk.folitics.cache.FoliticsCache;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.component.newsfeed.ImageSizeEnum;
import com.ohmuk.folitics.component.newsfeed.ImageTypeEnum;
import com.ohmuk.folitics.constants.Constants;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserConnection;
import com.ohmuk.folitics.hibernate.entity.UserImage;
import com.ohmuk.folitics.ouput.model.OpinionOutputModel;
import com.ohmuk.folitics.service.ISearchService;
import com.ohmuk.folitics.service.IUserService;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.ImageUtil;

@Component
@Transactional
public class SearchBusinessDeligate implements ISearchBusinessDeligate {

    final static Logger logger = LoggerFactory.getLogger(SearchBusinessDeligate.class);

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private ISearchService searchService;

    @Autowired
    private IUserBusinessDelegate userBuisnessDelegate;

    @Autowired
    private IOpinionBusinessDelegate opinionBuisnessDelegate;

    @Autowired
    private ISentimentBusinessDelegate sentimateBuisnessDelegat;

    @Value("${spring.profiles.active}")
    private String serverPath;

    @Autowired
    private EmbeddedWebApplicationContext appContext;

    @Autowired
    private volatile IUserService userService;

    @Override
    public SearchResponse searchDocument(String index, String type, String text) throws UnknownHostException {

        logger.info("Inside Search BusinessDeligate");

        SearchResponse response = searchService.searchDocument(index, type, text);

        logger.info("Exiting Search BusinessDeligate");
        return response;
    }

    @Override
    public List<SearchOutputModel> searchByComponent(String searchKeyword, String componentType, Long userId,
            HttpServletRequest request) throws Exception {

        List<SearchOutputModel> searchOutputModels = new ArrayList<SearchOutputModel>();

        if (componentType.equals(Constants.People)) {
            searchUser(searchKeyword, componentType, searchOutputModels, userId, request);

        }
        if (componentType.equalsIgnoreCase(Constants.Opinion)) {
            searchOpinion(searchKeyword, componentType, searchOutputModels, request);

        }
        /*
         * if (componentType.equalsIgnoreCase(Constants.Sentiment)) {
         * searchSentiment(searchKeyword, componentType, searchOutputModels);
         * 
         * }
         */
        if (componentType.equals(Constants.ALL)) {
            searchUser(searchKeyword, componentType, searchOutputModels, userId, request);
            searchOpinion(searchKeyword, componentType, searchOutputModels, request);
            /*
             * searchSentiment(searchKeyword, componentType,
             * searchOutputModels);
             */
        }

        return searchOutputModels;
    }

    /*
     * private void searchSentiment(String searchKeyword, String componentType,
     * List<SearchOutputModel> searchOutputModels) throws IOException {
     * List<Sentiment> sentimentList =
     * sentimateBuisnessDelegat.searchSentiment(searchKeyword); if
     * (sentimentList == null || sentimentList.size() <= 0) {
     * 
     * } else { for (Sentiment sentiment : sentimentList) { SearchOutputModel
     * searchOutputModel = new SearchOutputModel();
     * searchOutputModel.setComponentType(Constants.Sentiment);
     * searchOutputModel.setId(sentiment.getId());
     * searchOutputModel.setImage(sentiment.getImage());
     * searchOutputModel.setTitle(sentiment.getSubject());
     * searchOutputModel.setImageUrl(servletContext.getContextPath()+
     * "/sentiment/downloadSentimentImage?sentimentId=" + sentiment.getId() +
     * "&imageType=" + sentiment.getImageType()+ "&imageSize=" +
     * ImageSizeEnum.THUMBNAIL.getImageSize());
     * searchOutputModel.setUrl("#/sentiment?id=" + sentiment.getId());
     * 
     * searchOutputModel.setFormattedAge(sentiment.getFormattedAge());
     * 
     * searchOutputModels.add(searchOutputModel);
     * 
     * } } }
     */

    private void searchOpinion(String searchKeyword, String componentType, List<SearchOutputModel> searchOutputModels,
            HttpServletRequest request) throws Exception {
        LinkedHashSet<Opinion> opinionList = opinionBuisnessDelegate.searchOpinion(searchKeyword, null);
        List<OpinionOutputModel> opinionsModel = new ArrayList<OpinionOutputModel>();
        if (opinionList == null || opinionList.size() <= 0) {

        } else {
            for (Opinion opinion : opinionList) {
            	
            	String time =DateUtils.getDateOrTimeAgo(opinion.getCreateTime());
            	time= time.substring(0, time.length() - 4);
            	//System.out.println(time);
            	//Integer a = Integer.parseInt(time);
            	//System.out.println("a ::"+a);
            	
                opinionsModel.add(OpinionOutputModel.getModel(opinion));
            }
            for (OpinionOutputModel model : opinionsModel) {
                SearchOutputModel searchOutputModel = new SearchOutputModel();
                searchOutputModel.setComponentType(Constants.Opinion);
                searchOutputModel.setId(model.getId());

                Sentiment sentiment = sentimateBuisnessDelegat.read(model.getSentimentId());
                /*
                 * searchOutputModel.setImage(ThumbnailUtil.getImageThumbnail(
                 * sentiment.getImage(), sentiment.getImageType()));
                 */

                if (model.getImageUrl() == null || model.getImageUrl().isEmpty()) {
                    String url = getBaseUrl(request);

                    searchOutputModel.setImageUrl(serverPath + "/sentiment/downloadSentimentImage?sentimentId="
                            + model.getSentimentId() + "&imageType=" + sentiment.getImageType() + "&imageSize="
                            + ImageSizeEnum.THUMBNAIL.getImageSize());
                } else {
                    searchOutputModel.setImageUrl(model.getImageUrl());
                }

                // searchOutputModel.setImage(sentiment.getImage());
                searchOutputModel.setTitle(model.getTitle());
                searchOutputModel.setDescription(model.getText());
                searchOutputModel.setUrl("#/viewOpinion?id=" + model.getId());
                searchOutputModel.setFormattedAge(model.getFormattedAge());
                searchOutputModel.setType(model.getType());
                User dbUser = userBuisnessDelegate.findUserById(model.getUser().getId());
                searchOutputModel.setName(dbUser.getName());
                searchOutputModel.setUserName(dbUser.getUsername());
                searchOutputModel.setUserImages(dbUser.getUserImages());
                searchOutputModel.setUserId(dbUser.getId());
                searchOutputModel.setLocation(dbUser.getState() + "-" + dbUser.getCity());

                searchOutputModel.setList(model.getAgreeDisagrePoll().getPollOptionOutputModels());
                searchOutputModel.setSentimentCategory(sentiment.getSubject());
                searchOutputModel.setSentimentId(sentiment.getId());
                searchOutputModels.add(searchOutputModel);
            }

        }
    }

    public String getBaseUrl(HttpServletRequest request) throws UnknownHostException, SocketException {
        Connector connector = ((TomcatEmbeddedServletContainer) appContext.getEmbeddedServletContainer()).getTomcat()
                .getConnector();
        String scheme = connector.getScheme();
        String ip = InetAddress.getLocalHost().getHostAddress();
        System.out.println(" ip ::" + ip);

        InetAddress ipAddress;

        InetAddress addr = InetAddress.getLocalHost();
        Enumeration<InetAddress> inetAddresses;
        ipAddress = InetAddress.getLocalHost();
        String inet = "";
        Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
        /*
         * for (NetworkInterface netint : Collections.list(nets)){
         * 
         * inetAddresses = netint.getInetAddresses();
         * System.out.println("Collection list ::"
         * +Collections.list(inetAddresses)); inet =
         * Collections.list(inetAddresses).get(1).toString();
         * 
         * break; }
         */
        int port = connector.getPort();
        String contextPath = appContext.getServletContext().getContextPath();
        System.out.println("local url :::" + scheme + "://" + inet + ":" + port + contextPath);

        Map<String, String> result = new HashMap<>();

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            result.put(key, value);
        }

        System.out.println("result :::" + result.get("referer"));

        return scheme + "://" + ip + ":" + port + contextPath;
    }

    private void searchUser(String searchKeyword, String componentType, List<SearchOutputModel> searchOutputModels,
            Long userId, HttpServletRequest request) throws Exception {
        List<User> users = userBuisnessDelegate.findByUserNameOrName(searchKeyword);
        if (users == null || users.size() <= 0) {

        } else {
            for (User user : users) {
            	
                SearchOutputModel searchOutputModel = new SearchOutputModel();
                searchOutputModel.setComponentType("User");
                searchOutputModel.setId(user.getId());
               // String url = getBaseUrl(request);
                searchOutputModel.setImageUrl(serverPath + "/user/downloadUserImage?userId=" + user.getId()
                        + "&imageType=" + ImageTypeEnum.USERIMAGE.getImageType() + "&imageSize="
                        + ImageSizeEnum.THUMBNAIL.getImageSize());
                searchOutputModel.setTitle(user.getName());
                UserImage userImage = ImageUtil.getUserImage(ImageTypeEnum.USERIMAGE.getImageType(),
                        user.getUserImages());
                if (userImage != null) {
                    // searchOutputModel.setImage(userImage.getImage());
                } else {
                    searchOutputModel
                            .setImage((byte[]) SingletonCache.getInstance().get(FoliticsCache.KEYS.USER_IMAGE));
                }

                /*
                 * // userImage try { User dbUser =
                 * userBuisnessDelegate.findUserById(user.getId());
                 * List<UserImage> userImages = user.getUserImages(); if
                 * (userImages != null && !userImages.isEmpty()) { for
                 * (UserImage image : userImages) {
                 * 
                 * if
                 * ((image.getImageType()).equalsIgnoreCase(ImageTypeEnum.USERIMAGE
                 * .getImageType())) { UserOutputModel userModel = new
                 * UserOutputModel(user.getId(), user.getName(), "User", null,
                 * null, null); userModel.setImage(image.getImage());
                 * //searchOutputModel.setImage(image.getImage()); } } } else {
                 * UserOutputModel userModel = new UserOutputModel(user.getId(),
                 * user.getName(), "User", null, null, null); } } catch
                 * (Exception e) { e.printStackTrace(); }
                 */

                searchOutputModel.setUrl("#/personality/?id=" + user.getId());
                searchOutputModel.setFormattedAge(DateUtils.getDateOrTimeAgo(user.getCreateTime()));
                searchOutputModel.setUser(user);
                searchOutputModel.setAgeString(DateUtils.calculateAgeWithString(user.getDob()));

                if (userId != null) {
                    UserConnection userConnection = userService.findUserConnectionByUserAndConnectionId(userId,
                            user.getId());
                    if (userConnection != null && userConnection.getConnectionStatus() != null) {
                        searchOutputModel.setConnectionStatus(userConnection.getConnectionStatus());
                    } else {
                        searchOutputModel.setConnectionStatus("AddConnection");
                    }
                } else {
                    searchOutputModel.setConnectionStatus("AddConnection");
                }
                if (user.getId().equals(userId) || user.getStatus().equals("Deleted")) {

                } else {
                    searchOutputModels.add(searchOutputModel);
                }

            }
        }
    }
}
