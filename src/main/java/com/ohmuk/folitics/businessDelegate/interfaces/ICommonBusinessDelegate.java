package com.ohmuk.folitics.businessDelegate.interfaces;

/**
 * @author Abhishek
 *
 */
public interface ICommonBusinessDelegate {

    public boolean incrementView(Long opinionId) throws Exception;
}
