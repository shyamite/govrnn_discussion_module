package com.ohmuk.folitics.businessDelegate.interfaces;

import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.ohmuk.folitics.charting.beans.OpinionAggregation;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.ouput.model.OpinionOutputModel;
import com.ohmuk.folitics.ouput.model.PastOpinionOutputModel;

/**
 * @author Abhishek
 *
 */
public interface IOpinionBusinessDelegate {

    /**
     * Business delegate method to create opinion by calling
     * com.ohmuk.folitics.service.IOpinionService.create
     * 
     * @author Abhishek
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion Opinion
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     * @throws Exception
     */
    public Opinion create(Opinion opinion) throws Exception;

    /**
     * Business Delegate method to get opinion object using
     * com.ohmuk.folitics.service.IOpinionService.read by passing the id
     * 
     * @author Abhishek
     * @param Long
     *            id
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     * @throws Exception
     */
    public Opinion read(Long id) throws Exception;

    /**
     * Business Delegate method to get opinion object using
     * com.ohmuk.folitics.service.IOpinionService.readAll
     * 
     * @author Abhishek
     * @return java.util.List<com.ohmuk.folitics.jpa.entity.Opinion>
     * @throws Exception
     */
    public List<Opinion> readAll() throws Exception;

    /**
     * Business Delegate method to update opinion using
     * com.ohmuk.folitics.service.IOpinionService.update
     * 
     * @author Abhishek
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion opinion
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     * @throws Exception
     */
    public Opinion update(Opinion opinion) throws Exception;

    /**
     * Business Delegate method to soft delete an opinion by giving id
     * 
     * @author Abhishek
     * @param Long
     *            id
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     * @throws Exception
     */
    public Opinion delete(Long id) throws Exception;

    /**
     * Business Delegate method to soft delete an opinion by giving object of
     * com.ohmuk.folitics.jpa.entity.Opinion
     * 
     * @author Abhishek
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion opinion
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     * @throws Exception
     */
    public Opinion delete(Opinion opinion) throws Exception;

    /**
     * Business Delegate method to permanent delete an opinion using
     * com.ohmuk.folitics.service.IOpinionService.delete by passing id
     * 
     * @author Abhishek
     * @param Long
     *            id
     * @return boolean
     * @throws Exception
     */
    public boolean deleteFromDB(Long id) throws Exception;

    /**
     * Business Delegate method to permanent delete an opinion using
     * com.ohmuk.folitics.service.IOpinionService.delete by passing object of
     * com.ohmuk.folitics.jpa.entity.Opinion
     * 
     * @author Abhishek
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion opinion
     * @return boolean
     * @throws Exception
     */
    public boolean deleteFromDB(Opinion opinion) throws Exception;

    public List<Opinion> getTopMostOpinion() throws Exception;

    /**
     * @param userId
     * @return List<Opinion>
     * @throws Exception
     */
    public List<Opinion> getOpinionByUser(Long userId) throws Exception;

    /**
     * Method to get opinion for a given sentiment id
     * 
     * @param userId
     * @return
     * @throws MessageException
     * @throws Exception
     */
    public List<Opinion> getOpinionForSentiment(Long userId) throws MessageException, Exception;

    /**
     * Method to get aggregation of opinion for a given sentiment Id
     * 
     * @param sentimentId
     * @return
     * @throws Exception
     */
    public OpinionAggregation getOpinionsAggregate(Long sentimentId) throws Exception;

    /**
     * @param sentimentId
     * @param componentId
     * @param componentType
     * @return
     * @throws Exception
     */
    public List<Opinion> getOpinionComponent(Long sentimentId, Long componentId, String componentType) throws Exception;

    /**
     * 
     * @param count
     * @return
     * @throws Exception
     */
    public List<OpinionOutputModel> getLatestOpinions(int count) throws Exception;

    /**
     * @param sentimentId
     * @param opinionType
     * @param count
     * @return
     * @throws Exception
     */
    public List<Opinion> getOpinionsByType(Long sentimentId, String opinionType, int count) throws Exception;

    public LinkedHashSet<Opinion> searchOpinion(String searchKeyword, Long sentimentId) throws Exception;

	PastOpinionOutputModel getPastOpinionOutputModel(Long userId) throws Exception;

	/**
	 * @param sentimentId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	List<Opinion> getOpinionForSentimentAndUser(Long sentimentId, Long userId) 	throws Exception;

    public String uploadOpinionImage(MultipartFile file, Long sentimentId, Long userId) throws Exception;

	List<OpinionOutputModel> getRelatedOpinions(Long id) throws Exception;
}
