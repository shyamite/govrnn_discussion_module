package com.ohmuk.folitics.excel.reader.input;

import java.io.IOException;

import org.apache.poi.ss.usermodel.Workbook;


public interface ExcelFileReader {
	Workbook getReader(String filePath) throws  IOException ;
}
