/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.poll.PollOption;

/**
 * @author Kalpana
 *
 */
public class PollOptionOutputModel extends BaseOutputModel<String> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String pollOption;
	
	/**
	 * @return the pollOption
	 */
	public String getPollOption() {
		return pollOption;
	}

	/**
	 * @param pollOption the pollOption to set
	 */
	public void setPollOption(String pollOption) {
		this.pollOption = pollOption;
	}

	public PollOptionOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<String> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
	}

	public static final PollOptionOutputModel getModel(PollOption entity) {
		PollOptionOutputModel model = new PollOptionOutputModel(entity.getId(),
				null, "Poll", null, null, null);
		model.setPollOption(entity.getPollOption());
		return model;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PollOptionOutputModel [pollOption=" + pollOption + "]";
	}
}
