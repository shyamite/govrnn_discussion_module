/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.ohmuk.folitics.cache.FoliticsCache;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.component.newsfeed.ImageTypeEnum;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.enums.FileType;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.UserImage;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.hibernate.entity.poll.PollOption;
import com.ohmuk.folitics.hibernate.entity.trend.TrendMapping;
import com.ohmuk.folitics.util.ThumbnailUtil;

/**
 * @author soumya
 *
 */
public class TrendMappingOutputModel extends BaseOutputModel<TrendMappingOutputModel> implements Serializable{

	private static final long serialVersionUID = 1L;

	private long trendId;
	private long trendmappingId;
	private Timestamp updateTimeStamp;

	private OpinionOutputModel opinionOutputModel;
	private ResponseOutputModel responseOutputModel;

	public TrendMappingOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<String> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, null, fromattedAge);
		
	}
	
	public static final TrendMappingOutputModel getModelFromOpinion(Opinion entity,long trendId, long trendmappingId) {
		TrendMappingOutputModel trendMappingOutputModel = new TrendMappingOutputModel(trendId,
				null, entity.getType(), null, null, entity.getFormattedAge());
		
		OpinionOutputModel model = new OpinionOutputModel(entity.getId(),
				entity.getSubject(), entity.getType(), null, null, entity.getFormattedAge());
		model.setImageUrl(entity.getImageUrl());
		model.setImage(entity.getSentiment().getImage());
		model.setSentimentId(entity.getSentiment().getId());
		model.setSentimentName(entity.getSentiment().getSubject());		
		model.setSentimentCategory(entity.getSentiment().getType());
		model.setText(entity.getText());
		model.setAgreeDisagrePoll(PollOutputModel.getModel(entity.getAgreeDisagrePoll()));
		model.setUser(UserOutputModel.getModel(entity.getUser()));
		//model.setUserId(entity.getUser().getId());
		//model.setUserName(entity.getUser().getName());
		model.setLink(LinkOutputModel.getModel(entity.getLink()));	
		trendMappingOutputModel.setTrendId(trendId);
		trendMappingOutputModel.setTrendmappingId(trendmappingId);
		trendMappingOutputModel.setOpinionOutputModel(model);
		return trendMappingOutputModel;
	}


	public static final TrendMappingOutputModel getModelFromResponse(Response entity,long trendId, long trendmappingId) {
		TrendMappingOutputModel trendMappingOutputModel = new TrendMappingOutputModel(trendId,
				null, ComponentType.RESPONSE.getValue(), null, null, entity.getFormattedAge());
		
		ResponseOutputModel model = new ResponseOutputModel(entity.getId(),
				null,  ComponentType.RESPONSE.getValue(), null, null, entity.getFormattedAge());
		//model.setImage(entity.getUser().get);
		model.setUserName(entity.getUser().getName());
		try {
			for (UserImage userImage : entity.getUser().getUserImages()) {
				if(userImage.getImageType().equals(ImageTypeEnum.USERIMAGE.getImageType())){
						model.setUserImage(ThumbnailUtil.getImageThumbnail(userImage.getImage(),FileType.JPEG.getValue()));
				}
			}
			if(model.getUserImage()==null){
				model.setUserImage((byte[])SingletonCache.getInstance().get(FoliticsCache.KEYS.USER_IMAGE));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setOpinionId(entity.getOpinion().getId());
		model.setFlag(entity.getFlag());
		model.setContent(entity.getContent());
		model.setEdited(entity.getEdited());
		model.setPollId(entity.getUpDownVote().getId());	
		
		trendMappingOutputModel.setTrendId(trendId);
		trendMappingOutputModel.setTrendmappingId(trendmappingId);
		trendMappingOutputModel.setResponseOutputModel(model);
		return trendMappingOutputModel;
	}

	/**
	 * @return the trendId
	 */
	public long getTrendId() {
		return trendId;
	}

	/**
	 * @param trendId
	 *            the trendId to set
	 */
	public void setTrendId(long trendId) {
		this.trendId = trendId;
	}

	/**
	 * @return the trendmappingId
	 */
	public long getTrendmappingId() {
		return trendmappingId;
	}

	/**
	 * @param trendmappingId
	 *            the trendmappingId to set
	 */
	public void setTrendmappingId(long trendmappingId) {
		this.trendmappingId = trendmappingId;
	}

	/**
	 * @return the opinionOutputModel
	 */
	public OpinionOutputModel getOpinionOutputModel() {
		return opinionOutputModel;
	}

	/**
	 * @param opinionOutputModel the opinionOutputModel to set
	 */
	public void setOpinionOutputModel(OpinionOutputModel opinionOutputModel) {
		this.opinionOutputModel = opinionOutputModel;
	}

	/**
	 * @return the responseOutputModel
	 */
	public ResponseOutputModel getResponseOutputModel() {
		return responseOutputModel;
	}

	/**
	 * @param responseOutputModel the responseOutputModel to set
	 */
	public void setResponseOutputModel(ResponseOutputModel responseOutputModel) {
		this.responseOutputModel = responseOutputModel;
	}

	/**
	 * @return the updateTimeStamp
	 */
	public Timestamp getUpdateTimeStamp() {
		return updateTimeStamp;
	}

	/**
	 * @param updateTimeStamp the updateTimeStamp to set
	 */
	public void setUpdateTimeStamp(Timestamp updateTimeStamp) {
		this.updateTimeStamp = updateTimeStamp;
	}
	
	@Override
	public int compareTo(Object o) {
		TrendMappingOutputModel o1= (TrendMappingOutputModel)o;
		if (getUpdateTimeStamp() == null || o1.getUpdateTimeStamp() == null)
			return 0;
		return getUpdateTimeStamp().compareTo(o1.getUpdateTimeStamp());

	}
}
