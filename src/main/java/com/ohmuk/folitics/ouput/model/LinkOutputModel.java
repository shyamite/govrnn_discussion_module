/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.Link;

/**
 * @author Kalpana
 *
 */
public class LinkOutputModel extends BaseOutputModel<String> implements
		Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String description;
	private String link;
	private String iconUrl;

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public LinkOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<String> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
	}

	public static final LinkOutputModel getModel(Link entity) {
		if (entity != null) {
			LinkOutputModel model = new LinkOutputModel(entity.getId(), null,
					"Link", null, null, null);
			model.setDescription(entity.getDescription());
			model.setLink(entity.getLink());
			model.setIconUrl(getIconUrlForFile(entity.getLink()));
			return model;
		}
		return null;
	}

}
