/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;

/**
 * @author Kalpana
 *
 */
public class PastOpinionOutputModel extends BaseOutputModel<String> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long support;
	private Long against;
	private List<PastOpinionOutputModel.OpinionByMonth> opinionByMonths;
	
	public class OpinionByMonth
	{
		private String month;
		private Date dummyDate;
		private int support;
		private int against;
		/**
		 * @return the month
		 */
		public String getMonth() {
			return month;
		}
		/**
		 * @param month the month to set
		 */
		public void setMonth(String month) {
			this.month = month;
		}
		/**
		 * @return the support
		 */
		public int getSupport() {
			return support;
		}
		/**
		 * @param support the support to set
		 */
		public void setSupport(int support) {
			this.support = support;
		}
		/**
		 * @return the against
		 */
		public int getAgainst() {
			return against;
		}
		/**
		 * @param against the against to set
		 */
		public void setAgainst(int against) {
			this.against = against;
		}
		/**
		 * @return the dummyDate
		 */
		public Date getDummyDate() {
			try {
			 if(getMonth() != null && !getMonth().isEmpty()){
				 dummyDate = new SimpleDateFormat("dd-MMM-yyyy").parse("01-"+getMonth()); 
			 }
			 else{
					dummyDate = new SimpleDateFormat("dd-MMM-yyyy").parse(new Date().toString());
			 }
				
			} catch (ParseException e) {
				e.printStackTrace();
			
			}
			return dummyDate;
		}
		/**
		 * @param dummyDate the dummyDate to set
		 */
		public void setDummyDate(Date dummyDate) {
			this.dummyDate = dummyDate;
		}
		
	
	}
	/**
	 * @return the support
	 */
	public Long getSupport() {
		return support;
	}
	/**
	 * @param support the support to set
	 */
	public void setSupport(Long support) {
		this.support = support;
	}
	/**
	 * @return the against
	 */
	public Long getAgainst() {
		return against;
	}
	/**
	 * @param against the against to set
	 */
	public void setAgainst(Long against) {
		this.against = against;
	}
	/**
	 * @return the opinionByMonths
	 */
	public List<PastOpinionOutputModel.OpinionByMonth> getOpinionByMonths() {
		return opinionByMonths;
	}
	/**
	 * @param opinionByMonths the opinionByMonths to set
	 */
	public void setOpinionByMonths(List<PastOpinionOutputModel.OpinionByMonth> opinionByMonths) {
		this.opinionByMonths = opinionByMonths;
	};
	
	
	/*public static final GetPastOpinionOutputModel getModel(Opinion entity) {
		GetPastOpinionOutputModel model = new GetPastOpinionOutputModel(entity.getId(),
				null, entity.getType(), null, null, entity.getFormattedAge());
		model.setSubject(entity.getSubject());
		model.setText(entity.getText());
		if(entity.getAttachment() != null)
			model.setAttachment(entity.getAttachment());
		model.setCreated_By(entity.getCreated_By());
		if(entity.getAgreeDisagrePoll() != null)
			model.setAgreeDisagreePoll(PollOutputModel.getModel(entity.getAgreeDisagrePoll()));
		return model;
	}*/


	
}
