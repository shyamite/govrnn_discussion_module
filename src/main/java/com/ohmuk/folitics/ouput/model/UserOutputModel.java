package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.ohmuk.folitics.cache.FoliticsCache;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.component.newsfeed.ImageTypeEnum;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserImage;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.ImageUtil;

public class UserOutputModel extends BaseOutputModel<String> implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    byte[] image;
    private String name;
    private String userName;
    private String emailId;
    private String state;
    private String city;
    private String currentLocation;
    private String sex;
    private String ageString;
    private String connectionStatus;

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * @param emailId
     *            the emailId to set
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the currentLocation
     */
    public String getCurrentLocation() {
        StringBuilder sb = new StringBuilder();
        if (currentLocation != null) {
            sb.append(currentLocation);
        }
        if (city != null) {
            if(sb.length()>0)
            	sb.append(", ");
            sb.append(city);
        }
        if (state != null) {
        	if(sb.length()>0)
            	sb.append(", ");
            sb.append(state);
        }
        return sb.toString();
    }

    /**
     * @param currentLocation
     *            the currentLocation to set
     */
    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public UserOutputModel() {
        super();
    }

    public UserOutputModel(Long id, String title, String type, UIMetrics uiMetrics, List<String> otherAttributes,
            String fromattedAge) {
        super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
    }

    @Override
    public String toString() {
        return "UserModel [image=" + Arrays.toString(image) + "]";
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAgeString() {
        return ageString;
    }

    public void setAgeString(String ageString) {
        this.ageString = ageString;
    }

    public String getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(String connectionStatus) {
        this.connectionStatus = connectionStatus;
    }

    public static final UserOutputModel getModel(User entity) {
        UserOutputModel model = new UserOutputModel(entity.getId(), entity.getName(), "User", null, null,
                DateUtils.getDateOrTimeAgo(entity.getCreateTime()));
        UserImage ui = ImageUtil.getUserImage(ImageTypeEnum.USERIMAGE.getImageType(), entity.getUserImages());
        if (ui != null) {
            model.setImage(ui.getImage());
            model.setImageFileType(ui.getFileType());
        } else {
            model.setImage((byte[]) SingletonCache.getInstance().get(FoliticsCache.KEYS.USER_IMAGE));
            model.setImageFileType("jpg");
        }
        model.setName(entity.getName());

        model.setUserName(entity.getUsername());
        model.setEmailId(entity.getEmailId());
        model.setState(entity.getState());
        model.setCity(entity.getCity());
        model.setCurrentLocation(entity.getCurrentLocation());
        model.setSex(entity.getGender());
        model.setAgeString(DateUtils.calculateAgeWithString(entity.getDob()));
        return model;
    }
}
