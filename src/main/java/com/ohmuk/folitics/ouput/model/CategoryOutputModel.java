/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.Category;

/**
 * @author Kalpana
 *
 */
public class CategoryOutputModel extends BaseOutputModel<String> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public CategoryOutputModel() {
		super();
	}

	public CategoryOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<String> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
		// TODO Auto-generated constructor stub
	}

	
	public static final CategoryOutputModel getModel(Category entity) {
		CategoryOutputModel model = new CategoryOutputModel(entity.getId(),
				 null, entity.getType(),null, null, null);
		model.setName(entity.getName());
		return model;
	}
	
	 
}
