package com.ohmuk.folitics.mongodb.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.ohmuk.folitics.MongoAutoConfiguration;
import com.ohmuk.folitics.mongodb.entity.NotificationMappingMongo;

/**
 * @author Abhishek
 *
 */
@Service
public class NotificationMappingMongodbDAO {

    @Autowired
    private MongoAutoConfiguration mongoAutoConfiguration;

    private DBCollection getDBCollection() {
        DB db = mongoAutoConfiguration.getMongo().getDB(mongoAutoConfiguration.getProperties().getDatabase());
        DBCollection collection = db.getCollection("NotificationMapping");
        return collection;
    }

    private List<NotificationMappingMongo> getNotificationMappingMongo(DBCursor cursor, List<NotificationMappingMongo> nfs) throws Exception {

        while (cursor.hasNext()) {

            DBObject obj = cursor.next();
            NotificationMappingMongo nf = mongoAutoConfiguration.getMongoTemplate().getConverter()
                    .read(NotificationMappingMongo.class, obj);
            nfs.add(nf);
        }
        return nfs;
    }

    public boolean deleteByUserId(Long userId) throws Exception {
    	DBCursor cursor = getDBCollection().find(new BasicDBObject("userId", userId));
    	 List<NotificationMappingMongo> nfs = new ArrayList<NotificationMappingMongo>();         
    	nfs = getNotificationMappingMongo(cursor, nfs);	
    	for(NotificationMappingMongo notificationMongo : nfs){
    		getDBCollection().remove(new BasicDBObject("userId",userId));
    	}
        
    	  cursor = getDBCollection().find(new BasicDBObject("userId", userId));
    	  nfs = new ArrayList<NotificationMappingMongo>();         
    	  nfs = getNotificationMappingMongo(cursor, nfs);	
   	
    	  if(nfs.isEmpty()){

    	        return true;  
    	  }else{
    		  return false;
    	  }
        
    }
}
