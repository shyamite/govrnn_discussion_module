package com.ohmuk.folitics.mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ohmuk.folitics.mongodb.entity.ErrorLog;

/**
 * @author Jahid Ali
 */
public interface IErrorLogRepository extends MongoRepository<ErrorLog, String> {
}
