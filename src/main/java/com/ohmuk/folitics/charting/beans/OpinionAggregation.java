package com.ohmuk.folitics.charting.beans;

import java.io.Serializable;

public class OpinionAggregation implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    Long sentimentid;
    
    double  support;
    
    double  against;

    int totalVotes;
	/**
	 * @return the sentimentid
	 */
	public Long getSentimentid() {
		return sentimentid;
	}

	/**
	 * @param sentimentid the sentimentid to set
	 */
	public void setSentimentid(Long sentimentid) {
		this.sentimentid = sentimentid;
	}

	/**
	 * @return the support
	 */
	public double getSupport() {
		return support;
	}

	/**
	 * @param support the support to set
	 */
	public void setSupport(double support) {
		this.support = support;
	}

	/**
	 * @return the against
	 */
	public double getAgainst() {
		return against;
	}

	/**
	 * @param against the against to set
	 */
	public void setAgainst(double against) {
		this.against = against;
	}

	public int getTotalVotes() {
		return totalVotes;
	}

	public void setTotalVotes(int totalVotes) {
		this.totalVotes = totalVotes;
	}
    
    
    
}
