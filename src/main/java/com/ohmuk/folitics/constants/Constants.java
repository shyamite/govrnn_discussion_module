package com.ohmuk.folitics.constants;

/**
 * @author Abhishek
 *
 */
public class Constants {

    public static final Long OPINION_WEIGHT = 5l;
    public static final Long RESPONSE_WEIGHT = 1l;
    public static final String FOLLOW_COUNT_SERVICE_SUFFIX = "FollowCountService";
    public static final String SHARE_SERVICE_SUFFIX = "ShareService";

    public static final String LIKE_SERVICE_SUFFIX = "LikeService";
    public static final String LIKE_COUNT_SERVICE_SUFFIX = "LikeCountService";
    public static final String SHARE_COUNT_SERVICE_SUFFIX = "ShareCountService";

    public static final String COMMENT_COUNT_SERVICE_SUFFIX = "CommentCountService";
    public static final String COMMENT_SERVICE_SUFFIX = "CommentService";

    public static final String RATING_SERVICE_SUFFIX = "RatingService";

    public static final boolean useElasticSearch = false;

    public static final String Opinion = "Opinion";
    public static final String People = "People";
    public static final String Sentiment = "Sentiment";
    public static final String ALL = "ALL";
    
    public static final String RESPONSE = "Response";
    public static final String OPINION = "Opinion";
    public static final String UP = "Up";
    public static final String DOWN = "Down";
    public static final String AGREE = "Agree";
    public static final String DISAGREE = "Disagree";
    public static final String PERMANENT = "Permanent";
    public static final String TEMPORARY = "Temporary";
    public static final String AGRICULTURE = "Agriculture";
    public static final String AIRWAYS = "Airways";
    public static final String BANKING = "Banking";
    public static final String CHILD = "Child";
    public static final String COMMERCE_TRADE = "Commerce and Trade";
    public static final String COMMUNALISM = "Communalism";
    public static final String CRIME = "Crime";
    public static final String EDUCATION = "Education";
    public static final String HEALTH = "Health";
    public static final String POLICE = "Police";
    public static final String SCIENCE_AND_TECHNOLOGY = "Science & Technology";
    public static final String MEDIA_AND_ENTERTAINMENT = "Media and Entertainment";
    
}
