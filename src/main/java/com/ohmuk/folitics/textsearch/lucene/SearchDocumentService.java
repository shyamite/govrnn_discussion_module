package com.ohmuk.folitics.textsearch.lucene;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

import com.ohmuk.folitics.hibernate.entity.SentimentNews;

public class SearchDocumentService {

	// public static void main(String[] args) {
	// Analyzer analyzer = new StandardAnalyzer();
	// try {
	// Directory index = new RAMDirectory();
	// // Directory index = FSDirectory.open(new File("index-dir"));
	// IndexWriterConfig config = new IndexWriterConfig(analyzer);
	// IndexWriter writer = new IndexWriter(index, config);
	//
	// // Add some Document objects containing quotes
	// String link1 = "www.ndtv.com";
	// String link2 = "www.bbc.com";
	// String link3 = "www.pti.com";
	// createDocument(
	// writer,
	// link1,
	// "Theodore Roosevelt",
	// "It behooves every man to remember that the work of the "
	// + "critic, is of altogether secondary importance, and that, "
	// + "in the end, progress is accomplished by the man who does "
	// + "things.");
	// createDocument(
	// writer,
	// link2,
	// "Friedrich Hayek",
	// "The case for individual freedom rests largely on the "
	// + "recognition of the inevitable and universal ignorance "
	// + "of all of us concerning a great many of the factors on "
	// + "which the achievements of our ends and welfare depend.");
	// createDocument(
	// writer,
	// link3,
	// "Ayn Rand",
	// "There is nothing to take a man's freedom away from "
	// + "him, save other men. To be free, a man must be free "
	// + "of his brothers.");
	// createDocument(writer, link3, "Mohandas Gandhi",
	// "Freedom is not worth having if it does not connote "
	// + "freedom to err.");
	//
	// // Optimize and close the writer to finish building the index
	// writer.close();
	//
	// String querystr = "Friedrich";
	// // Query q = new QueryParser("title", analyzer).parse(querystr);
	// Query q = new QueryParser("title", analyzer).parse(querystr);
	//
	// int hitsPerPage = 10;
	// IndexReader reader = DirectoryReader.open(index);
	// IndexSearcher searcher1 = new IndexSearcher(reader);
	// TopScoreDocCollector collector = TopScoreDocCollector
	// .create(hitsPerPage);
	// searcher1.search(q, collector);
	// ScoreDoc[] hits = collector.topDocs().scoreDocs;
	//
	// System.out.println("Query string: " + querystr);
	// System.out.println("Found " + hits.length + " hits.");
	// for (int i = 0; i < hits.length; ++i) {
	// int docId = hits[i].doc;
	// Document d = searcher1.doc(docId);
	// System.out.println((i + 1) + ". " + d.get("plainText") + "\t"
	// + d.get("title") + "\t" + d.get("link"));
	// }// Finally , close reader
	// } catch (IOException ioe) {
	// // In this example we aren't really doing an I/O, so this
	// // exception should never actually be thrown.
	// ioe.printStackTrace();
	// } catch (org.apache.lucene.queryparser.classic.ParseException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	public static List<SentimentNews> search(List<String> keywords,
			List<SentimentNews> sentimentNews) {
		Analyzer analyzer = new StandardAnalyzer();
		Map<String, SentimentNews> newsLinkToSentimentNewsMap = new HashMap<String, SentimentNews>();
		List<SentimentNews> filterdNews = new ArrayList<SentimentNews>();
		try {
			Directory index = new RAMDirectory();
			// Directory index = FSDirectory.open(new File("index-dir"));
			IndexWriterConfig config = new IndexWriterConfig(analyzer);
			IndexWriter writer = new IndexWriter(index, config);

			// Add some Document objects containing quotes
			for (SentimentNews news : sentimentNews) {
				String link = news.getLink();
				String title = news.getTitle();
				String plainText = news.getPlainText();
				newsLinkToSentimentNewsMap.put(link, news);
				createDocument(writer, link, title, plainText);
			}

			// Optimize and close the writer to finish building the index
			writer.close();

			String querystr = getKeywordsString(keywords);
			QueryParser titleQueryParser = new QueryParser("title", analyzer);
			//titleQueryParser.setDefaultOperator(QueryParser.Operator.AND);
			Query titleSearch = titleQueryParser.parse(querystr);
			searcher(filterdNews, newsLinkToSentimentNewsMap, titleSearch,
					index, querystr);
//			QueryParser textQueryParser = new QueryParser("plainText", analyzer);
//			textQueryParser.setDefaultOperator(QueryParser.Operator.AND);
//			Query textSearch = textQueryParser.parse(querystr);
//			searcher(filterdNews, newsLinkToSentimentNewsMap, textSearch,
//					index, querystr);
		} catch (IOException ioe) {
			// In this example we aren't really doing an I/O, so this
			// exception should never actually be thrown.
			ioe.printStackTrace();
		} catch (org.apache.lucene.queryparser.classic.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return filterdNews;
	}

	private static void searcher(List<SentimentNews> filterdNews,
			Map<String, SentimentNews> newsLinkToSentimentNewsMap,
			Query query, Directory index, String querystr)
			throws IOException {
		int hitsPerPage = 10;
		IndexReader reader = DirectoryReader.open(index);
		IndexSearcher searcher = new IndexSearcher(reader);
		TopScoreDocCollector collector = TopScoreDocCollector
				.create(hitsPerPage);
		searcher.search(query, collector);
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		System.out.println("Query string: " + querystr);
		System.out.println("Found " + hits.length + " hits.");
		for (int i = 0; i < hits.length; ++i) {
			int docId = hits[i].doc;
			Document d = searcher.doc(docId);
			System.out.println((i + 1) + ". " + d.get("plainText") + "\t"
					+ d.get("title") + "\t" + d.get("link"));
			String link = d.get("link");
			filterdNews.add(newsLinkToSentimentNewsMap.get(link));
		}
	}

	private static void createDocument(IndexWriter w, String link,
			String title, String newsText) throws IOException {
		Document doc = new Document();
		doc.add(new TextField("link", link, Field.Store.YES));
		doc.add(new TextField("title", title, Field.Store.YES));
		// Here, we use a string field for course_code to avoid tokenizing.
		doc.add(new StringField("plainText", newsText, Field.Store.YES));
		w.addDocument(doc);
	}

	private static String getKeywordsString(List<String> keywords) {
		StringBuilder sb = new StringBuilder();
		for (String s : keywords) {
			sb.append(s).append(" ");
		}
		return sb.toString();
	}

}