'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the govrnnApp
 */
angular.module('govrnnApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
