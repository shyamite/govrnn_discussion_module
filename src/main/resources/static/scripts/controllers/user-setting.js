'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:UserSettingCtrl
 * @description
 * # UserSettingCtrl
 * Controller of the govrnnApp
 * url:http://localhost:8080/#/userSetting/2
 */
angular.module('govrnnApp')
	.controller('UserSettingCtrl', ['$http', '$scope', '$rootScope','$routeParams', '$location', 'UserSettingService', '$localStorage', '$route',
		function ($http, $scope, $rootScope, $routeParams, $location, UserSettingService, $localStorage, $route) {
		
		 $scope.$storage = $localStorage;	
		 $scope.tab = 1;
		 $scope.message="";
		 $scope.confirmPassword="";
		 if($scope.isUserLoggedIn() == true){
		 $scope.userId=$scope.getLoggedInUserId();}else{
			 $scope.loginNow();
		 }
		 $scope.disabled=true;
		 $scope.pwdIsClick=false;
		  $scope.ifAnyChange=true;
		
		$scope.changePassword={
				id:$scope.userId,
			    currentPassword:"",
		        newPassword: ""
		     // confirmPassword: ""
		 };
		$scope.personalDetails={
				id:$scope.userId,
				name:"",
				emailId:"",
				dob:"",
				address:"",
				mobileNumber:"",
				gender:"",
				maritalStatus:"",
				state:"",
				city:""
		}
		
		/*========================state-city==================================================*/
		// Countries
		var state_arr = new Array("Select State","Andaman and Nicobar Island (UT)","Andhra Pradesh","Arunachal Pradesh","Assam",
				"Bihar","Chandigarh(UT)","Chhattisgarh","Dadra and Nagar Haveli(UT)","Daman and Diu (UT)","Delhi (NCT)",
				"Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala",
				"Lakshadweep(UT)","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Odisha",
				"Puducherry(UT)","Punjab","Rajasthan","Sikkim","Tamil Nadu","Telangana","Tripura","Uttarakhand",
				"Uttar Pradesh","West Bengal");

        $.each(state_arr, function (i, item) {
            $('#state').append($('<option>', {
                value: i,
                text : item,
            }, '</option>' ));
        });

        
        
        // States
        var s_a = new Array();
        s_a[0]="Select City";
        
        s_a[1]="Select City|Nicobar|North and Middle Andaman|South Andaman";
        
        s_a[2]="Select City|Anantapur|Chittoor|East Godavari|Guntur|YSR Kadapa|Krishna|Kurnool|Potti sriramulu Nellore|Prakasam|" +
        		"Srikakulam|Srikakulam|Visakhapatnam|Vizianagaram|West Godavari";
        
        s_a[3]="Select City|Anjaw|Central Siang|Changlang|East Siang|East Kameng|Kra Daadi|Kurung Kumey|Lohit|Lower Dibang Valley|" +
        		"Longding|Namsai|Papum Pare|Upper Dibang Valley|Upper Siang|Upper Subansiri|Tawang|Tirap|West Kameng|West Siang";
        
        s_a[4]="Select City|Baksa|Barpeta|Biswanath|Bongaigaon|Cachar|Charaideo|Chirang|Darrang|Dhemaji|Dhubri|Dibrugarh|Goalpara|Golaghat|Hailakandi|Hojai|" +
				"Jorhat|Kamrup Metropolitan|Kamrup|Karbi Anglong|Karimganj|Kokrajhar|Lakhimpur|Majuli|Morigaon|Nagaon|Nalbari|Dima Hasao|Sivasagar|Sonitpur|" +
				"South Salmara-Mankachar|Tinsukia|Udalguri|West Karbi Anglong";
        
        s_a[5]="Select City|Araria|Arwal|Aurangabad|Banka|Begusarai|Bhagalpur|Bhojpur|Buxar|Darbhanga|East Champaran|Gaya|Gopalganj|Jamui|Jehanabad|Khagaria|" +
        		"Kishanganj|Kaimur|Katihar|Lakhisarai|Madhubani|Munger|Madhepura|Muzaffarpur|Nalanda|Nawada|Patna|Purnia|Rohtas|Saharsa|" +
				"Samastipur|Sheohar|Sheikhpura|Saran|Sitamarhi|Supaul|Siwan|Vaishali|West Champaran	";
        
        s_a[6]="Select City|Chandigarh";
        
        s_a[7]="Select City|Balod|Baloda Bazar|Balrampur|Bemetara|Bijapur|Bilaspur|Dantewada|Dhamtari|Durg|Gariaband|Jagdalpur|Janjgir-Champa|Jashpur|Kabirdham|Khagaria|" +
				"Kondagaon|Korba|Koriya|Mahasamund|Mungeli|Narayanpur|Raigarh|Raipur|Rajnandgaon|Sukma|Surajpur|Surguja";
        
        s_a[8]="Select City|Dadra & Nagar Haveli";
        
        s_a[9]="Select City|Daman|Diu";
        
        s_a[10]="Select City|Central Delhi|East Delhi|New Delhi|North Delhi|North East Delhi|Shahdara|South Delhi|South East Delhi|South West Delhi|West Delhi";
        
        s_a[11]="Select City|North Goa|South Goa";
        
        s_a[12]="Select City|Anand|Aravalli|Banaskantha (Palanpur)|Bharuch|Bhavnagar|Botad|Chhota Udepur|Dahod|Dangs (Ahwa)|Devbhoomi Dwarka|Gandhinagar|Gir Somnath|Jamnagar|Junagadh|" +
        		"Kachchh|Kheda (Nadiad)|Mahisagar|Mehsana|Morbi|Narmada (Rajpipla)|Navsari|Panchmahal (Godhra)|Patan|Porbandar|Rajkot|Sabarkantha (Himmatnagar)|Surat|Surendranagar|Tapi (Vyara)|Vadodara|Valsad";
   
        s_a[13]="Select City|Ambala|Bhiwani|Charkhi Dadri|Faridabad|Fatehabad|Gurgaon|Hisar|Jhajjar|Jind|Kaithal|Karnal|Kurukshetra|Mahendragarh|Nuh|Palwal|Panchkula|Panipat|Rewari|Rohtak|Sirsa|Sonipat|Yamunanagar";
        
        s_a[14]="Select City|Bilaspur|Chamba|Hamirpur|Kangra|Kinnaur|Kullu|Lahaul & Spiti|Mandi|Shimla|Sirmaur (Sirmour)|Solan|Una";
        
        s_a[15]="Select City|Anantnag|Bandipore|Baramulla|Budgam|Doda|Ganderbal|Jammu|Kargil|Kathua|Kishtwar|Kulgam|Leh|Pulwama|Rajouri|Ramban|Reasi|Samba|Shopian|Srinagar|Udhampur";
        
        s_a[16]="Select City|Bokaro|Chatra|Deoghar|Dhanbad|Dumka|East Singhbhum|Garhwa|Giridih|Godda|Gumla|Jamtara|Khunti|Koderma|Latehar|Lohardaga|Pakur|Palamu|Ramgarh|Ranchi|Sahibganj|Seraikela-Kharsawan|Simdega|West Singhbhum";
        
        s_a[17]="Select City|Bagalkot|Ballari (Bellary)|Belagavi (Belgaum)|Bengaluru (Bangalore) Rural|Bengaluru (Bangalore) Urban|Bidar|Chamarajanagar|Chikballapur|Chikkamagaluru(Chikmagalur)|Chitradurga|Dakshina Kannada|Davangere|Dharwad|Gadag|Hassan|Haveri|Kalaburagi(Gulbarga)|Kolar|Koppal|Mandya|Raichur|Ramanagara|Shivamogga (Shimoga)|Tumakuru (Tumkur)|Udupi|Uttara Kannada (Karwar)|Vijayapura (Bijapur)|Yadgir";
        
        s_a[18]="Select City|Alappuzha|Ernakulam|Idukki|Kannur|Kasaragod|Kollam|Kottayam|Kozhikode|Malappuram|Palakkad|Pathanamthitta|Thrissur|Wayanad";
        
        s_a[19]="Select City|Lakshadweep";
        
        s_a[20]="Select City|AgarMalwa|Alirajpur|Anuppur|Balaghat|Betul|Bhind|Bhopal|Burhanpur|Chhatarpur|Chhindwara|Datia|Dewas|Dhar|Guna|Gwalior|Harda|Indore|Jabalpur|Jhabua|Katni|Khandwa|Khargone|Mandla|Mandsaur|Morena|Narsinghpur|Panna|Raisen|Rajgarh|Ratlam|Rewa|Sagar|Satna|Sehore|Seoni|Shajapur|Sheopur|Shivpuri|Sidhi|Singrauli|Tikamgarh|Ujjain|Umaria|Vidisha";
        
        s_a[21]="Select City|Ahmednagar|Akola|Amravati|Beed|Bhandara|Buldhana|Chandrapur|Gadchiroli|Gondia|Hingoli|Jalgaon|Jalna|Kolhapur|Latur|Mumbai City|Mumbai Suburban|Nagpur|Nanded|Nandurbar|Nashik|Osmanabad|Palghar|Parbhani|Pune|Raigad|Ratnagiri|Satara|Sindhudurg|Solapur|Thane|Wardha|Washim|Yavatmal";
        
        s_a[22]="Select City|Bishnupur|Chandel|Churachandpur|Imphal East|Imphal West|Jiribam|Kamjong|Kangpokpi|Pherzawl|Senapati|Tengnoupal|Thoubal|Ukhrul";
       
        s_a[23]="Select City|East Garo Hills|East Jaintia Hills|East Khasi Hills|North Garo Hills|Ri Bhoi|South Garo Hills|South West Garo Hills|West Garo Hills|West Jaintia Hills|West Khasi Hills";
        
        s_a[24]="Select City|Aizawl|Champhai|Kolasib|Lawngtlai|Lunglei|Mamit|Saiha|Serchhip";
       
        s_a[25]="Select City|Dimapur|Kiphire|Kohima|Longleng|Mon|Peren|Phek|Tuensang|Wokha|Zunheboto";
        
        s_a[26]="Select City|Angul|Balangir|Balasore|Bargarh|Bhadrak|Boudh|Deogarh|Dhenkanal|Gajapati|Ganjam|Jajpur|Jharsuguda|Kalahandi|Kandhamal|Kendrapara|Kendujhar (Keonjhar)|Koraput|Malkangiri|Mayurbhanj|Nabarangpur|Nayagarh|Nuapada|Puri|Sambalpur|Sonepur|Sundargarh";
        
        s_a[27]="Select City|Karaikal| Mahe|Pondicherry|Yanam";
        
        s_a[28]="Select City|Amritsar|Barnala|Bathinda|Faridkot|Fatehgarh Sahib|Fazilka|Gurdaspur|Hoshiarpur|Jalandhar|Kapurthala|Ludhiana|Mansa|Moga|Muktsar|Nawanshahr (Shahid Bhagat Singh Nagar)|Pathankot|Patiala|Rupnagar|Sahibzada Ajit Singh Nagar (Mohali)|Sangrur|Tarn Taran";
        
        s_a[29]="Select City|Ajmer|Alwar|Banswara|Baran|Barmer|Bharatpur|Bhilwara|Bikaner|Bundi|Chittorgarh|Churu|Dausa|Dungarpur|Hanumangarh|Jaipur|Jaisalmer|Jalore|Jhalawar|Jhunjhunu|Jodhpur|Karauli|Kota|Nagaur|Pali|Rajsamand|Sawai Madhopur|Sikar|Sirohi|Sri Ganganagar|Tonk|Udaipur";
       
        s_a[30]="Select City|East Sikkim|North Sikkim|South Sikkim|West Sikkim";
        
        s_a[31]="Select City|Ariyalur|Chennai|Coimbatore|Cuddalore|Dharmapuri|Erode|Kanchipuram|Kanyakumari|Karur|Krishnagiri|Madurai|Nagapattinam|Namakkal|Nilgiris|Perambalur|Pudukkottai|Ramanathapuram|Salem|Sivaganga|Thanjavur|Theni|Thoothukudi (Tuticorin)|Tiruchirappalli|Tirunelveli|Tiruppur|Tiruvannamalai|Tiruvarur|Vellore|Viluppuram|Virudhunagar";
        
        s_a[32]="Select City|Adilabad|Bhadradri Kothagudem|Hyderabad|Jagtial|Jayashankar Bhoopalpally|Jogulamba Gadwal|Kamareddy|Karimnagar|Khammam|Komaram Bheem Asifabad|Mahabubabad|Mahabubnagar|Mancherial|Medak|Medchal|Nagarkurnool|Nalgonda|Nirmal|Nizamabad|Peddapalli|Rajanna Sircilla|Rangareddy|Sangareddy|Suryapet|Vikarabad|Wanaparthy|Warangal (Rural)|Warangal (Urban)|Yadadri Bhuvanagiri";
       
        s_a[33]="Select City|Dhalai|Gomati|Khowai|North Tripura|Sepahijala|South Tripura|Unakoti|West Tripura";
       
        s_a[34]="Select City|Almora|Bageshwar|Champawat|Dehradun|Nainital|Pauri Garhwal|Pithoragarh|Rudraprayag|Udham Singh Nagar|Uttarkashi"
       
        s_a[35]="Select City|Agra|Aligarh|Ambedkar Nagar|Amethi (Chatrapati Sahuji Mahraj Nagar)|Amroha (J.P. Nagar)|Auraiya|Azamgarh|Bahraich|Ballia|Balrampur|Banda|Barabanki|Bareilly|Basti|Bijnor|Budaun|Bulandshahr|Chandauli|Chitrakoot|Deoria|Etah|Etawah|Faizabad|Farrukhabad|Fatehpur|Firozabad|Gautam Buddha Nagar|Ghaziabad|Ghazipur|Gonda|Hamirpur|Hapur (Panchsheel Nagar)|Hardoi|Hathras|Jalaun|Jaunpur|Jhansi|Kannauj|Kanpur Dehat|Kanpur Nagar|Kanshiram Nagar (Kasganj)|Kaushambi|Kushinagar (Padrauna)|Lakhimpur - Kheri|Lalitpur|Maharajganj|Mainpuri|Mathura|Mau|Meerut|Mirzapur|Moradabad|Muzaffarnagar|Pilibhit|Pratapgarh|RaeBareli|Rampur|Saharanpur|Sambhal (Bhim Nagar)|Sant Kabir Nagar|Sant Ravidas Nagar|Shahjahanpur|Shamali (Prabuddh Nagar)|Shravasti|Siddharth Nagar|Sitapur|Sonbhadra|Sultanpur|Unnao|Varanasi"
        
        s_a[36]="Select City|Alipurduar|Bankura|Birbhum|Burdwan (Bardhaman)|Dakshin Dinajpur (South Dinajpur)|Darjeeling|Hooghly|Howrah|Jalpaiguri|Kalimpong|Kolkata|Malda|Murshidabad|Nadia|North 24 Parganas|Paschim Medinipur (West Medinipur)|Purba Medinipur (East Medinipur)|Purulia|Uttar Dinajpur (North Dinajpur)"	
        
        
        $('#state').change(function(){
            var s = $(this).val();
           // if(s!=user.state){$scope.ifAnyChange=false;}
            if(s=='Select State'){
                $('#city').empty();
                $('#city').append($('<option>', {
                    value: '0',
                    text: 'Select City',
                }, '</option>'));
            }
            var city_arr = s_a[s].split("|");
            $('#city').empty();
            $.each(city_arr, function (j, item_city) {
                $('#city').append($('<option>', {
                    value: item_city,
                    text: item_city,
                }, '</option>'));
               
            });

        });

 /*================================RELIGION-CASTE===================================================*/
       
        var religion_arr = new Array("Hindu","Islam","Christianity","Sikhism","Buddhism","Jainism","Parsi","other");
        $.each(religion_arr, function (i, item) {
            $('#religion').append($('<option>', {
                value: i,
                text : item,
            }, '</option>' ));
            
            if(item==$scope.user.religion){
            	//alert(item+" , "+i);
            	$('select>option:eq(i)').attr('selected', true);
            }
        });
        
        var caste_a = new Array();
        caste_a[0]="Select Caste";
        caste_a[1]="Select Caste|Bramhins|Thakur|Rajputs|Gupta|other";
        caste_a[2]="Select Caste|shekh|Shia|Sunni|other";
        caste_a[3]="Select Caste|priests|Nun| Malankara Jacobite Syrian Orthodox Church| Malankara Orthodox Syrian Church|Portuguese Latin Catholic missionaries|other";
        caste_a[4]="Select Caste|Singh|Kaur|Gaya|Begusarai|Choudhary|other";
        caste_a[5]="Select Caste|Tithiya|Mogra|Shah|Choudhary|Pipada|Jain|Sahlod|other";
        
        
        $('#religion').change(function(){
            var s = $(this).val();
            if(s=='Select Religion'){
                $('#caste').empty();
                $('#caste').append($('<option>', {
                    value: '0',
                    text: 'Select Caste',
                }, '</option>'));
            }
            var caste_arr = caste_a[s].split("|");
            $('#caste').empty();

            $.each(caste_arr, function (j, item_caste) {
                $('#caste').append($('<option>', {
                    value: item_caste,
                    text: item_caste,
                }, '</option>'));
                
                
            });


        });
        
 /*================================QUALIFICATION-COURSE==============================================*/       

        var quali_arr = new Array("Select Qualification","Post Graduate or Above","Undergraduate or Diploma","Intermediate (12th)","Higher Secondary (10th) or Below");
        
        $.each(quali_arr, function (i, item) {
            $('#qualification').append($('<option>', {
                value: i,
                text : item,
            }, '</option>' ));
        });
        
        var cou_a = new Array();
        cou_a[0]="Select Course";
        cou_a[1]="Select Course|MS|MA|M.Sc|M.Com|ME/Mtech|LLM|MCA|MBA|other";
        cou_a[2]="Select Course|BA|B.Sc|BBA|B.Com|BSC|BE|other";
        cou_a[3]="Select Course|HSS|SSS|8th|5th|other";
       
       
        $('#qualification').change(function(){
            var s = $(this).val();
            if(s=='Select qualification'){
                $('#course').empty();
                $('#course').append($('<option>', {
                    value: '0',
                    text: 'Select Course',
                }, '</option>'));
            }
            var course_arr = cou_a[s].split("|");
            $('#course').empty();

            $.each(course_arr, function (j, item_course) {
                $('#course').append($('<option>', {
                    value: item_course,
                    text: item_course,
                }, '</option>'));
            });


        });
/*==================================GET USER API CALL===============================================*/		
        if($scope.isUserLoggedIn() == true){
        $http.get('/user/find?id='+$scope.userId).then(function(response) {
				if (response.data.messages != null) {
					$scope.user = response.data.messages[0];	
					//$scope.personalDetails.id:"2",
					$scope.$storage.sessionData.user_image = $scope.user.userImages[0].image;
					$scope.userImage=$scope.user.userImages[0];
					$scope.personalDetails.name= $scope.user.name;
					$scope.personalDetails.emailId= $scope.user.emailId;
					$scope.personalDetails.caste= $scope.user.caste;
					//var date = new Date(parseInt(jsonDate.substr($scope.user.dob)));
					//alert(JSOG.stringify($scope.user.city));
					// $scope.user.dob;
					//alert(Date.UTC($scope.personalDetails.dob));
					var currentTime = new Date($scope.user.dob);
					var month = currentTime.getMonth() + 1;
					var day = currentTime.getDate();
					var year = currentTime.getFullYear();
					$scope.birthDate = day + "/" + month + "/" + year;
					//$scope.personalDetails.dob=$scope.birthDate;
					
					$scope.personalDetails.address= $scope.user.address;
					$scope.personalDetails.mobileNumber= $scope.user.mobileNumber;
					//alert($scope.personalDetails.mobileNumber);
					$scope.personalDetails.gender= $scope.user.gender;
					$scope.personalDetails.maritalStatus = $scope.user.maritalStatus;
					$scope.personalDetails.state = $scope.user.state;
					$scope.personalDetails.city = $scope.user.city;
					$scope.personalDetails.religion = $scope.user.religion;
					$scope.personalDetails.qualification = $scope.user.qualification;
					if($scope.user.maritalStatus=="Married"){$('#mar').prop('checked', true);}
					else{$('#unmar').prop('checked', true);}
					
					if($scope.user.gender=="Male"){$('#male').prop('checked', true);}
					else{$('#female').prop('checked', true);}
					
					 $.each(religion_arr, function (i, item) { 
						
				            if(i==$scope.user.religion || item==$scope.user.religion){
			                $('#religion').val($scope.user.religion).prop('selectedIndex', i);
			                	//alert(i);
			     
			                }
				        });
					 
					 $.each(quali_arr, function (i, item) { 
						// alert($scope.user.qualification);
				            if(item==$scope.user.qualification){
				            	//alert(i);
			                	$('#qualification').val($scope.user.qualification).prop('selectedIndex', i);
			                }
				        });
					 
					 $.each(state_arr, function (i, item) {  
				            if(i==$scope.user.state||item==$scope.user.state){
			                	$('#state').val($scope.user.state).prop('selectedIndex', i);
			                	//alert(i);
			                	var city_arr = s_a[i].split("|");		                		
			                			 $.each(city_arr, function (j, item_city) {
			             	                $('#city').append($('<option>', {
			             	                    value: item_city,
			             	                   text: item_city  }, '</option>'));                                      
			             	               if(item_city==$scope.user.city){
			             	            	   //alert();
							                	$('#city').val($scope.user.city).prop('selectedIndex', j);}
			             	            });

			                }
				        }); 
				}
			});
        }else{
		         $scope.loginNow();
	        }
/*==================================TAB IMPLEMENTATION==========================================*/
		    $scope.setTab = function(newTab){
		      $scope.tab = newTab;
		    };

		    $scope.isSet = function(tabNum){
		      return $scope.tab === tabNum;
		    };
/*===============================CHANGE PASSWORD SUBMISSION======================================*/
		    	$scope.submit = function() {
		    		 if($scope.isUserLoggedIn() == true){
		    	var password = $("#new_pas").val();
	            var confirmPassword = $("#conf_pas").val();
	            var currentPassword = $("#cur_pas").val();
	            if (!currentPassword.length) {
	            	$scope.message = "Please enter current password";
	            }
	            else if (password != confirmPassword) {
	            	 $("#new_pas").css("background", "red");
	                 $("#conf_pas").css("background", "red");
	                 //$("#register").prop("disabled", true);
	            	$scope.message = "Password and Confirm password not match";
	            	
	            }
	            else if(password!="" && confirmPassword!="" && password == confirmPassword){
	            	//if(password != $scope.user.password)
	            	{
	            		//$scope.changePassword.currentPassword= $scope.user.password;
	            		//console.log("hello password");
	            		 $("#cur_pas").css("background", "");
		                 $("#new_pas").css("background", "");
		                 $("#conf_pas").css("background", "");
	            		
	            		 //$("#cur_pas").css("border-color", "green");
		                 //$("#new_pas").css("border-color", "green");
		                 //$("#conf_pas").css("border-color", "green");
		                 
		                 var result = UserSettingService.editPassword($scope.changePassword);
	            		 //$('#new_pas').prop('disabled', true);
	            		  //if(result)
	            		  {
	            				$scope.message = "Your password is changed successfully";
	            				$('#conf_pas').prop('disabled', true);
	            				 $("#cur_pas").val('');
	    		                 $("#new_pas").val('');
	    		                 $("#conf_pas").val('');
	            		  }	            		 
	            		 $('#message').html('');
	            		 $('#mess').html('');
	            	}
//	            	else{
//	            		 $("#cur_pas").css("background", "red");
//		                 $("#new_pas").css("background", "red");
//		                 //$("#register").prop("disabled", true);
//		                 $scope.message = "password already exist";
//	            	}
	            }
		    		 }else{
					     $scope.loginNow();
					    }
		      };
		    
/*=================================CHANGE PERSONAL DETAILS==============================================*/
		      $(".datepicker form-control")
	          .datepicker({
	             dateFormat: 'dd/mm/yyyy'
	          }); 
		      
		      $("#state").change(function () {
		    	  $scope.ifAnyChange=false;
		      });

		      $('input[name=state]').change(function() {
		    	  $scope.ifAnyChange=false;
		      });
		      $scope.emailFormat = /[a-z]+[a-z0-9._]+@[a-z]+\.(com|org|net|co.in|edu|gov)$/;
		      $scope.mobileFormat= /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
		      $scope.personalSubmit = function() {
		    	  if($scope.isUserLoggedIn() == true){
		    	  $scope.personalDetails.dob=$scope.birthDate.split("/").reverse().join("-");
		    	  if(religion_arr[$('#religion').val()]!="Select Religion"){
		    	  $scope.personalDetails.religion=religion_arr[$('#religion').val()];}
		    	  
		    	  if($('#caste').val()!="Select Caste"){
		    	  $scope.personalDetails.caste=$('#caste').val();}
		    	  
		    	  if(state_arr[$('#state').val()]!="Select State"){
		    	 $scope.personalDetails.state= state_arr[$('#state').val()];}
		    	 
		    	/* if($('#city').val()!="Select City"){
		    	  $scope.personalDetails.city=$('#city').val();}*/
		    	 
		    	 if(quali_arr[$('#qualification').val()]!="Select Qualification"){
		    	  $scope.personalDetails.qualification=quali_arr[$('#qualification').val()];}
		    	 
		    	  //alert($scope.personalDetails.state+","+$scope.personalDetails.city+","+$scope.personalDetails.religion+","+$scope.personalDetails.caste);
		    	// alert($scope.personalDetails.qualification);
		    	  if($('#male').is(':checked')==true){$scope.personalDetails.gender="Male"}
		    	  else if($('#female').is(':checked')==true){$scope.personalDetails.gender="Female"}else{$scope.personalDetails.gender=""}
		    	  
		    	  if($('#mar').is(':checked')==true){$scope.personalDetails.maritalStatus="Married"}
		    	  else if($('#unmar').is(':checked')==true){$scope.personalDetails.maritalStatus="UnMarried"}else{$scope.personalDetails.maritalStatus=""}
		    	  
		    	  //alert(JSOG.stringify($scope.personalDetails));
		    	  if($scope.personalDetails){
		    		  UserSettingService.editPersonalDetail($scope.personalDetails);
		    		  //$scope.succmessage="succesfully saved ur details"
		    			  
		    	  }
		    	  
		      }else{
				     $scope.loginNow();
				    }
		      };
		
			     
			      $('input[name=myInput]').change(function() {
			    	  $scope.ifAnyChange=false;
			      });
			      
			      $('#textadd').change(function() {
			    	  $scope.ifAnyChange=false;
			      });
			      
			      $scope.enableSubmit= function(){$scope.ifAnyChange=false;}
	 
/*==================================UPLOAD IMAGE===============================================================*/	
			      
			      $scope.showUserImage= function(){
			    	  $('#userPic_Modal').appendTo("body").modal('show');
			      }
			      
			      $scope.showImage=function(){
						$('#uploadFile_modal').appendTo("body").modal('show');
						};
			      
			      $scope.attachment;		     
			      $scope.uploadFile = function(element) { 
			    	  if($scope.isUserLoggedIn() == true){
			    	  if (typeof ($("#uploadBtn")[0].files) != "undefined") {
			                var size = parseFloat($("#uploadBtn")[0].files[0].size / 2048).toFixed(2);
			                var img_mb= size/1000;
			                if(img_mb<=2){
			                	$('#fileUp').css("background", "green");
			                	$('#uploadFile_modal').modal('hide');
			                	var regFormData = new FormData();
			                	regFormData.append('image',$(element)[0].files[0]);
			                	regFormData.append('imageType',"userimage"); 
			                	regFormData.append('userId',$scope.userId);
			                	//alert(JSOG.stringify(regFormData));
			                	$http({
							      method: 'POST',
							      url: "/user/uploadimage",
							      headers: {'Content-Type': undefined},
							      data: regFormData
							      }).then(function successCallback(response) {
							     // alert(JSOG.stringify(response));
								 
							      $route.reload();
							    	  // $http.get('/user/downloadUserImage?userId='+$scope.userId+'&imageType=userimage&imageSize=standard').then(function(response) {
											// if (response != null) {
												// //alert(JSOG.stringify(response.data));
												// var imgval= response.data;
												// $scope.userImage= imgval;
												// $('#user_img').attr('src','data:image/png;base64,'+imgval);
												// $('#indx_user_img').attr('src','data:image/png;base64,'+imgval);
												// $('#per_user_img').attr('src','data:image/png;base64,'+imgval);
												// $('#userPic_Modal_img').attr('src','data:image/png;base64,'+imgval);
												// $('#per_usrModalImg').attr('src','data:image/png;base64,'+imgval);
											// }
										// });  

							      }, function errorCallback(response) {
							      //alert("fail");
							      });
			                }else{
			                	$scope.errormsg = "Please upload the image of less than 2 mb size";
			                	$('#fileUp').css("background", "red");
			                	alert("Please upload the image of less than 2 mb size");
			                		
			                }
			                
			            } else {
			                alert("This browser does not support HTML5.");
			            }
			    	  
			    	 // alert(JSOG.stringify($(element)[0].files[0]));
			    	  }else{
						     $scope.loginNow();
						    }
			    	  
			    	  };
			    	  
			      
			      
			     
			      
			      
/*====================================DEACTIVATE ACCOUNT================================================*/
			      $scope.deactivateAcc= function(){
			    	  if($scope.isUserLoggedIn() == true){
			    	  $http.post('/user/deactivateUserAccount?userId='+$scope.userId).then(
								function(response){
									alert("Succesfully deactivated your account");
									$scope.logout();
									window.location.href="/";
									/*window.location.hash="no-back-button";
									window.location.hash="Again-No-back-button";//again because google chrome don't insert first hash into history
									window.onhashchange=function(){window.location.hash="no-back-button";}
									*/
								});
			      }else{
					     $scope.loginNow();
				    }
			  }
/*====================================================NOTIFICATION=============================================*/
			      $scope.webshow=false;
					 $scope.mobshow=false;
					 $scope.emailshow=false;
									
		 
			      $scope.webopen=function(){
						 if($('#webpage_noti').hasClass('coll fa fa-pencil')){
							 $scope.webshow=true;
							 $('.ul.noti_sett li ul.sub_lis').show();
							// alert($scope.webshow);
					    	 $('#webpage_noti').removeClass('coll fa fa-pencil');
					    	  $('#webpage_noti').addClass('coll fa fa-times');	
						 }else{
							 //alert();
				    		 $scope.webshow=false;
				    		 $('#webpage_noti').removeClass('coll fa fa-times');
					    	 $('#webpage_noti').addClass('coll fa fa-pencil');
						 }
			    	  	    	  
			    	};
			    	
			    	/*$scope.mobopen=function(){
			    		 if($('#mob_noti').hasClass('coll fa fa-pencil')){
							 $scope.mobshow=true;
					    	 $('#mob_noti').removeClass('coll fa fa-pencil');
					    	  $('#mob_noti').addClass('coll fa fa-times');	
						 }else{
							 //alert();
				    		 $scope.mobshow=false;
				    		 $('#mob_noti').removeClass('coll fa fa-times');
					    	 $('#mob_noti').addClass('coll fa fa-pencil');
						 }	    	  
				    	};*/
				    	
				    	/*$scope.emailopen=function(){
				    		 if($('#email_noti').hasClass('coll fa fa-pencil')){
								 $scope.emailshow=true;
						    	 $('#email_noti').removeClass('coll fa fa-pencil');
						    	  $('#email_noti').addClass('coll fa fa-times');	
							 }else{
								 //alert();
					    		 $scope.emailshow=false;
					    		 $('#email_noti').removeClass('coll fa fa-times');
						    	 $('#email_noti').addClass('coll fa fa-pencil');
							 }		    	  
					    	};*/
			   
					    	
			    	 $http.get('/user/getAllUserUINotification?userId='+$scope.userId).then(function(response) {
							if (response.data.messages != null) {
								$scope.notiRes = response.data.messages;	
								$scope.genEnabled=JSOG.stringify($scope.notiRes[0].enabled);
								$scope.opinionEnabled=JSOG.stringify($scope.notiRes[1].enabled);
								
								//alert("gen: "+$scope.genEnabled+" , opi: "+$scope.opinionEnabled);
								if($scope.genEnabled=="true"){
									$('#web_1').prop('checked', true);
									}
								if($scope.opinionEnabled=="true"){
									$('#web_2').prop('checked', true);
									}
								
							}
						});
			    	 $('#notiButton').hide();
			    	 $( "#web_1" ).click(function() {
			    		 $('#notiButton').show(); 
						});
					 $( "#web_2" ).click(function() {	 
						 $('#notiButton').show();
						});
			    	 
					    	$scope.userNotification={
							    	 'userId' : $scope.userId,
							    	 'notifications': [
							    	  {'notificationType':"Opinion",
							    	   'enabled' : ""},
							    	  {'notificationType':"General",
							    	   'enabled' : ""}
							    	]
							    	};
					    	
					    	 $scope.updateNotificationSettings= function(){
					    		 if($scope.isUserLoggedIn() == true){
					    			 if(($('#web_1').prop('checked'))==true){
							    	   $scope.userNotification.notifications[0].enabled="true";
							    	   //$('#web_1').prop('checked', true);}
					    			 }else{
							    		$scope.userNotification.notifications[0].enabled="false";
							    		//$('#web_1').prop('checked', false);
							    		}
							    	if(($('#web_2').prop('checked'))==true){
							    		$scope.userNotification.notifications[1].enabled="true";
							    		//$('#web_2').prop('checked', true);
							    		}else{
							    			$scope.userNotification.notifications[1].enabled="false";
							    			//$('#web_2').prop('checked', false);
							    			}
							    	
					    			 //alert($scope.userNotification.notifications[0].enabled);
					    			 //alert($scope.userNotification.notifications[1].enabled);
							    	UserSettingService.updateNoti($scope.userNotification);
							    	 $scope.disabled=true;
							    	
					    	 }else{
							     $scope.loginNow();
						    }
					    };	
		   /*======================================password submit button disable========================================*/
					
					    
			      $("button[id=register]").attr('disabled','disabled');
			      $("#conf_pas").prop('disabled', true);
			   
			      $('#new_pas').on('focus', function () {
			    	  $scope.pwdIsClick=false;  
			      });
			      $('#conf_pas').on('keyup', function () {
			    	  $scope.pwdIsClick=true; 
			      });			      
			      $('#new_pas').on('keyup', function () {
					     var per=$(this).val();
					     
					    	 // alert(per);
					    	  if ($(this).val() == $('#cur_pas').val()) {
					    	        $('#message').html('you already have this password').css('color', 'red');	        
					    	    }else{
					    	    	$('#message').html('').css('color', '');
					    	    	if ( per.length>6){
					    	    		$scope.pwdIsClick=true;
										$("#conf_pas").prop('disabled', false);
					    	    		$('#conf_pas').on('keyup', function () {
						    	    		if ($(this).val() == $('#new_pas').val()) {
						    	    			$('#mess').html('ok you can now submit').css('color', 'green');
						    	    			$("button[id=register]").removeAttr('disabled');
						    	    		} else $('#mess').html('not matching with your new password entry').css('color', 'red');
						    	    	});
					    	    	}
					    	    } /*else {
					    	    	$('#message').html('').css('color', '');
					    	    	if ( per.match(/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/)) {
					    	    		//alert("yes");
					    	    		$('#number').removeClass('invalid').addClass('valid');
					    	    		$('#letter').removeClass('invalid').addClass('valid');
					    	    		$('#capital').removeClass('invalid').addClass('valid');
					    	    		$('#length').removeClass('invalid').addClass('valid');
										$scope.pwdIsClick=true;
										$("#conf_pas").prop('disabled', false);
					    	    		$('#conf_pas').on('keyup', function () {
						    	    		if ($(this).val() == $('#new_pas').val()) {
						    	    			$('#mess').html('ok you can now submit').css('color', 'green');
						    	    			$("button[id=register]").removeAttr('disabled');
						    	    		} else $('#mess').html('not matching with your new password entry').css('color', 'red');
						    	    	});
			    	            			}
									else {
				    	            $('#letter').removeClass('valid').addClass('invalid');
									}
					    	    	
					    	    	
					    	    }*/
					    	 
					      });
			  
			var par=($routeParams.userId);	  
			//var a1=par.split("&tabName="));	 
			//alert(par.split("tabNumber=")[1]);
			$scope.tabNum=par.split("tabNumber=")[1];
			if($scope.tabNum==4){$scope.setTab(4);}
			else if($scope.tabNum==2){$scope.setTab(2);} 
			else if($scope.tabNum==3){$scope.setTab(3);} 
			else{$scope.setTab(1);}
			
			
	       
		}]);
