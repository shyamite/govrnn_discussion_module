'use strict';

/**
 * @ngdoc directive
 * @name govrnnApp.directive:myDirective
 * @description
 * # myDirective
 */
angular.module('govrnnApp')
  .directive('privacyPolicy', function () {
    return {
      templateUrl: 'templates/modal_privacy_policy.html',
      restrict: 'E',	
      link: function postLink(scope, element, attrs) {
       // element.text('this is the myDirective directive');
	   // $('.animation_text input').on('blur', function(){
		   // $(this).parent('.animation_text').removeClass('input-desc-hover');

		// }).on('focus', function(){
		  // $(this).parent('.animation_text').addClass('input-desc-hover');
		// });

		// $('.animation_text.dob input').on('click', function(){
		  // $(this).parent('.animation_text').addClass('has_txt');
		// });

		// var inputs = $('.animation_text input').not(':submit');

		// inputs.on('input', function() {
			// $(inputs[inputs.index(this)]).parent().toggleClass('has_txt', this.value > '');
		// });
		// if(inputs[0])
			// inputs[0].focus();
      },
	  controller: ['$scope','$http','$rootScope', function ($scope, $http, $rootScope) {

		$scope.user = {};
		  $scope.closePrivacyPolicyPopup = function(form){
			$scope.hasError = false;
			$scope.user = {};
			form.$setPristine();
			$('#privacypolicy_pop').modal("hide");
			$('#sign_pop').modal("show");
		};
    }]
    };
  });
