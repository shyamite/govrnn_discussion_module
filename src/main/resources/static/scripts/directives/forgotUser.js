'use strict';

/**
 * @ngdoc directive
 * @name govrnnApp.directive:myDirective
 * @description
 * # myDirective
 */
angular.module('govrnnApp')
  .directive('forgotUserModal', function () {
    return {
      templateUrl: 'templates/modal_forgotuser.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        // element.text('this is the myDirective directive');
        // $('.animation_text input').on('blur', function(){
          // $(this).parent('.animation_text').removeClass('input-desc-hover');

        // }).on('focus', function(){
          // $(this).parent('.animation_text').addClass('input-desc-hover');
        // });

        // var inputs = $('.animation_text input').not(':submit');

        // inputs.on('input', function() {
          // $(inputs[inputs.index(this)]).parent().toggleClass('has_txt', this.value > '');
        // });
        // inputs[0].focus();
      },
      controller: ['$scope','$http','$rootScope', function ($scope, $http, $rootScope) {

        $scope.user = {};
        $scope.hasError = false;
		$scope.forgotUserStatus = false;
		$scope.forgotUserDeletedStatus = false;
        /* reset logged in user password*/
        $scope.resetuser = function(form) {
          if (form.$valid) {
            var userData = $.param({
              emailId: $scope.user.emailId
            });
            $http({
              method: 'POST',
              url: $rootScope.liveHost+"/user/forgotUserName",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              // withCredentials: true,
              data: userData
            }).then(function successCallback(response) {
			  if(response.data.success === true){	
				//  $('#forgotuser_pop').modal("hide");
				  $scope.user = {};
				  form.$setPristine();
				  $scope.forgotUserStatus = true;
			  }else{
				  $scope.forgotUserDeletedStatus = true;
					$scope.forgotUserDeletedMessage =response.data.messages[0] ;
				$scope.hasError = true;
				$scope.user = {};
				form.$setPristine();
			  }
              console.log("success forgot");
            }, function errorCallback(response) {
              $scope.hasError = true;
             $scope.user = {};
              console.log("error forgot");
            });

          }
        };
		
		$scope.showSignInPopup = function (form) {
		  $scope.hasError = false;
		  $scope.forgotPwdStatus = false;	
          $scope.user = {};
		  form.$setPristine();
		  $('#forgotuser_pop').modal("hide");
          $('#login_pop').modal("show");
        };
		
		$scope.closeForgotUserPopup = function(form){
			$scope.hasError = false;
			$scope.forgotPwdStatus = false;
			$scope.user = {};
			form.$setPristine();
			$('#forgotuser_pop').modal("hide");
		};
		
      }]
    };
  });
