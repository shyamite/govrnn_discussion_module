package com.ohmuk.folitics.controller.response;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.utils.TestDataPreRequisites;
import com.ohmuk.folitics.utils.TestDataUtils;
import com.ohmuk.folitics.utils.TestUtils;

/**
 * @author Abhishek
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestResponseController {

	// Test RestTemplate to invoke the APIs.
	private TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void TestResponseCURD() throws Exception {
		Map<String, Object> prerequisiteMap = new HashMap<String, Object>();
		prerequisiteMap = TestDataPreRequisites.createResponsePrerequisite();
		Response response = testAddResponseApi(prerequisiteMap);
		testAddResponseWithChildrenApi(prerequisiteMap,response);
		//Response response1 = testAddResponseApi(prerequisiteMap);
		testAddResponseWithChildrenApi(prerequisiteMap,response);
		testEditResponseApi(response, prerequisiteMap);
		testGetByOpinionIdResponseApi(response.getOpinion().getId());
		testGetByUserIdResponseApi(response.getUser().getId());
		testGetResponsesForOpinionResponseApi(response.getOpinion().getId());
		testGetAllResponsesApi();
		/*testDeleteResponseApi(response.getId());
		testPermanentDeleteResponseApi(response.getId());*/
		// TestDataPreRequisites.deleteResponsePrerequisite(prerequisiteMap);
	}

	private Response testAddResponseApi(Map<String, Object> prerequisiteMap) throws JsonProcessingException {
		// HttpEntity<String> httpEntity =
		// TestUtils.getHttpEntity(TestDataUtils.getResponseRequestBody(prerequisiteMap));

		String mapAsJson = new ObjectMapper().writeValueAsString(TestDataUtils.getResponseRequestBody(prerequisiteMap));

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(mapAsJson, requestHeaders);
		/*
		 * HttpHeaders userListHeaders = new HttpHeaders();
		 * userListHeaders.setContentType(MediaType.APPLICATION_JSON);
		 * HttpEntity<String> userIds = new
		 * HttpEntity<String>(mapAsJson,requestHeaders);
		 */

		/*
		 * LinkedMultiValueMap<String, Object> map = new
		 * LinkedMultiValueMap<>(); map.add("request", requestEntity);
		 * 
		 * HttpHeaders headers = new HttpHeaders();
		 * headers.setContentType(MediaType.APPLICATION_JSON);
		 * 
		 * HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new
		 * HttpEntity<LinkedMultiValueMap<String, Object>>( map, headers);
		 */

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RESPONSE_CONTROLLER_APIS.ADD);
		HttpEntity<ResponseDto<Response>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, requestEntity, new ParameterizedTypeReference<ResponseDto<Response>>() {
				});

		ResponseDto<Response> apiResponse = response.getBody();
		assertNotNull(apiResponse);
		assertTrue(apiResponse.getSuccess());
		Response responseObject = (Response) (response.getBody().getMessages().get(0));
		// commented to avoid infinite recursive json
		// responseObject = TestDataUtils.getResponse(responseObject.getId());
		assertNotNull(responseObject);
		return responseObject;
	}

	private Response testAddResponseWithChildrenApi(Map<String, Object> prerequisiteMap, Response parent)
			throws JsonProcessingException {
		// HttpEntity<String> httpEntity =
		// TestUtils.getHttpEntity(TestDataUtils.getResponseRequestBody(prerequisiteMap));
		
		String mapAsJson = new ObjectMapper()
				.writeValueAsString(TestDataUtils.getResponseRequesWithParentBody(prerequisiteMap, parent));

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(mapAsJson, requestHeaders);

		// LinkedMultiValueMap<String, Object> map = new
		// LinkedMultiValueMap<>();
		// map.add("request", requestEntity);
		//
		// HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new
		// HttpEntity<LinkedMultiValueMap<String, Object>>(
		// map, requestHeaders);

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RESPONSE_CONTROLLER_APIS.ADD);
		HttpEntity<ResponseDto<Response>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, requestEntity, new ParameterizedTypeReference<ResponseDto<Response>>() {
				});

		ResponseDto<Response> apiResponse = response.getBody();
		assertNotNull(apiResponse);
		assertTrue(apiResponse.getSuccess());
		Response responseObject = (Response) (response.getBody().getMessages().get(0));
		// commented to avoid infinite recursive json
		// responseObject = TestDataUtils.getResponse(responseObject.getId());
		assertNotNull(responseObject);
		return responseObject;
	}

	private void testEditResponseApi(Response responseObject, Map<String, Object> prerequisiteMap)
			throws JsonProcessingException {
		// Map<String, Object> requestBody =
		// TestDataUtils.getResponseRequestBody(prerequisiteMap);
		// Response responseObject = TestDataUtils.getResponse(id);
		String updatedDescription = "this is updated text";
		// requestBody.put("id", id);
		// requestBody.put("content", updatedDescription);
		responseObject.setContent(updatedDescription);

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RESPONSE_CONTROLLER_APIS.EDIT);

		// HttpEntity<String> httpEntity = TestUtils.getHttpEntity(requestBody);

		HttpEntity<String> httpEntity = TestUtils
				.getHttpEntity(new ObjectMapper().convertValue(responseObject, Map.class));

		HttpEntity<ResponseDto<Response>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Response>>() {
				});
		ResponseDto<Response> apiResponse = response.getBody();
		assertNotNull(apiResponse);
		assertTrue(apiResponse.getSuccess());
		Response responseRetunred = (Response) (response.getBody().getMessages().get(0));
		assertTrue(updatedDescription.equalsIgnoreCase(responseRetunred.getContent()));
	}

	private void testDeleteResponseApi(Long id) throws JsonProcessingException {
		Response responseObject = TestDataUtils.getResponse(id);
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RESPONSE_CONTROLLER_APIS.DELETE);
		HttpEntity<?> entity = new HttpEntity<>(responseObject);
		HttpEntity<ResponseDto<Response>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, entity, new ParameterizedTypeReference<ResponseDto<Response>>() {
				});
		assertNotNull(response);
		assertTrue(response.getBody().getSuccess());
	}

	private void testPermanentDeleteResponseApi(Long id) {
		// Response responseObject = TestDataUtils.getResponse(id);
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RESPONSE_CONTROLLER_APIS.PERMANENTDELETE)
				.queryParam("id", id);
		HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
		HttpEntity<ResponseDto<Response>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<Response>>() {
				});
		assertNotNull(response);
		assertTrue(response.getBody().getSuccess());
	}

	private void testGetByOpinionIdResponseApi(Long id) {
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RESPONSE_CONTROLLER_APIS.GET_BY_OPINION_ID)
				.queryParam("id", id);
		HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
		HttpEntity<ResponseDto<Response>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<Response>>() {
				});
		assertNotNull(response);
		System.out.println("response ::"+response);
		assertTrue(response.getBody().getSuccess());
	}

	private void testGetByUserIdResponseApi(Long id) {
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RESPONSE_CONTROLLER_APIS.GET_BY_USER_ID)
				.queryParam("id", id);
		HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
		HttpEntity<ResponseDto<Response>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<Response>>() {
				});
		assertNotNull(response);
		assertTrue(response.getBody().getSuccess());
	}

	private void testGetResponsesForOpinionResponseApi(Long id) {
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RESPONSE_CONTROLLER_APIS.GET_RESPONSE_FOR_OPINION)
				.queryParam("id", id);
		HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
		HttpEntity<ResponseDto<Response>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<Response>>() {
				});

		assertNotNull(response);
		assertTrue(response.getBody().getSuccess());

	}

	private List<Response> testGetAllResponsesApi() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
                TestUtils.BASE_URL + TestUtils.RESPONSE_CONTROLLER_APIS.GET_ALL_RESPONSES);
        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
        HttpEntity<ResponseDto<List<Response>>> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<List<Response>>>() {
                });

        ResponseDto<List<Response>> apiResponses = response.getBody();
        assertNotNull(apiResponses);
      //  assertTrue(apiResponse.getSuccess());
        List<Response> responses = apiResponses.getMessages().get(0);
        return  responses;


    }

}