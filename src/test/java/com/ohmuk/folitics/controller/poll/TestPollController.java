package com.ohmuk.folitics.controller.poll;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.utils.TestDataPreRequisites;
import com.ohmuk.folitics.utils.TestDataPreRequisites.ComponentPrerequisiteMapKeys;
import com.ohmuk.folitics.utils.TestDataUtils;
import com.ohmuk.folitics.utils.TestUtils;

@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPollController {
	// Test RestTemplate to invoke the APIs.
	private TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void TestPollCURD() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			IntrospectionException, IOException {
		//user 1
		Map<String, Object> prerequisitemap1 = new HashMap<String, Object>();
		prerequisitemap1 = TestDataPreRequisites.createPollPrerequisite();
		// user 2
		Map<String, Object> prerequisitemap2 = new HashMap<String, Object>();
		prerequisitemap2 = TestDataPreRequisites.createPollPrerequisite();
		
		User user  = (User)prerequisitemap1.get(ComponentPrerequisiteMapKeys.USER);
		Poll poll1 = testAddPollApi(prerequisitemap1,prerequisitemap2);
		Poll poll2 = testAddPollApi(prerequisitemap1,prerequisitemap2);
		testPollOptionsApi(poll1.getId(),user.getId() );
		testActivePollsApi();
		testEditPollApi(poll1.getId());
		testDeletePollApi(poll1.getId());
		testPermanentDeletePollApi(poll1.getId());
		testPermanentDeletePollApi(poll2.getId());
	}

		private Poll testAddPollApi(Map<String, Object> user1,Map<String, Object> user2) throws IOException {
	
    	HttpMessageConverter<Object> jackson = new MappingJackson2HttpMessageConverter();
        HttpMessageConverter<byte[]> resource = new ByteArrayHttpMessageConverter();
        FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        formHttpMessageConverter.addPartConverter(jackson);
        formHttpMessageConverter.addPartConverter(resource);

        RestTemplate restTemplate = new RestTemplate(Arrays.asList(jackson, resource, formHttpMessageConverter));

        Path path = Paths.get(TestUtils.TEST_IMAGE);
        byte[] data = Files.readAllBytes(path);

        Resource file = new ByteArrayResource(data) {
            @Override
            public String getFilename() {
                return "testimage.jpg";
            }
        };

        HttpHeaders imageHeaders = new HttpHeaders();
        imageHeaders.setContentType(MediaType.IMAGE_JPEG);
        HttpEntity<Resource> image = new HttpEntity<Resource>(file, imageHeaders);

        String mapAsJson = new ObjectMapper().writeValueAsString(TestDataUtils.getPollRequestBody(user1,user2));

        HttpHeaders opinionHeaders = new HttpHeaders();
        opinionHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> opinionEntity = new HttpEntity<String>(mapAsJson, opinionHeaders);

        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file", image);
        map.add("poll", opinionEntity);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(
                map, headers);
	
	    UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.POLL_CONTROLLER_APIS.ADD);
		
		HttpEntity<ResponseDto<Poll>> responsePoll = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Poll>>() {
				});
		ResponseDto<Poll> apiPollResponse = responsePoll.getBody();
		assertNotNull(apiPollResponse);
		assertTrue(apiPollResponse.getSuccess());
		Poll poll1 = (Poll) (responsePoll.getBody().getMessages().get(0));
		Poll poll2 = TestDataUtils.getPoll(poll1.getId());
		assertNotNull(poll2);
		return poll2;
	}

	private void testEditPollApi(Long id) throws JsonProcessingException, IntrospectionException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		Poll poll = TestDataUtils.getPoll(id);

		// Map<String, Object> requestBody =
		// TestDataUtils.getPollRequestBody(prerequisitemap);

		/*
		 * BeanInfo info = Introspector.getBeanInfo(poll.getClass()); for
		 * (PropertyDescriptor pd : info.getPropertyDescriptors()) { Method
		 * reader = pd.getReadMethod(); if (reader != null)
		 * requestBody.put(pd.getName(), reader.invoke(poll)); }
		 */

		String updatedQuestion = "Is this updated question?";
		poll.setQuestion(updatedQuestion);

		/*
		 * requestBody.put("id", id); requestBody.put("question",
		 * updatedQuestion); requestBody.put("editedBy", 11l);
		 */

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.POLL_CONTROLLER_APIS.EDIT);

		Map<String, Object> map = new ObjectMapper().convertValue(poll, Map.class);
		HttpEntity<String> httpEntity = TestUtils.getHttpEntity(map);

		HttpEntity<ResponseDto<Poll>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Poll>>() {
				});

		ResponseDto<Poll> apiResponse = response.getBody();

		assertNotNull(apiResponse);
		assertTrue(apiResponse.getSuccess());

		Poll poll2 = (Poll) (response.getBody().getMessages().get(0));
		assertTrue(updatedQuestion.equals(poll2.getQuestion()));
	}

	private void testDeletePollApi(Long id) throws JsonProcessingException {

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.POLL_CONTROLLER_APIS.DELETE_BY_ID).queryParam("id", id);

		HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();

		HttpEntity<ResponseDto<Poll>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,
				httpEntity, new ParameterizedTypeReference<ResponseDto<Poll>>() {
				});

		assertNotNull(response);
		//assertTrue(response.getBody().getSuccess());
	}

	private void testPermanentDeletePollApi(Long id) throws JsonProcessingException {

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.POLL_CONTROLLER_APIS.PERMANENT_DELETE_BY_ID)
				.queryParam("id", id);

		HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();

		HttpEntity<ResponseDto<Poll>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,
				httpEntity, new ParameterizedTypeReference<ResponseDto<Poll>>() {
				});

		assertNotNull(response);
		
	}

	private void testActivePollsApi() {
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.POLL_CONTROLLER_APIS.GET_ACTIVE_POLLS);
		HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
		HttpEntity<ResponseDto<Poll>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,
				httpEntity, new ParameterizedTypeReference<ResponseDto<Poll>>() {
				});
		assertNotNull(response);
		assertTrue(response.getBody().getSuccess());
	}

	private void testPollOptionsApi(Long pollId,Long userId) throws JsonProcessingException {
		
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.POLL_CONTROLLER_APIS.GET_POLL_OPTIONS).queryParam("pollId", pollId).queryParam("userId", userId);
		HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
		HttpEntity<ResponseDto<Long>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,
				httpEntity, new ParameterizedTypeReference<ResponseDto<Long>>() {
				});
		assertNotNull(response);
		assertTrue(response.getBody().getSuccess());
		
	}
	/*
	 * private void testGetPollForSentiment(Map<String, Object> prerequisitemap)
	 * { Sentiment sentiment = (Sentiment)
	 * prerequisitemap.get(ComponentPrerequisiteMapKeys.SENTIMENT);
	 * UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
	 * TestUtils.BASE_URL +
	 * TestUtils.POLL_CONTROLLER_APIS.GET_POLLS_FOR_SENTIMENT).queryParam("id",
	 * sentiment.getId()); HttpEntity<HttpHeaders> httpEntity =
	 * TestUtils.getHttpEntityHttpHeaders(); HttpEntity<ResponseDto<Poll>>
	 * response = restTemplate.exchange(builder.build().encode().toUri(),
	 * HttpMethod.GET, httpEntity, new
	 * ParameterizedTypeReference<ResponseDto<Poll>>() { });
	 * assertNotNull(response); assertTrue(response.getBody().getSuccess()); }
	 */
}
