package com.ohmuk.folitics.controller.comment;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohmuk.folitics.beans.CommentBean;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.utils.TestDataPreRequisites;
import com.ohmuk.folitics.utils.TestUtils;

/**
 * @author soumya
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCommentController {
	// Test RestTemplate to invoke the APIs.
	protected TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void TestController() throws JsonProcessingException, IOException {

		CommentBean commentBean = TestDataPreRequisites.createCommentPrerequisite("trend");
		testCreateCommentApi(commentBean);
	//	testDeleteTrendCommentApi(commentBean);
	//	testDeleteTrendApi(commentBean);

	}

	protected void testCreateCommentApi(CommentBean commentBean)
			throws JsonProcessingException, IllegalArgumentException {

		@SuppressWarnings("unchecked")
		HttpEntity<String> httpEntity = TestUtils
				.getHttpEntity(new ObjectMapper().convertValue(commentBean, Map.class));

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(TestUtils.BASE_URL + TestUtils.COMMENT_CONTROLLER_APIS.ADD_COMMENT);
		HttpEntity<ResponseDto<Object>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Object>>() {
				});
		assertNotNull(response);
		Object responseObject = response.getBody().getMessages().get(0);
		// return response.getBody().getMessages().get(0);
	}

	protected void testgetAllCommentsApi(CommentBean commentBean)
			throws JsonProcessingException, IllegalArgumentException {

		@SuppressWarnings("unchecked")
		HttpEntity<String> httpEntity = TestUtils
				.getHttpEntity(new ObjectMapper().convertValue(commentBean, Map.class));

		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(TestUtils.BASE_URL + TestUtils.COMMENT_CONTROLLER_APIS.GET_ALL_COMMENTS_BY_TYPE_COMPONENT_ID)
				.queryParam("componentType", commentBean.getComponentType())
				.queryParam("componentId", commentBean.getComponentId());
		HttpEntity<ResponseDto<Object>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Object>>() {
				});
		assertNotNull(response);
		// return response.getBody().getMessages().get(0);
	}

	protected void testDeleteTrendApi(CommentBean commentBean)
			throws JsonProcessingException, IllegalArgumentException {

		 UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
	                TestUtils.BASE_URL + TestUtils.TREND_CONTROLLER_APIS.DELETE_TREND).queryParam("id", commentBean.getId());
	        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
	        HttpEntity<ResponseDto<Object>> response = restTemplate.exchange(builder.build().encode().toUri(),
	                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<Object>>() {
	                });
		assertNotNull(response);
		Object responseObject = response.getBody().getMessages().get(0);
		// return response.getBody().getMessages().get(0);
	}

	protected void testDeleteTrendCommentApi(CommentBean commentBean)
			throws JsonProcessingException, IllegalArgumentException {

		 UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
	                TestUtils.BASE_URL + TestUtils.TREND_CONTROLLER_APIS.DLETE_TREND_COMMENTS).queryParam("trendMappingId", commentBean.getId());
	        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
	        HttpEntity<ResponseDto<Object>> response = restTemplate.exchange(builder.build().encode().toUri(),
	                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<Object>>() {
	                });
		assertNotNull(response);
		Object responseObject = response.getBody().getMessages().get(0);
		// return response.getBody().getMessages().get(0);
	}

}
