package com.ohmuk.folitics.controller.notification;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.ouput.model.GeneralNotificationOutputModel;
import com.ohmuk.folitics.utils.TestDataUtils;
import com.ohmuk.folitics.utils.TestUtils;

@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestNotificationController {
	// Test RestTemplate to invoke the APIs.
	private TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void TestNotification() throws Exception {
		testOpinionNotification();
		testGeneralNotification();
	}

	private void testOpinionNotification() throws Exception {

		HttpEntity<String> httpEntity = TestUtils.getHttpEntity(TestDataUtils.getNotificationPreRequisites());

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.NOTIFICATION_CONTROLLER_APIS.GET_OPINION_NOTIFICATION)
				.queryParam("userId", 30).queryParam("componentId", 1l).queryParam("componentType", "");
		HttpEntity<ResponseDto<List<GeneralNotificationOutputModel>>> response = restTemplate.exchange(
				builder.build().encode().toUri(), HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<ResponseDto<List<GeneralNotificationOutputModel>>>() {
				});
		assertNotNull(response);
		

	}

	private void testGeneralNotification() throws Exception {

		HttpEntity<String> httpEntity = TestUtils.getHttpEntity(TestDataUtils.getNotificationPreRequisites());

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.NOTIFICATION_CONTROLLER_APIS.GET_GENERAL_NOTIFICATION)
				.queryParam("userId", 30).queryParam("componentId", 1l).queryParam("componentType", "");
		HttpEntity<ResponseDto<GeneralNotificationOutputModel>> response = restTemplate.exchange(
				builder.build().encode().toUri(), HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<ResponseDto<GeneralNotificationOutputModel>>() {
				});
		assertNotNull(response);
	}

}
